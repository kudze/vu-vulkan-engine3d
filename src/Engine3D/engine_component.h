//
// Created by karol on 5/6/2020.
//

#ifndef INC_3DENGINE_ENGINE_COMPONENT_H
#define INC_3DENGINE_ENGINE_COMPONENT_H

#include "util/non_copyable.h"

namespace Engine3D
{
    class Engine;

    class EngineComponent : public NonCopyable {
        Engine* engine = nullptr;

    public:
        explicit EngineComponent(Engine* engine);
        virtual ~EngineComponent();

        Engine* getEngine() const;
    };
}

#include "engine.h"


#endif //INC_3DENGINE_ENGINE_COMPONENT_H
