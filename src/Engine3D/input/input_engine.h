//
// Created by karol on 5/6/2020.
//

#ifndef INC_3DENGINE_INPUT_ENGINE_H
#define INC_3DENGINE_INPUT_ENGINE_H

#include "../engine_component.h"
#include "../engine.h"

namespace Engine3D {

    class InputEngine
        : public EngineComponent
    {
        bool mouseInsideWindow = false;
        int mouseXmoved = 0;
        int mouseYmoved = 0;

        void handleEvent(SDL_Event const& event);

    public:
        InputEngine(Engine* engine);
        ~InputEngine();

        void pollEvents();

        bool isKeyDown(int key) const;
        bool isKeyUp(int key) const;

        int getMouseMovedXAxisLastTick() const;
        int getMouseMovedYAxisLastTick() const;
    };

}

#endif //INC_3DENGINE_INPUT_ENGINE_H
