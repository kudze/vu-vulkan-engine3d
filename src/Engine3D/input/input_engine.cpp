//
// Created by karol on 5/6/2020.
//

#include "input_engine.h"
#include "../window/sdl.h"

using namespace Engine3D;

InputEngine::InputEngine(Engine3D::Engine *engine)
    : EngineComponent(engine)
{
    //SDL_SetWindowGrab(engine->getWindow()->getHandle(), SDL_TRUE);
}

InputEngine::~InputEngine() = default;

void InputEngine::handleEvent(SDL_Event const &event) {
    if (event.type == SDL_QUIT)
        this->getEngine()->stop();

    if (event.type == SDL_WINDOWEVENT_SIZE_CHANGED)
        this->getEngine()->getRenderer()->recreateSwapchain();

}

void InputEngine::pollEvents() {
    int currX, currY;
    SDL_GetMouseState(&currX, &currY);

    auto size = this->getEngine()->getWindow()->getSize();
    size.setHeight(size.getHeight() / 2);
    size.setWidth(size.getWidth() / 2);

    if(SDL_GetWindowFlags(this->getEngine()->getWindow()->getHandle()) & SDL_WINDOW_MOUSE_FOCUS) {
        SDL_WarpMouseInWindow(this->getEngine()->getWindow()->getHandle(), size.getWidth(), size.getHeight());

        if(this->mouseInsideWindow) {
            this->mouseXmoved = currX - size.getWidth();
            this->mouseYmoved = currY - size.getHeight();
        }

        this->mouseInsideWindow = true;

        SDL_ShowCursor(false);
    }

    else {
        this->mouseXmoved = 0;
        this->mouseYmoved = 0;
        this->mouseInsideWindow = false;

        SDL_ShowCursor(true);
    }

    SDL::pollEvents(
        [this](SDL_Event const &event) {
            this->handleEvent(event);
        }
    );
}

bool InputEngine::isKeyDown(int key) const {
    return SDL_GetKeyboardState(nullptr)[key];
}

bool InputEngine::isKeyUp(int key) const {
    return !this->isKeyDown(key);
}

int InputEngine::getMouseMovedXAxisLastTick() const {
    return this->mouseXmoved;
}

int InputEngine::getMouseMovedYAxisLastTick() const {
    return this->mouseYmoved;
}
