//
// Created by karol on 5/6/2020.
//

#ifndef INC_3DENGINE_ENGINE_H
#define INC_3DENGINE_ENGINE_H

#include "scene/scene.h"
#include "util/non_copyable.h"

namespace Engine3D {
    class InputEngine;

    namespace Vulkan {
        class Renderer;
        class Shader;
    }

    class Engine : public NonCopyable {
    private:
        Window* window = nullptr;
        Scene* scene = nullptr;
        Vulkan::Renderer* renderer = nullptr;
        InputEngine* inputEngine = nullptr;

        bool running = true;

        void update();

    public:
        explicit Engine(Window* window);
        ~Engine();

        void run();
        void stop();

        void setScene(Scene *scene);
        Scene *getScene() const;

        bool shouldRun() const;
        Window* getWindow() const;
        Vulkan::Renderer* getRenderer() const;
        InputEngine* getInputEngine() const;
    };

}

#include "vulkan/vulkan_renderer.h"
#include "vulkan/vulkan_shader.h"
#include "input/input_engine.h"


#endif //INC_3DENGINE_ENGINE_H
