//
// Created by karol on 5/12/2020.
//

#ifndef INC_3DENGINE_NON_COPYABLE_H
#define INC_3DENGINE_NON_COPYABLE_H

namespace Engine3D {

    /**
     * Makes class non assignable & non copyable
     */
    class NonCopyable {
    public:
        NonCopyable(NonCopyable const&) = delete;
        NonCopyable& operator=(NonCopyable const&) = delete;
        NonCopyable() {}
    };

};

#endif //INC_3DENGINE_NON_COPYABLE_H
