//
// Created by karol on 5/6/2020.
//

#include "engine_component.h"

using namespace Engine3D;

EngineComponent::EngineComponent(Engine* engine)
    : engine(engine)
{

}

EngineComponent::~EngineComponent() = default;

Engine * EngineComponent::getEngine() const {
    return this->engine;
}