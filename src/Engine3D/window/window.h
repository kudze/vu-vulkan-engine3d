//
// Created by kkraujelis on 2020-02-28.
//

#ifndef INC_3DENGINE_WINDOW_H
#define INC_3DENGINE_WINDOW_H

#include <string>
#include "SDL2/SDL.h"

#include "../math/dimensions.h"
#include "../util/non_copyable.h"


namespace Engine3D
{
    /**
     * Window class
     */
    class Window : public NonCopyable
    {
        public:
            using Size = Engine3D::Dimensions<uint16_t>;

            enum Type
            {
                WINDOWED,
                BORDERLESS,
                FULLSCREEN
            };

        private:

            SDL_Window* window = nullptr;

            void setWindow(SDL_Window *window);

    protected:

            void createWindow(Type type, std::string const& name, Size const& size, bool resizeable);
            void destroyWindow();

            virtual uint32_t constructFlags(Type type) const;

            Window();

        public:
            Window(
                Type type,
                std::string const& name,
                Size const& size,
                bool resizeable = false
            );
            virtual ~Window();

            void resize(Size const& size);
            void setType(Type type);
            void rename(std::string const& name);
            void setIsResizeable(bool enabled);

            Size getSize() const;
            Type getType() const;
            bool isResizeable() const;
            std::string getName() const;
            SDL_Window* getHandle() const;
    };

}


#endif //INC_3DENGINE_WINDOW_H
