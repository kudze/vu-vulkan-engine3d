//
// Created by kkraujelis on 2020-02-28.
//

#include "sdl.h"

namespace Engine3D
{
    namespace SDL
    {
        bool initialized = false;
    }
}

using namespace Engine3D;


bool SDL::isInitialized() {
    return SDL::initialized;
}

void SDL::initialize() {

    if(SDL::initialized)
        return;

    auto result = SDL_Init(SDL_INIT_VIDEO);

    if(result < 0)
        throw std::runtime_error("Failed to initialize SDL library! Error: " + std::string(SDL_GetError()));

    SDL::initialized = true;
}

void SDL::deinitialize() {

    if(!SDL::initialized)
        return;

    SDL_Quit();
}

void SDL::pollEvents(std::function<void(const SDL_Event &)> const& callback)
{
    SDL_Event event = {};

    while( SDL_PollEvent( &event ) != 0 )
    {
        callback(event);
    }
}