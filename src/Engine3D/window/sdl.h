//
// Created by kkraujelis on 2020-02-28.
//

#ifndef INC_3DENGINE_SDL_H
#define INC_3DENGINE_SDL_H

#include <stdexcept>
#include <functional>
#include "SDL2/SDL.h"

namespace Engine3D::SDL {

    bool isInitialized();

    /**
     * @throws std::runtime_exception
     */
    void initialize();

    void deinitialize();

    void pollEvents(std::function < void(SDL_Event
    const&)> const& callback);

};


#endif //INC_3DENGINE_SDL_H
