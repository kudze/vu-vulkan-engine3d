//
// Created by kkraujelis on 2020-02-28.
//

#include "window.h"
#include "sdl.h"

using namespace Engine3D;
using Engine3D::Window;

Window::Window(
    Type type,
    std::string const &name,
    Window::Size const &dimensions,
    bool resizeable
) {
    if (!SDL::isInitialized())
        SDL::initialize();

    this->createWindow(type, name, dimensions, resizeable);
}

Window::~Window() {
    this->destroyWindow();
}

uint32_t Window::constructFlags(Type type) const {
    uint32_t res = 0;

    switch (type) {
        case Type::WINDOWED: {

            break;
        }
        case Type::BORDERLESS: {
            res |= SDL_WINDOW_BORDERLESS;
            break;
        }
        case Type::FULLSCREEN: {
            res |= SDL_WINDOW_FULLSCREEN;
            break;
        }
    }

    return res;
}

Window::Window() = default;

void Window::createWindow(Type type, std::string const &name, Window::Size const &size, bool resizeable) {
    this->window = SDL_CreateWindow(
        name.c_str(),
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        size.getWidth(),
        size.getHeight(),
        this->constructFlags(type)
    );

    if (this->window == nullptr)
        throw std::runtime_error("Failed to create a window!" + std::string(SDL_GetError()));

    this->setIsResizeable(resizeable);
}

void Window::destroyWindow() {
    if (this->window == nullptr)
        return;

    SDL_DestroyWindow(this->window);
    this->window = nullptr;
}

void Window::resize(Window::Size const &size) {
    SDL_SetWindowSize(
        this->window,
        size.getWidth(),
        size.getHeight()
    );
}

void Window::setType(Type type) {
    if (this->getType() == type)
        return;

    auto name = this->getName();
    auto dimensions = this->getSize();

    this->destroyWindow();
    this->createWindow(type, name, dimensions, this->isResizeable());
}

void Window::rename(std::string const &name) {
    SDL_SetWindowTitle(
        this->window,
        name.c_str()
    );
}

void Window::setIsResizeable(bool enabled) {
    SDL_SetWindowResizable(this->window, enabled ? SDL_bool::SDL_TRUE : SDL_bool::SDL_FALSE);
}

std::string Window::getName() const {
    return std::string(
        SDL_GetWindowTitle(this->window)
    );
}

Window::Size Window::getSize() const {
    int w, h;

    SDL_GetWindowSize(
        this->window,
        &w,
        &h
    );

    return Window::Size(
        static_cast<uint16_t>(w),
        static_cast<uint16_t>(h)
    );
}

Window::Type Window::getType() const {
    uint32_t flags = SDL_GetWindowFlags(this->getHandle());

    if (flags & SDL_WINDOW_FULLSCREEN)
        return Window::Type::FULLSCREEN;

    if (flags & SDL_WINDOW_BORDERLESS)
        return Window::Type::BORDERLESS;

    return Window::Type::WINDOWED;
}

bool Window::isResizeable() const {
    return SDL_GetWindowFlags(this->getHandle()) & SDL_WINDOW_RESIZABLE;
}

SDL_Window *Window::getHandle() const {
    return this->window;
}

void Engine3D::Window::setWindow(SDL_Window *window) {
    Window::window = window;
}
