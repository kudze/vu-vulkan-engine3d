//
// Created by karol on 4/6/2020.
//

#ifndef INC_3DENGINE_SCENE_H
#define INC_3DENGINE_SCENE_H

#include <unordered_map>
#include <vector>

#include "entity.h"
#include "root_entity.h"
#include "../vulkan/vulkan_graphics_pipeline.h"

namespace Engine3D {
    class Engine;
    class Camera;
    class Object;

    /**
     * Class that descibes renderable scene.
     */
    class Scene : public NonCopyable {
        friend class SceneCameraComponent;

        /**
         * Pointer to a root entity of the scene.
         */
        RootEntity* rootEntity = nullptr;

        /**
         * Pointer to a camera for the scene.
         */
        Camera* camera = nullptr;

        /**
         * Pointer to rendering engine.
         */
        Engine* engine = nullptr;

        /**
         * This contains all objects mapped in scene with sorted to their pipeline.
         */
         std::unordered_map<Vulkan::graphicsPipelineIndex_t, std::vector<Object*>> objectsMappedToPipelines;

        void setCamera(Camera* camera);

    public:

        /**
         * Default constructor
         *
         * @param engine
         */
        explicit Scene(Engine* engine);

        /**
         * More explicit constructor.
         *
         * @param engine
         * @param rootEntity
         * @param camera
         */
        Scene(Engine* engine, Camera* camera);

        /**
         * Destructor
         */
        virtual ~Scene();

        /**
         * a function which is called every tick
         */
        void update();

        void registerSceneObject(Object* object);
        void unregisterSceneObject(Object* object);

        /**
         * Root entity getter.
         *
         * @return
         */
        RootEntity* getRootEntity() const;

        /**
         * Returns wherether scene has camera
         *
         * @return
         */
        bool hasCamera() const;

        /**
         * Camera getter
         *
         * @return
         */
        Camera* getCamera() const;

        /**
         * Engine getter.
         *
         * @return
         */
        Engine* getEngine() const;

        /**
         * Returns all scene objects mapped to their own pipelines.
         *
         * @return
         */
        std::unordered_map<Vulkan::graphicsPipelineIndex_t, std::vector<Object*>> const& getObjectsMappedToPipelines() const;
    };
}

#include "camera.h"
#include "object.h"
#include "../engine.h"


#endif //INC_3DENGINE_SCENE_H
