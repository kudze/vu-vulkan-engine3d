//
// Created by karol on 4/14/2020.
//

#include "entity_component.h"
#include "entity.h"

using namespace Engine3D;

void EntityComponent::setEntity(Entity* entity)
{
    this->entity = entity;
}

EntityComponent::EntityComponent()
{

}

EntityComponent::EntityComponent(Engine3D::Entity *entity)
{
    entity->addComponent(this);
}

EntityComponent::~EntityComponent() = default;

Entity * EntityComponent::getEntity() const {
    return this->entity;
}
