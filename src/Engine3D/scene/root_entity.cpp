//
// Created by karol on 5/6/2020.
//

#include "root_entity.h"

using namespace Engine3D;

RootEntity::RootEntity(Scene* scene)
    : scene(scene)
{

}

Scene* RootEntity::getScene() const {
    return this->scene;
}

Entity::Type RootEntity::getType() const {
    return Entity::TYPE_ROOT_ENTITY;
}