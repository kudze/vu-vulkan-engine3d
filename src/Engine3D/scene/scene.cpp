//
// Created by karol on 4/6/2020.
//

#include <chrono>

#include "scene.h"
#include "../engine.h"

using namespace Engine3D;

Scene::Scene(Engine* engine)
    : Scene(engine, nullptr)
{

}

Scene::Scene(Engine* engine, Camera* camera)
    : rootEntity(new RootEntity(this)), camera(camera), engine(engine) {

}

Scene::~Scene() {
    delete this->rootEntity;
}

void Scene::update() {
    static auto lastTime = std::chrono::high_resolution_clock::now();
    auto now = std::chrono::high_resolution_clock::now();

    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(now - lastTime);
    float delta = duration.count() / 1000.0f;

    lastTime = now;

    this->rootEntity->update(delta);
}

void Scene::registerSceneObject(Object *object) {
    this->objectsMappedToPipelines[object->getGraphicsPipelineIndex()].push_back(object);
}

void Scene::unregisterSceneObject(Object *object) {
    auto& objects = this->objectsMappedToPipelines.at(object->getGraphicsPipelineIndex());

    auto it = std::find(objects.begin(), objects.end(), object);
    if(it == objects.end())
        throw std::runtime_error("unregisterSceneObject called for unregistered scene object!");

    objects.erase(it);
}

RootEntity * Scene::getRootEntity() const {
    return this->rootEntity;
}

void Scene::setCamera(Engine3D::Camera *camera) {
    if(this->camera != nullptr && camera != nullptr)
        throw std::runtime_error("Scene cannot have two cameras!");

    this->camera = camera;
}

bool Scene::hasCamera() const {
    return this->camera != nullptr;
}

Camera * Scene::getCamera() const {
    return this->camera;
}

Engine * Scene::getEngine() const {
    return this->engine;
}

const std::unordered_map<Vulkan::graphicsPipelineIndex_t, std::vector<Object *> > & Scene::getObjectsMappedToPipelines() const {
    return this->objectsMappedToPipelines;
}