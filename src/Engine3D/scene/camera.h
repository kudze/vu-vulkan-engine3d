//
// Created by karol on 4/7/2020.
//

#ifndef INC_3DENGINE_CAMERA_H
#define INC_3DENGINE_CAMERA_H

#include "glm.hpp"
#include "../vulkan/vulkan_window.h"
#include "entity.h"

#include <iostream>

namespace Engine3D {

    /**
     * Camera class.
     */
    class Camera
        : public Entity
    {
        /**
         * Pointer to Vulkan::Window class.
         */
        Vulkan::Window* window;

        /**
         * Field of view. (Visible x angle from camera.)
         */
        float fov;

    public:

        /**
         * Constructor
         *
         * @param window
         * @param fov
         */
        Camera(Vulkan::Window* window, float fov);

        /**
         * Destructor.
         */
        ~Camera();

        /**
         * Rotation getter.
         *
         * @return glm::vec3 const&
         */
        glm::vec3 const& getRotation() const = delete;

        /**
         * Rotation getter that returns a reference which could be edited.
         *
         * @return glm::vec3&
         */
        glm::vec3& getRotation() = delete;

        /**
         * Rotation setter
         *
         * @param rotation
         */
        void setRotation(const glm::vec3 &rotation) = delete;

        /**
         * Look at getter.
         *
         * @return glm::vec3 const&
         */
        glm::vec3 const& getLookAt() const;

        /**
         * Look at getter that returns a reference which could be edited.
         *
         * @return glm::vec3&
         */
        glm::vec3& getLookAt();

        /**
         * Look at setter
         *
         * @param rotation
         */
        void setLookAt(const glm::vec3 &rotation);

        /**
         * Rotates camera
         */
        void rotate(float amount, glm::vec3 const& axis, float degToClampAtZ);

        /**
         * Calculates View (Camera) matrix.
         *
         * @return glm::mat4
         */
        glm::mat4 calculateViewMatrix() const;

        /**
         * Calculates projection (Perspective) matrix.
         *
         * @return glm::mat4
         */
        glm::mat4 calculateProjectionMatrix() const;

        /**
         * Calculates front vector.
         *
         * @return
         */
        glm::vec3 calculateFrontVector() const;

        /**
         * Calculates Back vector.
         *
         * @return
         */
        glm::vec3 calculateBackVector() const;

        /**
         * Calculates up vector.
         *
         * @return
         */
        glm::vec3 calculateUpVector() const;

        /**
         * Calculates down vector.
         *
         * @return
         */
        glm::vec3 calculateDownVector() const;

        /**
         * Calculates left vector.
         *
         * @return
         */
        glm::vec3 calculateLeftVector() const;

        /**
         * Calculates right vector.
         *
         * @return
         */
        glm::vec3 calculateRightVector() const;

        /**
         * Field of view setter.
         *
         * @param fov
         */
        void setFieldOfView(float fov);

        /**
         * Window setter.
         *
         * @param window
         */
        void setWindow(Vulkan::Window* window);

        /**
         * Field of view getter.
         *
         * @return float
         */
        float getFieldOfView() const;

        /**
         * Window getter.
         *
         * @return Vulkan::Window*
         */
        Vulkan::Window* getWindow() const;

        /**
         * Returns type of entity.
         * In this instance will always return camera.
         *
         * @return
         */
        Type getType() const override;

        ///<< operator (prints vertex to ostream)
        ///@param os - output stream
        ///@param o - vertex to print
        ///@returns os
        friend std::ostream& operator<<(std::ostream& os, const Camera& o)
        {
            os  << o.getPosition().x << " " << o.getPosition().y << " " << o.getPosition().z
                << " "<< o.getLookAt().x << " " << o.getLookAt().y << " " << o.getLookAt().z
                << " " << o.fov;

            return os;
        }

        ///>> operator (reads vertex from istream)
        ///@param is - input stream
        ///@param o - vertex to read
        ///@returns is
        friend std::istream& operator>>(std::istream& is, Camera& o)
        {
            is >> o.getPosition().x >> o.getPosition().y >> o.getPosition().z >> o.getLookAt().x >> o.getLookAt().y >> o.getLookAt().z >> o.fov;

            return is;
        }
    };
}


#endif //INC_3DENGINE_CAMERA_H
