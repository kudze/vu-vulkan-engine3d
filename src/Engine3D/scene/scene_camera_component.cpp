//
// Created by karol on 5/6/2020.
//

#include "scene_camera_component.h"

using namespace Engine3D;

SceneCameraComponent::SceneCameraComponent(Engine3D::Camera *camera)
    : CameraComponent(camera)
{

}

SceneCameraComponent::~SceneCameraComponent() = default;

void SceneCameraComponent::onParentChange(Entity *oldParent, Entity *newParent) const {

    if(oldParent != nullptr)
        oldParent->findRootEntity()->getScene()->setCamera(nullptr);

    if(newParent != nullptr)
        newParent->findRootEntity()->getScene()->setCamera(this->getCamera());

}

void SceneCameraComponent::onUpdate(float deltaTime) const {

}

void SceneCameraComponent::onChildAdded(Entity *entity) const {

}

void SceneCameraComponent::onChildRemoved(Entity *entity) const {

}
