//
// Created by karol on 5/7/2020.
//

#include "camera_component.h"

using namespace Engine3D;

CameraComponent::CameraComponent()
    : EntityComponent()
{

}

CameraComponent::CameraComponent(Camera* camera)
    : EntityComponent(camera)
{

}

CameraComponent::~CameraComponent() = default;

Camera * CameraComponent::getCamera() const {
    return (Camera*) this->getEntity();
}