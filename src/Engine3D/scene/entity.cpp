//
// Created by karol on 4/6/2020.
//

#include "entity.h"
#include "gtc/matrix_transform.hpp"

#include <stdexcept>
#include <algorithm>

using namespace Engine3D;

Entity::Entity() = default;

Entity::~Entity()
{

    for(auto child : this->children)
        delete child;

    this->children.clear();

    for(auto component : this->components)
        delete component;

    this->components.clear();

};

Entity::Type Entity::getType() const
{
    return Entity::Type::TYPE_ENTITY;
}

glm::vec3& Entity::getPosition()
{
    return position;
}

const glm::vec3& Entity::getPosition() const
{
    return position;
}

void Entity::setPosition(const glm::vec3& position)
{
    Entity::position = position;
}

glm::vec3& Entity::getRotation()
{
    return rotation;
}

const glm::vec3& Entity::getRotation() const
{
    return rotation;
}

void Entity::setRotation(const glm::vec3& rotation)
{
    Entity::rotation = rotation;
}

glm::vec3& Entity::getScale()
{
    return scale;
}

const glm::vec3& Entity::getScale() const
{
    return scale;
}

void Entity::setScale(const glm::vec3& scale)
{
    Entity::scale = scale;
}

const std::vector<Entity*>& Entity::getChildren() const
{
    return this->children;
}

const std::vector<EntityComponent*>& Entity::getComponents() const
{
    return this->components;
}

std::vector<Entity const*> Entity::getParentTreeConst() const
{
    std::vector<Entity const*> result;
    Entity const* curr = this;

    while(!curr->isRootEntity()) {
        result.push_back(curr);
        curr = curr->getParent();
    }

    result.push_back(curr);

    return result;
}

std::vector<Entity*> Entity::getParentTree()
{
    std::vector<Entity*> result;
    Entity* curr = this;

    while(!curr->isRootEntity()) {
        result.push_back(curr);
        curr = curr->getParent();
    }

    result.push_back(curr);

    return result;
}

Entity* Entity::getFirstParentOfType(Entity::Type type) const
{
    if(!this->isRootEntity()) {
        Entity* curr = this->getParent();

        while(!curr->isRootEntity()) {
            if(curr->getType() == type)
                return curr;

            curr = curr->getParent();
        }
    }

    throw std::runtime_error("Entity could not find first entity of type: " + std::to_string(type));
}

void Entity::setChildren(const std::vector<Entity*>& children)
{
    this->children = children;
}

void Entity::addChild(Entity* child)
{
    if(child->parent != nullptr)
        throw std::runtime_error("Entity can't have two parents!");

    std::vector<Entity*> parentTree = this->getParentTree();
    if(std::find(parentTree.begin(), parentTree.end(), child) != parentTree.end())
        throw std::runtime_error("Circular inheritance is not supported!");

    this->children.push_back(child);

    for(auto component : child->getComponents())
        component->onParentChange(child->parent, this);

    child->parent = this;

    for(auto component : this->getComponents())
        component->onChildAdded(child);

    if(child->getType() == Entity::Type::TYPE_OBJECT)
        this->findRootEntity()->getScene()->registerSceneObject((Object*) child);
}

void Entity::removeChild(size_t index)
{
    auto child = this->children.at(index);

    for(auto component : child->getComponents())
        component->onParentChange(child->parent, nullptr);

    for(auto component : this->getComponents())
        component->onChildRemoved(child);

    if(child->getType() == Entity::Type::TYPE_OBJECT)
        this->findRootEntity()->getScene()->unregisterSceneObject((Object*) child);

    child->parent = nullptr;

    this->children.erase(this->children.begin() + index);
}

void Entity::removeChild(Entity* child)
{
    this->removeChild(
        this->findChildIndex(child)
    );
}

size_t Entity::findChildIndex(Entity* child) const
{
    auto it = std::find(this->children.begin(), this->children.end(), child);

    if(it == this->children.end())
        throw std::runtime_error("Such children does not exist!");

    return it - this->children.begin();
}

void Entity::setComponents(const std::vector<EntityComponent*>& components)
{
    this->components = components;
}

void Entity::addComponent(EntityComponent* component)
{
    if(component->getEntity() != nullptr)
        throw std::runtime_error("This entity component already belongs to another entity!");

    this->components.push_back(component);

    component->setEntity(this);
}

void Entity::removeComponent(EntityComponent* component)
{
    this->removeComponent(
        this->findComponentIndex(component)
    );
}

void Entity::removeComponent(size_t index)
{
    auto component = *(this->components.begin() + index);

    component->setEntity(nullptr);

    this->components.erase(this->components.begin() + index);
}

size_t Entity::findComponentIndex(EntityComponent* component) const
{
    auto it = std::find(this->components.begin(), this->components.end(), component);

    if(it == this->components.end())
        throw std::runtime_error("Such component does not exist!");

    return it - this->components.begin();
}

Entity* Entity::getParent() const
{
    return this->parent;
}

RootEntity* Entity::findRootEntity()
{
    Entity const* entity = this;

    while(!entity->isRootEntity())
        entity = entity->getParent();

    if(entity->getType() != TYPE_ROOT_ENTITY)
        throw std::runtime_error("Root entity of the tree isn't type of ROOT_ENITTY");

    return (RootEntity*) entity;
}

bool Entity::isRootEntity() const
{
    return this->parent == nullptr;
}

glm::mat4 Entity::calculateModelMatrix() const
{
    auto position = glm::identity<glm::mat4>(), rotation = glm::identity<glm::mat4>(), scale = glm::identity<glm::mat4>();

    scale = glm::scale(scale, this->getScale());
    rotation = glm::rotate(rotation, this->getRotation().x, glm::vec3(1, 0, 0));
    rotation = glm::rotate(rotation, this->getRotation().y, glm::vec3(0, 1, 0));
    rotation = glm::rotate(rotation, this->getRotation().z, glm::vec3(0, 0, 1));
    position = glm::translate(position, this->getPosition());

    return position * rotation * scale;
}

void Entity::update(float deltaTime)
{
    for(auto component : this->components)
        component->onUpdate(deltaTime);

    for(size_t i = 0; i < this->children.size(); i++)
        this->children.at(i)->update(deltaTime);
}
