//
// Created by karol on 5/6/2020.
//

#ifndef INC_3DENGINE_ROOT_ENTITY_H
#define INC_3DENGINE_ROOT_ENTITY_H

#include "entity.h"
#include "../util/non_copyable.h"

namespace Engine3D {
    class Scene;

    class RootEntity
        : public Entity
    {
        Scene* scene = nullptr;

    public:

        explicit RootEntity(Scene* scene);

        Scene* getScene() const;

        /**
         * Builds a list of all parents.
         * this entity is included in the list.
         * entity->parent->parent->....->parent till is null with all parents included
         * NOTE: this is disabled for RootEntity.
         *
         * @return std::vector<Entity const*>
         */
        std::vector<Entity const*> getParentTreeConst() const = delete;

        /**
        * Builds a list of all parents.
        * this entity is included in the list.
        * entity->parent->parent->....->parent till is null with all parents included
        * NOTE: this is disabled for RootEntity.
        *
        * @return std::vector<Entity*>
        */
        std::vector<Entity*> getParentTree() = delete;

        /**
         * Virtual function that returns the type of entity.
         *
         * @return
         */
        virtual Type getType() const;
    };

}

#include "scene.h"


#endif //INC_3DENGINE_ROOT_ENTITY_H
