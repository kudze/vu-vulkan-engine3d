//
// Created by karol on 4/7/2020.
//

#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#include <gtc/matrix_transform.hpp>
#include <gtx/rotate_vector.hpp>
#include "camera.h"
#include "scene_camera_component.h"

using namespace Engine3D;

Camera::Camera(Vulkan::Window *window, float fov)
    : window(window), fov(fov) {
    new SceneCameraComponent(this);

    this->setLookAt(glm::vec3(1, 0, 0));
};

Camera::~Camera() = default;

float Camera::getFieldOfView() const {
    return this->fov;
}

Vulkan::Window *Camera::getWindow() const {
    return this->window;
}

Entity::Type Camera::getType() const {
    return Entity::TYPE_CAMERA;
}

void Camera::setFieldOfView(float fov) {
    this->fov = fov;
}

void Camera::setWindow(Vulkan::Window *window) {
    this->window = window;
}

glm::mat4 Camera::calculateProjectionMatrix() const {
    auto size = this->getWindow()->getDrawableSize();
    float aspectRatio = ((float) size.getWidth()) / ((float) size.getHeight());

    glm::mat4 result = glm::perspective(glm::radians(this->getFieldOfView()), aspectRatio, 0.01f, 1000.0f);
    result[1][1] *= -1;

    return result;
}

glm::vec3 Camera::calculateFrontVector() const {
    auto viewMatrix = this->calculateViewMatrix();
    viewMatrix = glm::inverse(viewMatrix);

    return -glm::normalize(glm::vec3(viewMatrix[2]));
}

glm::vec3 Camera::calculateBackVector() const {
    return this->calculateFrontVector() * -1.f;
}

glm::vec3 Camera::calculateUpVector() const {
    return glm::vec3(0, 0, 1);
}

glm::vec3 Camera::calculateDownVector() const {
    return glm::vec3(0, 0, -1);
}

glm::vec3 Camera::calculateLeftVector() const {
    return glm::normalize(-glm::cross(this->calculateFrontVector(), this->calculateUpVector()));
}

glm::vec3 Camera::calculateRightVector() const {
    return -this->calculateLeftVector();
}

glm::mat4 Camera::calculateViewMatrix() const {
    return glm::lookAt(this->getPosition(), this->getPosition() + (this->getLookAt() * 0.01f), glm::vec3(0, 0, 1));
}

glm::vec3 &Camera::getLookAt() {
    return this->rotation;
}

const glm::vec3 &Camera::getLookAt() const {
    return this->rotation;
}

void Camera::setLookAt(const glm::vec3 &rotation) {
    this->rotation = rotation;
}

#include <iostream>

void Camera::rotate(float amount, const glm::vec3 &axis, float degToClampAtZ) {
    glm::vec3 oldRot = this->rotation;

    this->rotation = glm::rotate(this->rotation, amount, axis);

    glm::vec3 centerRotation = this->rotation;
    centerRotation.z = 0;
    centerRotation = glm::normalize(centerRotation);

    auto dot = glm::dot(centerRotation, this->rotation);
    auto angle = acos(dot);

    if (angle > degToClampAtZ)
        this->rotation = oldRot;
}