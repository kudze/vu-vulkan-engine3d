//
// Created by karol on 4/6/2020.
//

#ifndef INC_3DENGINE_ENTITY_H
#define INC_3DENGINE_ENTITY_H

#include <glm.hpp>
#include <vector>

#include "entity_component.h"

namespace Engine3D {

    class RootEntity;

    /**
     * Entity class
     * (Could be also called GameObject)
     */
    class Entity : public NonCopyable {
    protected:
        /**
         * Position of entity.
         */
        glm::vec3 position = glm::vec3(0, 0, 0);

        /**
         * Position of rotation.
         */
        glm::vec3 rotation = glm::vec3(0, 0, 0);

        /**
         * Position of scale.
         */
        glm::vec3 scale = glm::vec3(1, 1, 1);

    private:
        /**
         * Parent entity.
         * Root entity will have this set to nullptr.
         */
        Entity* parent = nullptr;

        /**
         * Childrens of the entity.
         */
        std::vector<Entity*> children;

        /**
         * Components of an entity.
         */
        std::vector<EntityComponent*> components;

    public:
        /**
         * Entity type enum.
         */
        enum Type {
            TYPE_ENTITY, //Entity
            TYPE_ROOT_ENTITY, //Root entity
            TYPE_OBJECT, //Object
            TYPE_CAMERA, //Camera
            TYPE_CANVAS, //Canvas
        };

        /**
         * Constructor
         */
        Entity();

        /**
         * Copy
         */
         Entity(Entity const& entity);

        /**
         * Destructor
         */
        virtual ~Entity();

        /**
         * virtual function that is called on every tick.
         * if Entity is extended this is required to be called.
         *
         * @param deltaTime - passed time since last tick (seconds)
         */
        virtual void update(float deltaTime);

        /**
         * Position getter.
         *
         * @return glm::vec3 const&
         */
        glm::vec3 const& getPosition() const;

        /**
         * Rotation getter.
         *
         * @return glm::vec3 const&
         */
        glm::vec3 const& getRotation() const;

        /**
         * Scale getter
         *
         * @return glm::vec3 const&
         */
        glm::vec3 const& getScale() const;

        /**
         * Position getter that returns a reference which could be edited.
         *
         * @return glm::vec3&
         */
        glm::vec3& getPosition();

        /**
         * Rotation getter that returns a reference which could be edited.
         *
         * @return glm::vec3&
         */
        glm::vec3& getRotation();

        /**
         * Scale getter that returns a reference which could be edited.
         *
         * @return glm::vec3&
         */
        glm::vec3& getScale();

        /**
         * Parent getter.
         * Root entity will return nullptr.
         *
         * @return Entity*
         */
        Entity* getParent() const;

        /**
         * A function which retrieves root entity from inheritance tree.
         *
         * @return
         */
        RootEntity* findRootEntity();

        /**
         * Function to check if entity is a root entity.
         *
         * @return true if entity is a root entity
         */
        bool isRootEntity() const;

        /**
         * Children getter.
         *
         * @return std::vector<Entity*> const&
         */
        std::vector<Entity*> const& getChildren() const;

        /**
         * Components getter.
         *
         * @return std::vector<EntityComponent*> const&
         */
        std::vector<EntityComponent*> const& getComponents() const;

        /**
         * Builds a list of all parents.
         * this entity is included in the list.
         * entity->parent->parent->....->parent till is null with all parents included
         *
         * @return std::vector<Entity const*>
         */
        std::vector<Entity const*> getParentTreeConst() const;

        /**
         * Returns first found parent entity of type
         */
        Entity* getFirstParentOfType(Entity::Type type) const;

        /**
        * Builds a list of all parents.
        * this entity is included in the list.
        * entity->parent->parent->....->parent till is null with all parents included
        *
        * @return std::vector<Entity*>
        */
        std::vector<Entity*> getParentTree();

        /**
         * Position setter
         *
         * @param position
         */
        void setPosition(const glm::vec3 &position);

        /**
         * Rotation setter
         *
         * @param rotation
         */
        void setRotation(const glm::vec3 &rotation);

        /**
         * Scale setter
         *
         * @param scale
         */
        void setScale(const glm::vec3 &scale);

        /**
         * Children setter
         *
         * @param children
         */
        void setChildren(std::vector<Entity*> const& children);

        /**
         * Adds a children to entity
         *
         * @param child
         */
        void addChild(Entity* child);

        /**
         * Removes children from an entity. (By pointer)
         *
         * @param child
         */
        void removeChild(Entity* child);

        /**
         * Removes children from an entity. (By index)
         *
         * @param index
         */
        void removeChild(size_t index);

        /**
         * Finds children index from an children pointer.
         *
         * @param child
         * @return size_t - index
         */
        size_t findChildIndex(Entity* child) const;

        /**
         * Components setter.
         *
         * @param components
         */
        void setComponents(std::vector<EntityComponent*> const& components);

        /**
         * Adds component to an entity.
         *
         * @param component
         */
        void addComponent(EntityComponent* component);

        /**
         * Removes component from an entity (by pointer)
         *
         * @param component
         */
        void removeComponent(EntityComponent* component);

        /**
         * Removes component from an entity (by index)
         *
         * @param index
         */
        void removeComponent(size_t index);

        /**
         * Finds index of a component from component pointer
         *
         * @param component
         * @return size_t - index
         */
        size_t findComponentIndex(EntityComponent* component) const;

        /**
         * Calculates and returns model matrix.
         *
         * @return
         */
        glm::mat4 calculateModelMatrix() const;

        /**
         * Virtual function that returns the type of entity.
         *
         * @return
         */
        virtual Type getType() const;
    };
}

#include "root_entity.h"

#endif //INC_3DENGINE_ENTITY_H
