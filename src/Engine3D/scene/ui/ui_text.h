//
// Created by karol on 5/19/2020.
//

#ifndef INC_3DENGINE_UI_TEXT_H
#define INC_3DENGINE_UI_TEXT_H

#include "../object.h"
#include "../../vulkan/vulkan_font.h"

namespace Engine3D::UI {
    class Canvas;

    class Text
        : public Object
    {
        Vulkan::Font* font;
        std::string text;
        glm::vec3 textColor;
        std::vector<Vulkan::TextUniformBufferLayout*> textureUniforms;

        /**
         * Updates model.
         * Should only be called upon initalization and text change.
         */
        void updateModel();

    public:
        Text(
            Vulkan::Renderer* renderingEngine,
            Vulkan::Font* font,
            std::string const& text,
            glm::vec3 const& textColor
        );
        virtual ~Text();

        void setText(std::string const& text);
        void setTextColor(glm::vec3 const& textColor);

        void updateBufferData(Camera* camera, uint32_t imageIndex) override;

        Canvas* getCanvas() const;
        Vulkan::Font* getFont() const;
        std::string const& getText() const;
        glm::vec3 const& getTextColor() const;
        std::vector<Vulkan::TextUniformBufferLayout*> const& getTextureUniformBuffers() const;
    };
}

#include "ui_canvas.h"

#endif //INC_3DENGINE_UI_TEXT_H
