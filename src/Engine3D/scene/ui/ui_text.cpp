//
// Created by karol on 5/19/2020.
//

#include <ext/matrix_transform.hpp>
#include "ui_text.h"

using namespace Engine3D;
using namespace UI;

Text::Text(
    Vulkan::Renderer *renderingEngine,
    Vulkan::Font *font,
    std::string const &text,
    glm::vec3 const &textColor
) : font(font), text(text), textColor(textColor),
    Object(new Vulkan::Model(nullptr, nullptr), renderingEngine, TEXT_PIPELINE_INDEX, nullptr) {
}

Text::~Text() {

};

void Text::updateModel() {
    auto rootEntity = this->findRootEntity();
    auto model = this->getModel();

    delete model->getVertexBuffer();
    delete model->getIndexBuffer();
    for (auto textureBuffer : this->textureUniforms)
        delete textureBuffer;
    this->textureUniforms.clear();

    std::vector<Vulkan::Vertex> vertices;
    std::vector<Vulkan::index_t> indices;

    float x = 0, y = 0;
    for (size_t i = 0; i < this->getText().size(); i++) {
        char letter = this->getText().at(i);

        if(letter != ' ') {
            auto characterInfo = this->getFont()->getCharacterInfo(letter);

            this->textureUniforms.push_back(
                new Vulkan::TextUniformBufferLayout(
                    rootEntity->getScene()->getEngine()->getRenderer()->getDevice(),
                    rootEntity->getScene()->getEngine()->getRenderer()->getDescriptorPool(),
                    rootEntity->getScene()->getEngine()->getRenderer()->getGraphicsPipelines().at(
                        this->getGraphicsPipelineIndex()),
                    characterInfo->getImage()
                )
            );

            float xpos = x + characterInfo->getBearing().x;
            float ypos = y - (characterInfo->getSize().y - characterInfo->getBearing().y);

            float w = characterInfo->getSize().x;
            float h = -characterInfo->getSize().y;

            size_t verticeIndex = vertices.size();

            vertices.emplace_back(
                Vulkan::Vertex(glm::vec3(xpos, ypos + h, 0.f), glm::vec3(1.f, 1.f, 1.f), glm::vec2(0.f, 0.f)));
            vertices.emplace_back(
                Vulkan::Vertex(glm::vec3(xpos, ypos, 0.f), glm::vec3(1.f, 1.f, 1.f), glm::vec2(0.f, 1.f)));
            vertices.emplace_back(
                Vulkan::Vertex(glm::vec3(xpos + w, ypos, 0.f), glm::vec3(1.f, 1.f, 1.f), glm::vec2(1.f, 1.f)));
            vertices.emplace_back(
                Vulkan::Vertex(glm::vec3(xpos + w, ypos + h, 0.f), glm::vec3(1.f, 1.f, 1.f), glm::vec2(1.f, 0.f)));

            /*indices.push_back(verticeIndex + 2);
            indices.push_back(verticeIndex + 1);
            indices.push_back(verticeIndex);
            indices.push_back(verticeIndex + 3);
            indices.push_back(verticeIndex + 2);
            indices.push_back(verticeIndex);*/

            indices.push_back(verticeIndex);
            indices.push_back(verticeIndex + 1);
            indices.push_back(verticeIndex + 2);
            indices.push_back(verticeIndex);
            indices.push_back(verticeIndex + 2);
            indices.push_back(verticeIndex + 3);

            x += (characterInfo->getAdvance() >> 6);
        }

        else
            x += 10;
    }

    model->setVertexBuffer(
        new Vulkan::VertexBuffer(
            rootEntity->getScene()->getEngine()->getRenderer()->getDevice(),
            rootEntity->getScene()->getEngine()->getRenderer()->getTransferCommandPool(),
            vertices.data(),
            static_cast<uint32_t>(vertices.size())
        )
    );

    model->setIndexBuffer(
        new Vulkan::IndexBuffer(
            rootEntity->getScene()->getEngine()->getRenderer()->getDevice(),
            rootEntity->getScene()->getEngine()->getRenderer()->getTransferCommandPool(),
            indices.data(),
            static_cast<uint32_t>(indices.size())
        )
    );
}

void Text::setText(const std::string &text) {
    this->text = text;

    this->updateModel();
}

void Text::setTextColor(glm::vec3 const &textColor) {
    this->textColor = textColor;
}

Canvas *Text::getCanvas() const {
    return (Canvas *) this->getFirstParentOfType(Entity::Type::TYPE_CANVAS);
}

Vulkan::Font *Text::getFont() const {
    return this->font;
}

const std::string &Text::getText() const {
    return this->text;
}

const glm::vec3 &Text::getTextColor() const {
    return this->textColor;
}

void Text::updateBufferData(Camera *camera, uint32_t imageIndex) {
    auto canvas = this->getCanvas();

    if (this->getModel()->getVertexBuffer() == nullptr || this->getModel()->getIndexBuffer() == nullptr)
        this->updateModel();

    Vulkan::GraphicsPipeline::UniformBufferObject data = {};
    data.model = this->calculateModelMatrix();
    data.view = glm::identity<glm::mat4>();
    data.proj = canvas->getOrthographicProjection();

    for(auto layout : this->getTextureUniformBuffers()) {
        layout->getMatrixBuffer()->mapMemory(&data);
        layout->getTextColorBuffer()->mapMemory(&this->textColor);
    }
}

const std::vector<Vulkan::TextUniformBufferLayout *> &Text::getTextureUniformBuffers() const {
    return this->textureUniforms;
}
