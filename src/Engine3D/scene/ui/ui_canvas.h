//
// Created by karol on 5/19/2020.
//

#ifndef INC_3DENGINE_UI_CANVAS_H
#define INC_3DENGINE_UI_CANVAS_H

#include "../entity.h"
#include "../../vulkan/vulkan_window.h"

namespace Engine3D::UI {
    class Canvas
        : public Entity {
        glm::mat4 orthographicProjection;
        bool visible = true;

    public:

        Canvas(Vulkan::Window* window);
        ~Canvas();

        virtual Type getType() const;

        void setVisible(bool visible);
        bool isVisible() const;

        glm::mat4 const& getOrthographicProjection() const;
    };
}


#endif //INC_3DENGINE_UI_CANVAS_H
