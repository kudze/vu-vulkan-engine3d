//
// Created by karol on 5/19/2020.
//

#include <glm.hpp>
#include <ext/matrix_clip_space.hpp>
#include "ui_canvas.h"

using namespace Engine3D;
using namespace UI;

Canvas::Canvas(Vulkan::Window* window)
    : Entity() {
    auto size = window->getDrawableSize();

    this->orthographicProjection = glm::ortho(0.f, (float) size.getWidth(), 0.f, (float) size.getHeight());
}

Canvas::~Canvas() = default;

Entity::Type Canvas::getType() const {
    return Entity::TYPE_CANVAS;
}

void Canvas::setVisible(bool visible) {
    this->visible = visible;
}

bool Canvas::isVisible() const {
    return this->visible;
}

glm::mat4 const& Canvas::getOrthographicProjection() const {
    return this->orthographicProjection;
}

