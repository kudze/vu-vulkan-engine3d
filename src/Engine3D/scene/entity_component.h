//
// Created by karol on 4/14/2020.
//

#ifndef INC_3DENGINE_ENTITY_COMPONENT_H
#define INC_3DENGINE_ENTITY_COMPONENT_H

#include "../util/non_copyable.h"

namespace Engine3D {
    class Entity;

    /**
     * Abstract class for creating components for entity.
     */
    class EntityComponent : public NonCopyable {
        friend class Entity;

        /**
         * Pointer to an entity.
         */
        Entity *entity = nullptr;

        void setEntity(Entity* entity);

    public:

        EntityComponent();

        /**
         * Constructor
         *
         * @param entity
         */
        explicit EntityComponent(
            Entity *entity
        );

        /**
         * Destructor
         */
        virtual ~EntityComponent();

        /**
         * Virtual function which is called on every tick.
         *
         * @param deltaTime - passed time since last tick (seconds)
         */
        virtual void onUpdate(float deltaTime) const = 0;

        /**
         * Virtual function which is called when child has been added to entity
         *
         * @param entity
         */
        virtual void onChildAdded(Entity* entity) const = 0;

        /**
         * Virtual function which is called when child has been removed from entity.
         *
         * @param entity
         */
        virtual void onChildRemoved(Entity* entity) const = 0;

        /**
         * Virtual function which is called when parent of the class changes
         */
         virtual void onParentChange(Entity* oldParent, Entity* newParent) const = 0;

        /**
         * Entity getter.
         *
         * @return
         */
        Entity *getEntity() const;
    };
}

#include "entity.h"


#endif //INC_3DENGINE_ENTITY_COMPONENT_H
