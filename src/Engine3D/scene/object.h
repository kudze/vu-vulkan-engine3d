//
// Created by karol on 4/6/2020.
//

#ifndef INC_3DENGINE_OBJECT_H
#define INC_3DENGINE_OBJECT_H

#include "entity.h"
#include "../vulkan/vulkan_model.h"
#include "../vulkan/vulkan_uniform_buffer.h"
#include "../vulkan/vulkan_graphics_pipeline.h"

namespace Engine3D {
    namespace Vulkan {
        class Renderer;
    }

    /**
     * Object. (3D Model in scene)
     *
     * @extends Engine3D::Entity
     */
    class Object
        : public Entity
    {

        /**
         * Pointer to a model.
         */
        Vulkan::Model* model;

        /**
         * Pointers to uniform buffers.
         */
        std::vector<Vulkan::UniformBufferLayoutInterface*> uniformBuffers;

        /**
         * Contains an pipeline index it's using.
         */
        Vulkan::graphicsPipelineIndex_t graphicsPipelineIndex;

    public:

        /**
         * Constructor
         *
         * @param model
         * @param renderingEngine
         */
        Object(Vulkan::Model* model, Vulkan::Renderer* renderingEngine, Vulkan::graphicsPipelineIndex_t graphicsPipelineIndex, Vulkan::Image* texture = nullptr);

        /**
         * Destructor
         */
        virtual ~Object();

        /**
         * Function which updates uniform buffers with the newest matrixes.
         *
         * @param camera
         * @param imageIndex
         */
        virtual void updateBufferData(Camera* camera, uint32_t imageIndex);

        /**
         * Model setter
         *
         * @param model
         */
        void setModel(Vulkan::Model* model);

        /**
         * Model getter.
         *
         * @return
         */
        Vulkan::Model* getModel() const;

        /**
         * Uniform buffers getter.
         *
         * @return
         */
        std::vector<Vulkan::UniformBufferLayoutInterface*> const& getUniformBuffers() const;

        /**
         * Returns graphics pipeline index.
         *
         * @return
         */
        Vulkan::graphicsPipelineIndex_t getGraphicsPipelineIndex() const;

        /**
         * Returns type of entity.
         *
         * @return
         */
        virtual Entity::Type getType() const override;
    };
}

#include "../vulkan/vulkan_renderer.h"

#endif //INC_3DENGINE_OBJECT_H
