//
// Created by karol on 5/6/2020.
//

#ifndef INC_3DENGINE_CAMERA_MOVEMENT_COMPONENT_H
#define INC_3DENGINE_CAMERA_MOVEMENT_COMPONENT_H

#include "camera_component.h"
#include "entity.h"

namespace Engine3D
{
    /**
     * A demo entity component that rotates entity on every tick.
     *
     * @extends Engine3D::EntityComponent
     */
    class SceneCameraComponent
        : public Engine3D::CameraComponent
    {
    public:

        /**
         * Constructor.
         *
         * @param entity
         */
        explicit SceneCameraComponent(Engine3D::Camera* entity);

        /**
         * Destructor
         */
        virtual ~SceneCameraComponent();


        /**
         * Virtual function which is called when parent of the class changes
         */
        virtual void onParentChange(Entity* oldParent, Entity* newParent) const override;

        void onUpdate(float deltaTime) const override;

        void onChildAdded(Entity *entity) const override;

        void onChildRemoved(Entity *entity) const override;

    };
}


#endif //INC_3DENGINE_CAMERA_MOVEMENT_COMPONENT_H
