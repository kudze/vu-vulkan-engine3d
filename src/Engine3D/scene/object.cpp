//
// Created by karol on 4/6/2020.
//

#include "object.h"

using namespace Engine3D;
using namespace Vulkan;

Object::Object(Engine3D::Vulkan::Model *model, Renderer *renderingEngine,
               Vulkan::graphicsPipelineIndex_t graphicsPipelineIndex, Vulkan::Image *texture)
    : Entity(), model(model), graphicsPipelineIndex(graphicsPipelineIndex) {

    this->uniformBuffers.resize(renderingEngine->getSwapchain()->getImageCount());
    for (auto &uniformBuffer : this->uniformBuffers) {
        if (graphicsPipelineIndex == TEXT_PIPELINE_INDEX) {
            /*uniformBuffer = new Vulkan::TextUniformBufferLayout(
                renderingEngine->getDevice(),
                renderingEngine->getDescriptorPool(),
                renderingEngine->getGraphicsPipelines().at(graphicsPipelineIndex)
            );*/
            uniformBuffer = nullptr;
        } else if (texture == nullptr) {
            uniformBuffer = new Vulkan::ColorUniformBufferLayout(
                renderingEngine->getDevice(),
                renderingEngine->getDescriptorPool(),
                renderingEngine->getGraphicsPipelines().at(graphicsPipelineIndex)
            );
        } else {
            uniformBuffer = new Vulkan::TextureUniformBufferLayout(
                renderingEngine->getDevice(),
                renderingEngine->getDescriptorPool(),
                renderingEngine->getGraphicsPipelines().at(graphicsPipelineIndex),
                texture
            );
        }
    }

}

Object::~Object() {

    for (auto uniformBuffer : this->uniformBuffers)
        delete uniformBuffer;

};

void Object::updateBufferData(Camera *camera, uint32_t imageIndex) {
    Vulkan::GraphicsPipeline::UniformBufferObject data = {};
    data.model = this->calculateModelMatrix();
    data.view = camera->calculateViewMatrix();
    data.proj = camera->calculateProjectionMatrix();

    auto layout = this->uniformBuffers.at(imageIndex);

    if (this->getGraphicsPipelineIndex() == COLOR_PIPELINE_INDEX)
        ((ColorUniformBufferLayout *) layout)->getMatrixBuffer()->mapMemory(&data);

    else
        ((TextureUniformBufferLayout *) layout)->getMatrixBuffer()->mapMemory(&data);
}

Entity::Type Object::getType() const {
    return Entity::Type::TYPE_OBJECT;
}

void Object::setModel(Vulkan::Model *model) {
    this->model = model;
}

Vulkan::Model *Object::getModel() const {
    return this->model;
}

const std::vector<Vulkan::UniformBufferLayoutInterface *> &Object::getUniformBuffers() const {
    return this->uniformBuffers;
}

Vulkan::graphicsPipelineIndex_t Object::getGraphicsPipelineIndex() const {
    return this->graphicsPipelineIndex;
}