//
// Created by karol on 5/7/2020.
//

#ifndef INC_3DENGINE_CAMERA_COMPONENT_H
#define INC_3DENGINE_CAMERA_COMPONENT_H

#include "entity_component.h"

namespace Engine3D {
    class Camera;

    class CameraComponent
        : public EntityComponent {

    public:
        /**
        * Constructor
        *
        * @param entity
        */
        CameraComponent();

        /**
        * Constructor
        *
        * @param entity
        */
        explicit CameraComponent(
            Camera *camera
        );

        /**
         * Destructor
         */
        virtual ~CameraComponent();

        Camera* getCamera() const;

    };
}

#include "camera.h"


#endif //INC_3DENGINE_CAMERA_COMPONENT_H
