//
// Created by kkraujelis on 2020-02-28.
//

#ifndef INC_3DENGINE_DIMENSIONS_HPP
#define INC_3DENGINE_DIMENSIONS_HPP

#include "dimensions.h"

template <typename T>
Engine3D::Dimensions<T>::Dimensions(T width, T height)
    : width(std::move(width)), height(std::move(height))
{

}

template <typename T>
Engine3D::Dimensions<T>::Dimensions(Dimensions<T> const& dimension)
    : width(dimension.getWidth()), height(dimension.getHeight())
{

}

template <typename T>
Engine3D::Dimensions<T>::~Dimensions() {

};

template <typename T>
T Engine3D::Dimensions<T>::getWidth() const {
    return this->width;
}

template <typename T>
T Engine3D::Dimensions<T>::getHeight() const {
    return this->height;
}

template <typename T>
Engine3D::Dimensions<T> Engine3D::Dimensions<T>::operator++() {
    return Dimensions<T>(++this->width, ++this->height);
}

template <typename T>
Engine3D::Dimensions<T> Engine3D::Dimensions<T>::operator++(int) {
    return Dimensions<T>(this->width++, this->height++);
}

template <typename T>
bool Engine3D::Dimensions<T>::operator==(const Dimensions<T> &o) const {
    return this->getHeight() == o.getHeight() && this->getWidth() == o.getWidth();
}

template <typename T>
bool Engine3D::Dimensions<T>::operator!=(const Dimensions<T> &o) const {
    return !this->operator==(o);
}

template<typename T>
void Engine3D::Dimensions<T>::setWidth(T width) {
    Dimensions::width = width;
}

template<typename T>
void Engine3D::Dimensions<T>::setHeight(T height) {
    Dimensions::height = height;
}

template <typename T>
Engine3D::Dimensions<T>& Engine3D::Dimensions<T>::operator=(Dimensions<T> const& dimensions)
{
    this->setWidth(dimensions.getWidth());
    this->setHeight(dimensions.getHeight());

    return *this;
}

#endif