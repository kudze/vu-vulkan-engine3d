//
// Created by kkraujelis on 2020-02-28.
//

#define TEMP

#ifndef INC_3DENGINE_DIMENSIONS_H
#define INC_3DENGINE_DIMENSIONS_H

namespace Engine3D
{

    ///This class purpose is to store 2D sizes.
    template <typename T>
    class Dimensions
    {
        private:
            ///@var width - width
            T width;
            ///@var height - height
            T height;

    public:
            ///Constructor
            ///@param width (x axis)
            ///@param height (y axis)
            Dimensions(T width, T height);

            ///Copy constructor
            ///@param dimensions
            Dimensions(Dimensions<T> const& dimensions);

            ///Destructor
            ~Dimensions();

            ///Getter for width
            ///@returns T
            T getWidth() const;

            ///Getter for height
            ///@returns T
            T getHeight() const;

            ///Setter for width
            ///@param width
            void setWidth(T width);

            ///Setter for height
            ///@param height
            void setHeight(T height);

            ///Copy operator=
            ///@param dimensions
            Dimensions<T>& operator=(Dimensions<T> const& dimensions);

            /**
             * Extends size by 1 in X and Y axis.
             */
            Dimensions<T> operator++();

            /**
             * Extends size by 1 in X and Y axis.
             */
            Dimensions<T> operator++(int);

            ///== operator (checks if dimensions are equal)
            ///@param o - another dimension
            bool operator==(Dimensions<T> const& o) const;

            ///!= operator (checks if dimensions are not equal)
            ///@param o - another dimension
            bool operator!=(Dimensions<T> const& o) const;

            std::string toString() const {
                return std::to_string(this->getWidth()) + " " + std::to_string(this->getHeight());
            }

            ///<< operator (prints dimension to ostream)
            ///@param os - output stream
            ///@param o - dimension to print
            ///@returns os
            friend std::ostream& operator<<(std::ostream& os, const Dimensions<T>& o)
            {
                os << o.toString();

                return os;
            }

            ///>> operator (reads dimension from istream)
            ///@param is - input stream
            ///@param o - dimension to read
            ///@returns is
            friend std::istream& operator>>(std::istream& is, Dimensions<T>& o)
            {
                is >> o.width >> o.height;

                return is;
            }
    };

}

#include "dimensions.hpp"

#endif //INC_3DENGINE_DIMENSIONS_H
