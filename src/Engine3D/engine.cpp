//
// Created by karol on 5/6/2020.
//

#include <chrono>

#include "engine.h"
#include "scene/object.h"

using namespace Engine3D;

Engine::Engine(Window* window)
    : window(window)
{
    this->renderer = new Vulkan::Renderer(this);
    this->inputEngine = new InputEngine(this);

    this->scene = new Scene(this);
}

Engine::~Engine() {
    delete this->scene;
    delete this->renderer;
    delete this->inputEngine;
    delete this->window;
}

void Engine::update() {
    this->getInputEngine()->pollEvents();
    this->getScene()->update();
    this->getRenderer()->render();
}

void Engine::run() {
    while (this->shouldRun()) {
        this->update();
    }
}

void Engine::stop() {
    this->running = false;
}

void Engine::setScene(Scene *scene) {
    delete this->scene;
    this->scene = scene;
}

Scene* Engine::getScene() const {
    return this->scene;
}

bool Engine::shouldRun() const {
    return this->running;
}

Window * Engine::getWindow() const {
    return this->window;
}

Vulkan::Renderer * Engine::getRenderer() const {
    return this->renderer;
}

InputEngine * Engine::getInputEngine() const {
    return this->inputEngine;
}

