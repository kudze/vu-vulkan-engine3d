//
// Created by kkraujelis on 2020-03-15.
//

#include "vulkan_swapchain.h"
#include "vulkan_image.h"
#include <iostream>
#include <algorithm>

using namespace Engine3D;
using namespace Vulkan;

VkSurfaceFormatKHR Swapchain::chooseSurfaceFormat(Engine3D::Vulkan::Surface* surface) const
{
    auto formats = surface->getAvailableFormats(
        this->device->getPhysicalDevice()
    );

    for(const auto& format : formats)
        if(format.format == VK_FORMAT_B8G8R8A8_SRGB && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
            return format;

    std::cout << "Preffered surface format not found..." << std::endl;

    return formats.at(0);
}

VkPresentModeKHR Swapchain::choosePresentMode(Engine3D::Vulkan::Surface* surface) const
{
    auto presentModes = surface->getAvailablePresentModes(
        this->device->getPhysicalDevice()
    );

    for(auto const& presentMode : presentModes)
        if(presentMode == VK_PRESENT_MODE_MAILBOX_KHR)
            return VK_PRESENT_MODE_MAILBOX_KHR;

    return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D Swapchain::chooseExtent(Engine3D::Vulkan::Surface* surface, Vulkan::Window* window) const
{
    auto capabilities = surface->getCapabilities(
        this->device->getPhysicalDevice()
    );

    if(capabilities.currentExtent.width != UINT32_MAX)
        return capabilities.currentExtent;

    auto windowSize = window->getDrawableSize();
    VkExtent2D actualExtent = {windowSize.getWidth(), windowSize.getHeight()};

    actualExtent.width = std::max(
        capabilities.minImageExtent.width,
        std::min(capabilities.maxImageExtent.width, actualExtent.width));
    actualExtent.height = std::max(
        capabilities.minImageExtent.height,
        std::min(capabilities.maxImageExtent.height, actualExtent.height));

    return actualExtent;
}

void Swapchain::updateImageHandles()
{
    uint32_t count = 0;
    vkGetSwapchainImagesKHR(this->getDevice()->getHandle(), this->getHandle(), &count, nullptr);

    if(count == 0)
        throw std::runtime_error("Created swapchain has 0 images available!");

    this->images.resize(count);
    vkGetSwapchainImagesKHR(this->getDevice()->getHandle(), this->getHandle(), &count, this->images.data());

    this->createImageViews();
}

void Swapchain::createImageViews()
{
    this->imageViews.resize(
        this->getImages().size()
    );

    for(size_t i = 0; i < this->getImages().size(); i++)
        this->imageViews.at(i) = Image::createImageView(this->getDevice(), this->getImages().at(i), this->getFormat());
}

void Swapchain::destroyImageViews()
{
    for(auto image : this->getImageViews())
        vkDestroyImageView(
            this->getDevice()->getHandle(),
            image,
            nullptr
        );
}

Swapchain::Swapchain(Engine3D::Vulkan::Device* device, Engine3D::Vulkan::Surface* surface, Vulkan::Window* window)
    : device(device)
{
    auto capabilities = surface->getCapabilities(device->getPhysicalDevice());

    auto surfaceFormat = this->chooseSurfaceFormat(surface);
    auto presentMode = this->choosePresentMode(surface);
    auto extent = this->chooseExtent(surface, window);

    this->format = surfaceFormat.format;
    this->extent = extent;

    uint32_t imageCount = capabilities.minImageCount + 1;
    if(capabilities.maxImageCount != 0 && imageCount > capabilities.maxImageCount)
        imageCount = capabilities.maxImageCount;

    VkSwapchainCreateInfoKHR swapchainCreateInfo = {};
    swapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swapchainCreateInfo.surface = surface->getHandle();
    swapchainCreateInfo.imageFormat = surfaceFormat.format;
    swapchainCreateInfo.imageColorSpace = surfaceFormat.colorSpace;
    swapchainCreateInfo.presentMode = presentMode;
    swapchainCreateInfo.imageExtent = extent;
    swapchainCreateInfo.minImageCount = imageCount;
    swapchainCreateInfo.imageArrayLayers = 1;
    swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    auto graphicsQueueFamilyIndex = device->getGraphicsQueueFamilyIndex();
    auto presentQueueFamilyIndex = device->getPresentQueueFamilyIndex();
    uint32_t queueIndices[] = {graphicsQueueFamilyIndex, presentQueueFamilyIndex};

    if(graphicsQueueFamilyIndex != presentQueueFamilyIndex) {
        swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        swapchainCreateInfo.queueFamilyIndexCount = 2;
        swapchainCreateInfo.pQueueFamilyIndices = queueIndices;
    } else {
        swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        swapchainCreateInfo.queueFamilyIndexCount = 0;
        swapchainCreateInfo.pQueueFamilyIndices = nullptr;
    }

    swapchainCreateInfo.preTransform = capabilities.currentTransform;
    swapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    swapchainCreateInfo.clipped = VK_TRUE;
    swapchainCreateInfo.oldSwapchain = VK_NULL_HANDLE;

    if(vkCreateSwapchainKHR(
        this->getDevice()->getHandle(),
        &swapchainCreateInfo,
        nullptr,
        &this->handle
    ) != VK_SUCCESS)
        throw std::runtime_error("Failed to create vulkan swapchain!");

    this->updateImageHandles();
}

Swapchain::~Swapchain()
{
    this->destroyImageViews();

    vkDestroySwapchainKHR(
        this->getDevice()->getHandle(),
        this->getHandle(),
        nullptr
    );
}

void Swapchain::initializeFrameBuffers(Vulkan::RenderPass* renderPass, Vulkan::Image* depthBuffer)
{
    this->frameBuffers.resize(
        this->getImageViews().size()
    );

    for(size_t i = 0; i < this->getImageViews().size(); i++) {
#define VK_SWAPCHAIN_FRAMEBUFFER_ATTACHMENT_SIZE 2
        VkImageView attachments[] = {
            this->getImageViews().at(i),
            depthBuffer->getView()
        };

        VkFramebufferCreateInfo framebufferInfo = {};
        framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferInfo.renderPass = renderPass->getHandle();
        framebufferInfo.attachmentCount = VK_SWAPCHAIN_FRAMEBUFFER_ATTACHMENT_SIZE;
        framebufferInfo.pAttachments = attachments;
        framebufferInfo.width = this->getExtent().width;
        framebufferInfo.height = this->getExtent().height;
        framebufferInfo.layers = 1;

        if(vkCreateFramebuffer(
            this->getDevice()->getHandle(),
            &framebufferInfo,
            nullptr,
            &this->frameBuffers.at(i)
        ) != VK_SUCCESS)
            throw std::runtime_error("Failed to create framebuffer!");
    }
}

void Swapchain::destroyFrameBuffers()
{
    for(auto frameBuffer : this->getFrameBuffers())
        vkDestroyFramebuffer(
            this->getDevice()->getHandle(),
            frameBuffer,
            nullptr
        );

    this->frameBuffers.clear();
}

VkSwapchainKHR Swapchain::getHandle() const
{
    return this->handle;
}

Vulkan::Device* Swapchain::getDevice() const
{
    return this->device;
}

const std::vector<VkImage>& Swapchain::getImages() const
{
    return this->images;
}

size_t Swapchain::getImageCount() const {
    return this->images.size();
}

const std::vector<VkImageView>& Swapchain::getImageViews() const
{
    return this->imageViews;
}

const std::vector<VkFramebuffer>& Swapchain::getFrameBuffers() const
{
    return this->frameBuffers;
}

VkFormat Swapchain::getFormat() const
{
    return this->format;
}

VkExtent2D Swapchain::getExtent() const
{
    return this->extent;
}

void Swapchain::setDevice(Device *device) {
    Swapchain::device = device;
}

void Swapchain::setHandle(VkSwapchainKHR handle) {
    Swapchain::handle = handle;
}

void Swapchain::setImages(const std::vector<VkImage> &images) {
    Swapchain::images = images;
}

void Swapchain::setImageViews(const std::vector<VkImageView> &imageViews) {
    Swapchain::imageViews = imageViews;
}

void Swapchain::setFrameBuffers(const std::vector<VkFramebuffer> &frameBuffers) {
    Swapchain::frameBuffers = frameBuffers;
}

void Swapchain::setFormat(VkFormat format) {
    Swapchain::format = format;
}

void Swapchain::setExtent(const VkExtent2D &extent) {
    Swapchain::extent = extent;
}
