//
// Created by karol on 4/6/2020.
//

#ifndef INC_3DENGINE_VULKAN_MODEL_H
#define INC_3DENGINE_VULKAN_MODEL_H

#include "vulkan_vertex_buffer.h"
#include "vulkan_index_buffer.h"

namespace Engine3D::Vulkan {
    /**
     * Class that describes renderable model.
     */
    class Model : public NonCopyable {
        VertexBufferInterface *vertexBuffer;
        IndexBuffer *indexBuffer;

    public:
        explicit Model(VertexBufferInterface *vertexBuffer, IndexBuffer *indexBuffer);

        ~Model();

        void setVertexBuffer(VertexBufferInterface *buffer);
        void setIndexBuffer(IndexBuffer *buffer);

        VertexBufferInterface *getVertexBuffer() const;
        IndexBuffer *getIndexBuffer() const;
    };
}


#endif //INC_3DENGINE_VULKAN_MODEL_H
