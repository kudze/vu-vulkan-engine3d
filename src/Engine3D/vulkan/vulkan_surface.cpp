//
// Created by kkraujelis on 2020-03-15.
//

#include "vulkan_surface.h"
#include <SDL2/SDL_vulkan.h>

using namespace Engine3D;
using namespace Vulkan;

Surface::Surface(Engine3D::Vulkan::Window* window, Engine3D::Vulkan::Instance* instance)
    : instance(instance)
{

    if(!SDL_Vulkan_CreateSurface(window->getHandle(), instance->getHandle(), &this->handle))
        throw std::runtime_error("Failed to create surface for window: " + std::string(SDL_GetError()));

}

Surface::~Surface()
{
    vkDestroySurfaceKHR(this->instance->getHandle(), this->getHandle(), nullptr);
}

Vulkan::Instance * Surface::getInstance() const {
    return this->instance;
}

VkSurfaceKHR Surface::getHandle() const {
    return this->handle;
}

VkSurfaceCapabilitiesKHR Surface::getCapabilities(Vulkan::PhysicalDevice *device) const
{
    //TODO: add caching.

    VkSurfaceCapabilitiesKHR result = {};

    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
        device->getHandle(),
        this->getHandle(),
        &result
    );

    return result;
}

std::vector<VkSurfaceFormatKHR> Surface::getAvailableFormats(Vulkan::PhysicalDevice *device) const {
    uint32_t count = 0;
    vkGetPhysicalDeviceSurfaceFormatsKHR(device->getHandle(), this->getHandle(), &count, nullptr);

    if(count == 0)
        throw std::runtime_error("VkSurface has 0 available formats!");

    std::vector<VkSurfaceFormatKHR> result(count);
    vkGetPhysicalDeviceSurfaceFormatsKHR(device->getHandle(), this->getHandle(), &count, result.data());

    return result;
}

std::vector<VkPresentModeKHR> Surface::getAvailablePresentModes(Engine3D::Vulkan::PhysicalDevice* device) const {
    uint32_t count = 0;
    vkGetPhysicalDeviceSurfacePresentModesKHR(device->getHandle(), this->getHandle(), &count, nullptr);

    if(count == 0)
        throw std::runtime_error("VkSurface has 0 available present modes!");

    std::vector<VkPresentModeKHR> result(count);
    vkGetPhysicalDeviceSurfacePresentModesKHR(device->getHandle(), this->getHandle(), &count, result.data());

    return result;
}

void Surface::setInstance(Instance *instance) {
    Surface::instance = instance;
}

void Surface::setHandle(VkSurfaceKHR handle) {
    Surface::handle = handle;
}
