//
// Created by kkraujelis on 2020-02-29.
//

#ifndef INC_3DENGINE_VULKAN_INSTANCE_H
#define INC_3DENGINE_VULKAN_INSTANCE_H

#include <string>
#include <vector>
#include <stdexcept>
#include "vulkan/vulkan.h"
#include "../window/window.h"
//#include "../rendering_engine.h"

namespace Engine3D::Vulkan {
    /**
     * Wrapper around VkInstnace.
     *
     * VkInstance is basically an handle to Vulkan Library.
     */
    class Instance : public NonCopyable {
    private:
        VkInstance handle;
        std::string appTitle;
        std::string engineTitle;

        void setHandle(VkInstance handle);

        void setAppTitle(std::string const &title);

        void setEngineTitle(std::string const &title);

    public:
        static std::vector<VkExtensionProperties> getAvailableInstanceExtensionProperties();

        static std::vector<VkLayerProperties> getAvailableInstanceLayerProperties();

        static bool isInstanceExtensionValid(std::string const &extension);

        static bool isInstanceLayerValid(std::string const &layer);

        Instance(
            std::string const &appName,
            uint32_t appVersion,
            std::string const &engineName,
            uint32_t engineVersion,
            std::vector<std::string> const &,
            std::vector<std::string> const &
        );

        ~Instance();

        VkInstance getHandle() const;

        std::string const &getAppTitle() const;

        std::string const &getEngineTitle() const;
    };
}


#endif //INC_3DENGINE_VULKAN_INSTANCE_H
