//
// Created by karol on 4/5/2020.
//

#include "vulkan_vertex_buffer.h"
#include "vulkan_command_buffer.h"
#include "vulkan_staging_buffer.h"

using namespace Engine3D;
using namespace Vulkan;

VertexDynamicBuffer::VertexDynamicBuffer(
    Vulkan::Device *device,
    Vertex const *data,
    uint32_t vertexCount,
    VkSharingMode sharingMode
) : VertexBufferInterface(
    device,
    vertexCount * sizeof(Vertex),
    VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
    sharingMode,
    VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
), vertexCount(vertexCount) {

    if (data != nullptr)
        this->mapMemory((void *) data);

}

VertexDynamicBuffer::~VertexDynamicBuffer() = default;

uint32_t VertexDynamicBuffer::getVertexCount() const {
    return this->vertexCount;
}

//TODO: utilize offset. (it states its way faster that way.)
VertexBuffer::VertexBuffer(
    Vulkan::Device *device,
    Vulkan::CommandPool *transferCommandPool,
    Vertex const *data,
    uint32_t vertexCount,
    VkSharingMode sharingMode
) : VertexBufferInterface(
    device,
    vertexCount * sizeof(Vertex),
    VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
    sharingMode,
    VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
), vertexCount(vertexCount) {
    VkDeviceSize rsize = vertexCount * sizeof(Vertex);

    //Setup staging buffers.
    auto stagingBuffer = new StagingBuffer(
        device,
        rsize,
        sharingMode
    );

    stagingBuffer->mapMemory((void *) data);

    //Record copy procedure.
    Vulkan::CommandBuffer commandBuffer(transferCommandPool);

    commandBuffer.beginRecording(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

    VkBufferCopy copyRegion = {};
    copyRegion.srcOffset = 0; // Optional
    copyRegion.dstOffset = 0; // Optional
    copyRegion.size = rsize;
    vkCmdCopyBuffer(
        commandBuffer.getHandle(),
        stagingBuffer->getHandle(),
        this->getHandle(),
        1, &copyRegion
    );

    commandBuffer.endRecording();

    //Submit command buffer.
    commandBuffer.submit();

    vkQueueWaitIdle(this->getDevice()->getTransferQueue());

    delete stagingBuffer;
}

VertexBuffer::~VertexBuffer() = default;

uint32_t VertexBuffer::getVertexCount() const {
    return this->vertexCount;
}