//
// Created by kkraujelis on 2020-03-16.
//

#ifndef INC_3DENGINE_VULKAN_GRAPHICS_PIPELINE_H
#define INC_3DENGINE_VULKAN_GRAPHICS_PIPELINE_H

#define COLOR_PIPELINE_INDEX 0
#define TEXTURE_PIPELINE_INDEX 1
#define TEXT_PIPELINE_INDEX 2

#include "vulkan_shader.h"
#include "vulkan_renderpass.h"
#include "vulkan_swapchain.h"
#include "glm.hpp"

namespace Engine3D::Vulkan {
    typedef uint16_t graphicsPipelineIndex_t;

    class GraphicsPipelineDescription {
        VertexShader* vertexShader;
        FragmentShader* fragmentShader;
        graphicsPipelineIndex_t uniqueID;

    public:
        GraphicsPipelineDescription(VertexShader* vertexShader, FragmentShader* fragmentShader, graphicsPipelineIndex_t uniqueID);
        ~GraphicsPipelineDescription();

        VertexShader* getVertexShader() const;
        FragmentShader* getFragmentShader() const;
        graphicsPipelineIndex_t getUniqueID() const;
    };

    /**
     * Wrapper around VkPipeline.
     *
     * Describes all rendering stages for frame.
     */
    class GraphicsPipeline : public NonCopyable {
    public:

        struct UniformBufferObject {
            glm::mat4 model;
            glm::mat4 view;
            glm::mat4 proj;
        };

    private:
        VkDescriptorSetLayout descriptorSetLayout;
        VkPipelineLayout layout;
        VkPipeline handle;
        Vulkan::Device *device;

        void setDescriptorSetLayoutHandle(VkDescriptorSetLayout descriptorSetLayout);
        void setLayout(VkPipelineLayout layout);
        void setHandle(VkPipeline handle);
        void setDevice(Device *device);

    public:
        GraphicsPipeline(
            GraphicsPipelineDescription* description,
            Vulkan::Swapchain *swapchain,
            Vulkan::Device *device,
            Vulkan::RenderPass *renderpass
        );

        ~GraphicsPipeline();

        VkDescriptorSetLayout getDescriptorSetLayerHandle() const;
        VkPipelineLayout getLayoutHandle() const;
        VkPipeline getHandle() const;
        Vulkan::Device *getDevice() const;

    };
}


#endif //INC_3DENGINE_VULKAN_GRAPHICS_PIPELINE_H
