//
// Created by kkraujelis on 2020-03-15.
//

#ifndef INC_3DENGINE_VULKAN_SWAPCHAIN_H
#define INC_3DENGINE_VULKAN_SWAPCHAIN_H


#include <vulkan/vulkan.h>
#include "vulkan_device.h"
#include "vulkan_renderpass.h"
#include "vulkan_image.h"

namespace Engine3D::Vulkan {
    /**
     * Wrapper around VkSwapchainKHR.
     *
     * Swapchain describes in which way to submit rendered images to window.
     */
    class Swapchain : public NonCopyable {
    private:
        Vulkan::Device *device = nullptr;
        VkSwapchainKHR handle = VK_NULL_HANDLE;
        std::vector<VkImage> images;
        std::vector<VkImageView> imageViews;
        std::vector<VkFramebuffer> frameBuffers;
        VkFormat format;
        VkExtent2D extent;

        void updateImageHandles();

        void createImageViews();

        void destroyImageViews();

        VkSurfaceFormatKHR chooseSurfaceFormat(Vulkan::Surface *surface) const;

        VkPresentModeKHR choosePresentMode(Vulkan::Surface *surface) const;

        VkExtent2D chooseExtent(Vulkan::Surface *surface, Vulkan::Window *window) const;

        void setDevice(Device *device);
        void setHandle(VkSwapchainKHR handle);
        void setImages(const std::vector<VkImage> &images);
        void setImageViews(const std::vector<VkImageView> &imageViews);
        void setFrameBuffers(const std::vector<VkFramebuffer> &frameBuffers);
        void setFormat(VkFormat format);
        void setExtent(const VkExtent2D &extent);

    public:
        Swapchain(Vulkan::Device *device, Vulkan::Surface *surface, Vulkan::Window *window);
        ~Swapchain();

        void initializeFrameBuffers(Vulkan::RenderPass *renderPass, Vulkan::Image* depthBuffer);
        void destroyFrameBuffers();

        Vulkan::Device *getDevice() const;
        VkSwapchainKHR getHandle() const;

        size_t getImageCount() const;
        std::vector<VkImage> const &getImages() const;
        std::vector<VkImageView> const &getImageViews() const;
        std::vector<VkFramebuffer> const &getFrameBuffers() const;
        VkFormat getFormat() const;
        VkExtent2D getExtent() const;
    };
}


#endif //INC_3DENGINE_VULKAN_SWAPCHAIN_H
