//
// Created by karol on 4/5/2020.
//

#include "vulkan_buffer.h"

#include <cstring>

using namespace Engine3D;
using namespace Vulkan;

VkBuffer Buffer::getHandle() const {
    return this->handle;
}

VkDeviceMemory Buffer::getMemoryHandle() const {
    return this->memoryHandle;
}

Device *Buffer::getDevice() const {
    return this->device;
}

Buffer::Buffer(
    Engine3D::Vulkan::Device *device,
    VkDeviceSize size,
    VkBufferUsageFlags usage,
    VkSharingMode sharingMode,
    VkMemoryPropertyFlags memoryPropertyFlags
)
    : device(device), size(size) {
    VkBufferCreateInfo bufferCreateInfo = {};
    bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferCreateInfo.size = size;
    bufferCreateInfo.usage = usage;
    bufferCreateInfo.sharingMode = sharingMode;

    if (vkCreateBuffer(
        this->getDevice()->getHandle(),
        &bufferCreateInfo,
        nullptr,
        &this->handle
    ) != VK_SUCCESS)
        throw std::runtime_error("Failed to allocate vulkan buffer!");

    //Allocating memory
    VkMemoryRequirements memoryRequirements;
    vkGetBufferMemoryRequirements(
        this->getDevice()->getHandle(),
        this->handle,
        &memoryRequirements
    );

    VkMemoryAllocateInfo memoryAllocateInfo = {};
    memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memoryAllocateInfo.allocationSize = memoryRequirements.size;
    memoryAllocateInfo.memoryTypeIndex = this->getDevice()->getPhysicalDevice()->findMemoryTypeIndex(
        memoryRequirements.memoryTypeBits,
        memoryPropertyFlags
    );

    if (vkAllocateMemory(
        this->getDevice()->getHandle(),
        &memoryAllocateInfo,
        nullptr,
        &this->memoryHandle
    ) != VK_SUCCESS)
        throw std::runtime_error("Failed to allocate vulkan buffer memory!");

    vkBindBufferMemory(this->getDevice()->getHandle(), this->getHandle(), this->getMemoryHandle(), 0);
}

Buffer::~Buffer() {
    vkDestroyBuffer(this->getDevice()->getHandle(), this->handle, nullptr);
    vkFreeMemory(this->getDevice()->getHandle(), this->memoryHandle, nullptr);
}

void Buffer::mapMemory(void *data) {
    //Uploading memory.
    void* gpuData;
    vkMapMemory(this->getDevice()->getHandle(), this->getMemoryHandle(), 0, this->size, 0, &gpuData);
    memcpy(gpuData, data, this->size);
    vkUnmapMemory(this->getDevice()->getHandle(), this->getMemoryHandle());
}

void Buffer::setHandle(VkBuffer handle) {
    this->handle = handle;
}

void Buffer::setMemoryHandle(VkDeviceMemory memoryHandle) {
    this->memoryHandle = memoryHandle;
}

void Buffer::setDevice(Device *device) {
    this->device = device;
}
