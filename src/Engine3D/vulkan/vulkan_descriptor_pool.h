//
// Created by kkraujelis on 2020-03-16.
//

#ifndef INC_3DENGINE_VULKAN_DESCRIPTOR_POOL_H
#define INC_3DENGINE_VULKAN_DESCRIPTOR_POOL_H

#include "vulkan_device.h"

namespace Engine3D::Vulkan {

        /**
         * VkDescriptorPool wrapper.
         * This stores all possible Descriptors.
         * Descriptor is used to describe data stored in uniform buffer to shader & driver.
         */
        class DescriptorPool : public NonCopyable {
        private:
            Vulkan::Device *device = nullptr;
            VkDescriptorPool handle = VK_NULL_HANDLE;

            void setHandle(VkDescriptorPool handle);
            void setDevice(Device *device);

        public:
            DescriptorPool(
                Engine3D::Vulkan::Device *device,
                size_t maxUniformBuffers
            );
            ~DescriptorPool();

            Vulkan::Device *getDevice() const;
            VkDescriptorPool getHandle() const;
        };

}

#endif //INC_3DENGINE_VULKAN_DESCRIPTOR_POOL_H
