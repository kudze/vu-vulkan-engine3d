//
// Created by kkraujelis on 2020-03-15.
//

#include <set>
#include "vulkan_device.h"

using namespace Engine3D;
using namespace Vulkan;

VkPhysicalDeviceProperties const &PhysicalDevice::getProperties() const {
    if (!this->properties.has_value()) {
        VkPhysicalDeviceProperties res;

        vkGetPhysicalDeviceProperties(
            this->getHandle(),
            &res
        );

        this->properties = res;
    }

    return this->properties.value();
}

VkPhysicalDeviceMemoryProperties const &PhysicalDevice::getMemoryProperties() const {
    if (!this->memProperties.has_value()) {
        VkPhysicalDeviceMemoryProperties res;

        vkGetPhysicalDeviceMemoryProperties(
            this->getHandle(),
            &res
        );

        this->memProperties = res;
    }

    return this->memProperties.value();
}

std::vector<VkQueueFamilyProperties> const &PhysicalDevice::getQueueFamilyProperties() const {
    if (this->queueFamilyProperties.empty()) {
        uint32_t count = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(
            this->getHandle(),
            &count,
            nullptr
        );

        if (count == 0)
            throw std::runtime_error("VkPhysicalDevice has no families!");

        this->queueFamilyProperties.resize(count);
        vkGetPhysicalDeviceQueueFamilyProperties(
            this->getHandle(),
            &count,
            this->queueFamilyProperties.data()
        );
    }

    return this->queueFamilyProperties;
}

std::vector<VkExtensionProperties> const &PhysicalDevice::getAvailableDeviceExtensionProperties() const {
    if (this->deviceExtensionProperties.empty()) {
        uint32_t count;
        vkEnumerateDeviceExtensionProperties(this->getHandle(), nullptr, &count, nullptr);

        if (count == 0)
            throw std::runtime_error("Physical device has 0 extensions!");

        this->deviceExtensionProperties.resize(count);
        vkEnumerateDeviceExtensionProperties(
            this->getHandle(),
            nullptr,
            &count,
            this->deviceExtensionProperties.data());
    }

    return this->deviceExtensionProperties;
}

std::vector<VkLayerProperties> const &PhysicalDevice::getAvailableDeviceLayerProperties() const {
    if (this->deviceLayerProperties.empty()) {
        uint32_t count;
        vkEnumerateDeviceLayerProperties(this->getHandle(), &count, nullptr);

        if (count == 0)
            throw std::runtime_error("Physical device has 0 layers!");

        this->deviceLayerProperties.resize(count);
        vkEnumerateDeviceLayerProperties(this->getHandle(), &count, this->deviceLayerProperties.data());
    }

    return this->deviceLayerProperties;
}

const std::map<VkFormat, VkFormatProperties> &PhysicalDevice::getAvailableDeviceFormatProperties() const {
    static const std::vector<VkFormat> formats = {
        VK_FORMAT_D32_SFLOAT,
        VK_FORMAT_D32_SFLOAT_S8_UINT,
        VK_FORMAT_D24_UNORM_S8_UINT
    };

    if (this->deviceFormatProperties.empty()) {
        for (auto format : formats) {
            VkFormatProperties properties;
            vkGetPhysicalDeviceFormatProperties(
                this->getHandle(),
                format,
                &properties
            );

            deviceFormatProperties[format] = properties;
        }
    }

    return this->deviceFormatProperties;
}

VkFormatProperties const& PhysicalDevice::getDeviceFormatProperties(VkFormat format) const {
    return this->getAvailableDeviceFormatProperties().at(format);
}

bool Vulkan::PhysicalDevice::isDeviceExtensionValid(const std::string &extensionName) const {
    auto const &properties = this->getAvailableDeviceExtensionProperties();

    for (auto const &extension : properties)
        if (std::string(extension.extensionName) == extensionName)
            return true;

    return false;
}

bool Vulkan::PhysicalDevice::isDeviceLayerValid(const std::string &layerName) const {
    auto const &properties = this->getAvailableDeviceLayerProperties();

    for (auto const &layer : properties)
        if (std::string(layer.layerName) == layerName)
            return true;

    return false;
}

uint32_t PhysicalDevice::findMemoryTypeIndex(uint32_t typeFilter, VkMemoryPropertyFlags properties) const {
    //TODO: add caching.
    auto memProperties = this->getMemoryProperties();

    for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++)
        if (
            (typeFilter & (1 << i))
            && (memProperties.memoryTypes[i].propertyFlags & properties)
               == properties
            )
            return i;

    throw std::runtime_error("Failed to find suitable memory type");
}

PhysicalDevice::PhysicalDevice(VkPhysicalDevice device)
    : handle(device) {
}

VkPhysicalDevice PhysicalDevice::getHandle() const {
    return this->handle;
}

std::string PhysicalDevice::getName() const {
    return std::string(this->getProperties().deviceName);
}

uint32_t PhysicalDevice::getQueueGraphicsFamilyIndex() const {
    auto properties = this->getQueueFamilyProperties();

    for (size_t i = 0; i < properties.size(); i++)
        if (properties.at(i).queueFlags & VK_QUEUE_GRAPHICS_BIT)
            return static_cast<uint32_t>(i);

    throw std::runtime_error("VkPhysicalDevice has no graphics queue family!");
}

uint32_t PhysicalDevice::getQueuePresentFamilyIndex(Vulkan::Surface *surface) const {
    auto properties = this->getQueueFamilyProperties();
    auto graphicsQueueFamilyIndex = this->getQueueGraphicsFamilyIndex();

    for (size_t i = 0; i < properties.size(); i++) {
        if (i == graphicsQueueFamilyIndex)
            continue;

        VkBool32 isPresentSupported;

        vkGetPhysicalDeviceSurfaceSupportKHR(
            this->getHandle(),
            static_cast<uint32_t>(i),
            surface->getHandle(),
            &isPresentSupported
        );

        if (isPresentSupported)
            return static_cast<uint32_t>(i);
    }

    VkBool32 isPresentSupported;

    vkGetPhysicalDeviceSurfaceSupportKHR(
        this->getHandle(),
        static_cast<uint32_t>(graphicsQueueFamilyIndex),
        surface->getHandle(),
        &isPresentSupported
    );

    if (isPresentSupported)
        return static_cast<uint32_t>(graphicsQueueFamilyIndex);

    throw std::runtime_error("VkPhysicalDevice has no present queue family!");
}

uint32_t PhysicalDevice::getQueueTransferFamilyIndex() const {
    auto properties = this->getQueueFamilyProperties();
    auto graphicsFamilyIndex = this->getQueueGraphicsFamilyIndex();

    for (size_t i = 0; i < properties.size(); i++) {
        if (i == graphicsFamilyIndex)
            continue;

        if (properties.at(i).queueFlags & VK_QUEUE_TRANSFER_BIT)
            return static_cast<uint32_t>(i);
    }

    if (properties.at(graphicsFamilyIndex).queueFlags & VK_QUEUE_TRANSFER_BIT)
        return static_cast<uint32_t>(graphicsFamilyIndex);

    throw std::runtime_error("VkPhysicalDevice has no graphics queue family!");
}

void PhysicalDevice::setHandle(VkPhysicalDevice handle) {
    PhysicalDevice::handle = handle;
}

void PhysicalDevice::setProperties(const std::optional<VkPhysicalDeviceProperties> &properties) {
    PhysicalDevice::properties = properties;
}

void PhysicalDevice::setQueueFamilyProperties(const std::vector<VkQueueFamilyProperties> &queueFamilyProperties) {
    PhysicalDevice::queueFamilyProperties = queueFamilyProperties;
}

void PhysicalDevice::setDeviceExtensionProperties(const std::vector<VkExtensionProperties> &deviceExtensionProperties) {
    PhysicalDevice::deviceExtensionProperties = deviceExtensionProperties;
}

void PhysicalDevice::setDeviceLayerProperties(const std::vector<VkLayerProperties> &deviceLayerProperties) {
    PhysicalDevice::deviceLayerProperties = deviceLayerProperties;
}

PhysicalDeviceManager::PhysicalDeviceManager(Engine3D::Vulkan::Instance *instance)
    : instance(instance) {

}

std::vector<PhysicalDevice *> PhysicalDeviceManager::getAllPhysicalDevices() const {
    uint32_t count = 0;
    vkEnumeratePhysicalDevices(
        this->instance->getHandle(),
        &count,
        nullptr
    );

    if (count == 0)
        throw std::runtime_error("No supported GPUs found!");

    std::vector<VkPhysicalDevice> devices(count);
    vkEnumeratePhysicalDevices(
        this->instance->getHandle(),
        &count,
        devices.data()
    );

    std::vector<Vulkan::PhysicalDevice *> result;
    for (auto device : devices)
        result.push_back(new Vulkan::PhysicalDevice(device));

    return result;
}

Instance *PhysicalDeviceManager::getInstance() const {
    return instance;
}

void PhysicalDeviceManager::setInstance(Instance *instance) {
    PhysicalDeviceManager::instance = instance;
}

Device::Device(Engine3D::Vulkan::PhysicalDevice *device, Vulkan::Surface *surface)
    : device(device) {
    std::vector<const char *> deviceExtensions;
    std::vector<const char *> deviceLayers;

    const char swapchain_ext[] = VK_KHR_SWAPCHAIN_EXTENSION_NAME;
    deviceExtensions.push_back(swapchain_ext);

#ifdef BUILD_DEBUG
    const char validation_layer[] = "VK_LAYER_KHRONOS_validation";
    deviceLayers.push_back(validation_layer);
#endif

    for (auto ext : deviceExtensions)
        if (!device->isDeviceExtensionValid(ext))
            throw std::runtime_error("VkDevice extension " + std::string(ext) + " is not supported!");

    for (auto layer : deviceLayers)
        if (!device->isDeviceLayerValid(layer))
            throw std::runtime_error("VkDevice layer " + std::string(layer) + "is not supported!");

    this->graphicsQueueFamilyIndex = device->getQueueGraphicsFamilyIndex();
    this->presentQueueFamilyIndex = device->getQueuePresentFamilyIndex(surface);
    this->transferQueueFamilyIndex = device->getQueueTransferFamilyIndex();

    std::set<uint32_t> queueIndexes = {this->getGraphicsQueueFamilyIndex(), this->getPresentQueueFamilyIndex(),
                                       this->getTransferQueueFamilyIndex()};

    float priority = 1.f;
    std::vector<VkDeviceQueueCreateInfo> deviceQueueCreateInfos;

    for (auto queueIndex : queueIndexes) {
        VkDeviceQueueCreateInfo deviceQueueCreateInfo = {};

        deviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        deviceQueueCreateInfo.queueFamilyIndex = queueIndex;
        deviceQueueCreateInfo.queueCount = 1;
        deviceQueueCreateInfo.pQueuePriorities = &priority;

        deviceQueueCreateInfos.push_back(deviceQueueCreateInfo);
    }

    VkPhysicalDeviceFeatures deviceFeatures = {};
    //deviceFeatures.samplerAnisotropy = VK_TRUE; -- seems to not run on linux because this feature is not present.

    VkDeviceCreateInfo deviceCreateInfo = {};
    deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceCreateInfo.queueCreateInfoCount = static_cast<uint32_t>(deviceQueueCreateInfos.size());
    deviceCreateInfo.pQueueCreateInfos = deviceQueueCreateInfos.data();
    deviceCreateInfo.pEnabledFeatures = &deviceFeatures;
    deviceCreateInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
    deviceCreateInfo.ppEnabledExtensionNames = deviceExtensions.data();
    deviceCreateInfo.enabledLayerCount = static_cast<uint32_t>(deviceLayers.size());
    deviceCreateInfo.ppEnabledLayerNames = deviceLayers.data();

    if (vkCreateDevice(
        device->getHandle(),
        &deviceCreateInfo,
        nullptr,
        &this->handle
    ) != VK_SUCCESS)
        throw std::runtime_error("Failed to create logical GPU device!");

    vkGetDeviceQueue(this->getHandle(), this->getGraphicsQueueFamilyIndex(), 0, &this->graphicsQueue);
    vkGetDeviceQueue(this->getHandle(), this->getPresentQueueFamilyIndex(), 0, &this->presentQueue);
    vkGetDeviceQueue(this->getHandle(), this->getTransferQueueFamilyIndex(), 0, &this->transferQueue);
}

Device::~Device() {
    vkDestroyDevice(
        this->getHandle(),
        nullptr
    );
}

void Device::waitTillIdle() const {
    vkDeviceWaitIdle(this->getHandle());
}

Vulkan::PhysicalDevice *Device::getPhysicalDevice() const {
    return this->device;
}

VkDevice Device::getHandle() const {
    return this->handle;
}

VkQueue Device::getGraphicsQueue() const {
    return this->graphicsQueue;
}

VkQueue Device::getPresentQueue() const {
    return this->presentQueue;
}

VkQueue Device::getTransferQueue() const {
    return this->transferQueue;
}

uint32_t Device::getGraphicsQueueFamilyIndex() const {
    return this->graphicsQueueFamilyIndex;
}

uint32_t Device::getPresentQueueFamilyIndex() const {
    return this->presentQueueFamilyIndex;
}

uint32_t Device::getTransferQueueFamilyIndex() const {
    return this->transferQueueFamilyIndex;
}

void Device::setDevice(PhysicalDevice *device) {
    Device::device = device;
}

void Device::setHandle(VkDevice handle) {
    Device::handle = handle;
}

void Device::setGraphicsQueue(VkQueue graphicsQueue) {
    Device::graphicsQueue = graphicsQueue;
}

void Device::setGraphicsQueueFamilyIndex(uint32_t graphicsQueueFamilyIndex) {
    Device::graphicsQueueFamilyIndex = graphicsQueueFamilyIndex;
}

void Device::setPresentQueue(VkQueue presentQueue) {
    Device::presentQueue = presentQueue;
}

void Device::setPresentQueueFamilyIndex(uint32_t presentQueueFamilyIndex) {
    Device::presentQueueFamilyIndex = presentQueueFamilyIndex;
}

void Device::setTransferQueue(VkQueue transferQueue) {
    Device::transferQueue = transferQueue;
}

void Device::setTransferQueueFamilyIndex(uint32_t transferQueueFamilyIndex) {
    Device::transferQueueFamilyIndex = transferQueueFamilyIndex;
}
