//
// Created by karol on 4/5/2020.
//

#include "vulkan_uniform_buffer.h"
#include "vulkan_graphics_pipeline.h"

using namespace Engine3D;
using namespace Vulkan;

#include <iostream>

ColorUniformBufferLayout::ColorUniformBufferLayout(
    Engine3D::Vulkan::Device *device,
    Engine3D::Vulkan::DescriptorPool *descriptorPool,
    Engine3D::Vulkan::GraphicsPipeline *graphicsPipeline,
    VkSharingMode sharingMode
) {
    this->matrixBuffer = new Buffer(
        device,
        sizeof(GraphicsPipeline::UniformBufferObject),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        sharingMode,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
    );

    VkDescriptorSetLayout descriptorSetLayout = graphicsPipeline->getDescriptorSetLayerHandle();

    VkDescriptorSetAllocateInfo descriptorSetAllocateInfo = {};
    descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptorSetAllocateInfo.descriptorPool = descriptorPool->getHandle();
    descriptorSetAllocateInfo.descriptorSetCount = 1;
    descriptorSetAllocateInfo.pSetLayouts = &descriptorSetLayout;

    if (vkAllocateDescriptorSets(
        device->getHandle(),
        &descriptorSetAllocateInfo,
        &this->vkDescriptorSet
    ) != VK_SUCCESS)
        throw std::runtime_error("Failed to allocate descriptor set for uniform buffer!");

    VkDescriptorBufferInfo descriptorBufferInfo = {};
    descriptorBufferInfo.buffer = this->matrixBuffer->getHandle();
    descriptorBufferInfo.offset = 0;
    descriptorBufferInfo.range = sizeof(GraphicsPipeline::UniformBufferObject);

    std::vector<VkWriteDescriptorSet> descriptorWrites(1);
    descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[0].dstSet = this->getDescriptorSetHandle();
    descriptorWrites[0].dstBinding = 0;
    descriptorWrites[0].dstArrayElement = 0;
    descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptorWrites[0].descriptorCount = 1;
    descriptorWrites[0].pBufferInfo = &descriptorBufferInfo;
    descriptorWrites[0].pImageInfo = nullptr;
    descriptorWrites[0].pTexelBufferView = nullptr;

    vkUpdateDescriptorSets(
        device->getHandle(),
        static_cast<uint32_t>(descriptorWrites.size()),
        descriptorWrites.data(),
        0,
        nullptr
    );
}

ColorUniformBufferLayout::~ColorUniformBufferLayout() {
    delete this->matrixBuffer;
}

Buffer *ColorUniformBufferLayout::getMatrixBuffer() const {
    return this->matrixBuffer;
}

VkDescriptorSet ColorUniformBufferLayout::getDescriptorSetHandle() const {
    return this->vkDescriptorSet;
}

TextureUniformBufferLayout::TextureUniformBufferLayout(
    Engine3D::Vulkan::Device *device,
    Engine3D::Vulkan::DescriptorPool *descriptorPool,
    Engine3D::Vulkan::GraphicsPipeline *graphicsPipeline,
    Engine3D::Vulkan::Image *texture, VkSharingMode sharingMode
) {
    this->matrixBuffer = new Buffer(
        device,
        sizeof(GraphicsPipeline::UniformBufferObject),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        sharingMode,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
    );

    VkDescriptorSetLayout descriptorSetLayout = graphicsPipeline->getDescriptorSetLayerHandle();

    VkDescriptorSetAllocateInfo descriptorSetAllocateInfo = {};
    descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptorSetAllocateInfo.descriptorPool = descriptorPool->getHandle();
    descriptorSetAllocateInfo.descriptorSetCount = 1;
    descriptorSetAllocateInfo.pSetLayouts = &descriptorSetLayout;

    if (vkAllocateDescriptorSets(
        device->getHandle(),
        &descriptorSetAllocateInfo,
        &this->vkDescriptorSet
    ) != VK_SUCCESS)
        throw std::runtime_error("Failed to allocate descriptor set for uniform buffer!");

    VkDescriptorBufferInfo descriptorBufferInfo = {};
    descriptorBufferInfo.buffer = this->matrixBuffer->getHandle();
    descriptorBufferInfo.offset = 0;
    descriptorBufferInfo.range = sizeof(GraphicsPipeline::UniformBufferObject);

    std::vector<VkWriteDescriptorSet> descriptorWrites(2);
    descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[0].dstSet = this->getDescriptorSetHandle();
    descriptorWrites[0].dstBinding = 0;
    descriptorWrites[0].dstArrayElement = 0;
    descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptorWrites[0].descriptorCount = 1;
    descriptorWrites[0].pBufferInfo = &descriptorBufferInfo;
    descriptorWrites[0].pImageInfo = nullptr;
    descriptorWrites[0].pTexelBufferView = nullptr;

    VkDescriptorImageInfo imageInfo = {};
    imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    imageInfo.imageView = texture->getView();
    imageInfo.sampler = texture->getSampler()->getHandle();

    descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[1].dstSet = this->getDescriptorSetHandle();
    descriptorWrites[1].dstBinding = 1;
    descriptorWrites[1].dstArrayElement = 0;
    descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptorWrites[1].descriptorCount = 1;
    descriptorWrites[1].pImageInfo = &imageInfo;

    vkUpdateDescriptorSets(
        device->getHandle(),
        static_cast<uint32_t>(descriptorWrites.size()),
        descriptorWrites.data(),
        0,
        nullptr
    );
}

TextureUniformBufferLayout::~TextureUniformBufferLayout() {
    delete this->matrixBuffer;
}

Buffer *TextureUniformBufferLayout::getMatrixBuffer() const {
    return this->matrixBuffer;
}

VkDescriptorSet TextureUniformBufferLayout::getDescriptorSetHandle() const {
    return this->vkDescriptorSet;
}

TextUniformBufferLayout::TextUniformBufferLayout(
    Engine3D::Vulkan::Device *device,
    Engine3D::Vulkan::DescriptorPool *descriptorPool,
    Engine3D::Vulkan::GraphicsPipeline *graphicsPipeline,
    Vulkan::Image* texture,
    VkSharingMode sharingMode
) {
    this->matrixBuffer = new Buffer(
        device,
        sizeof(GraphicsPipeline::UniformBufferObject),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        sharingMode,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
    );

    this->textColorBuffer = new Buffer(
        device,
        sizeof(glm::vec3),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        sharingMode,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
    );

    VkDescriptorSetLayout descriptorSetLayout = graphicsPipeline->getDescriptorSetLayerHandle();

    VkDescriptorSetAllocateInfo descriptorSetAllocateInfo = {};
    descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptorSetAllocateInfo.descriptorPool = descriptorPool->getHandle();
    descriptorSetAllocateInfo.descriptorSetCount = 1;
    descriptorSetAllocateInfo.pSetLayouts = &descriptorSetLayout;

    if (vkAllocateDescriptorSets(
        device->getHandle(),
        &descriptorSetAllocateInfo,
        &this->vkDescriptorSet
    ) != VK_SUCCESS)
        throw std::runtime_error("Failed to allocate descriptor set for uniform buffer!");

    std::vector<VkWriteDescriptorSet> descriptorWrites(3);

    VkDescriptorBufferInfo descriptorMatrixBufferInfo = {};
    descriptorMatrixBufferInfo.buffer = this->matrixBuffer->getHandle();
    descriptorMatrixBufferInfo.offset = 0;
    descriptorMatrixBufferInfo.range = sizeof(GraphicsPipeline::UniformBufferObject);

    descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[0].dstSet = this->getDescriptorSetHandle();
    descriptorWrites[0].dstBinding = 0;
    descriptorWrites[0].dstArrayElement = 0;
    descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptorWrites[0].descriptorCount = 1;
    descriptorWrites[0].pBufferInfo = &descriptorMatrixBufferInfo;
    descriptorWrites[0].pImageInfo = nullptr;
    descriptorWrites[0].pTexelBufferView = nullptr;

    VkDescriptorImageInfo imageInfo = {};
    imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    imageInfo.imageView = texture->getView();
    imageInfo.sampler = texture->getSampler()->getHandle();

    descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[1].dstSet = this->getDescriptorSetHandle();
    descriptorWrites[1].dstBinding = 1;
    descriptorWrites[1].dstArrayElement = 0;
    descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptorWrites[1].descriptorCount = 1;
    descriptorWrites[1].pImageInfo = &imageInfo;

    VkDescriptorBufferInfo descriptorTextColorBufferInfo = {};
    descriptorTextColorBufferInfo.buffer = this->matrixBuffer->getHandle();
    descriptorTextColorBufferInfo.offset = 0;
    descriptorTextColorBufferInfo.range = sizeof(glm::vec3);

    descriptorWrites[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[2].dstSet = this->getDescriptorSetHandle();
    descriptorWrites[2].dstBinding = 2;
    descriptorWrites[2].dstArrayElement = 0;
    descriptorWrites[2].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptorWrites[2].descriptorCount = 1;
    descriptorWrites[2].pBufferInfo = &descriptorTextColorBufferInfo;
    descriptorWrites[2].pImageInfo = nullptr;
    descriptorWrites[2].pTexelBufferView = nullptr;

    vkUpdateDescriptorSets(
        device->getHandle(),
        static_cast<uint32_t>(descriptorWrites.size()),
        descriptorWrites.data(),
        0,
        nullptr
    );
}

TextUniformBufferLayout::~TextUniformBufferLayout() {
    delete this->matrixBuffer;
    delete this->textColorBuffer;
}

Buffer *TextUniformBufferLayout::getMatrixBuffer() const {
    return this->matrixBuffer;
}

Buffer *TextUniformBufferLayout::getTextColorBuffer() const {
    return this->textColorBuffer;
}

VkDescriptorSet TextUniformBufferLayout::getDescriptorSetHandle() const {
    return this->vkDescriptorSet;
}