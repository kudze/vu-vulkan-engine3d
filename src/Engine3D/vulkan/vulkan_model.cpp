//
// Created by karol on 4/6/2020.
//

#include "vulkan_model.h"

using namespace Engine3D;
using namespace Vulkan;

Model::Model(VertexBufferInterface* vertexBuffer, IndexBuffer* indexBuffer)
    : vertexBuffer(vertexBuffer), indexBuffer(indexBuffer) {

}

Model::~Model() {
    delete this->vertexBuffer;
    delete this->indexBuffer;
}

void Model::setVertexBuffer(VertexBufferInterface *buffer) {
    this->vertexBuffer = buffer;
}

VertexBufferInterface *Model::getVertexBuffer() const {
    return this->vertexBuffer;
}

void Model::setIndexBuffer(IndexBuffer *buffer) {
    this->indexBuffer = buffer;
}

IndexBuffer * Model::getIndexBuffer() const {
    return this->indexBuffer;
}