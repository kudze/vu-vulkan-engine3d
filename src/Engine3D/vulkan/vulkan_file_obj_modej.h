//
// Created by karol on 5/18/2020.
//

#ifndef INC_3DENGINE_VULKAN_FILE_OBJ_MODEJ_H
#define INC_3DENGINE_VULKAN_FILE_OBJ_MODEJ_H

#include "vulkan_model.h"

namespace Engine3D::Vulkan {

    class FileOBJModel
        : public Model
    {

    public:
        explicit FileOBJModel(Device* device, CommandPool* transferCommandPool, std::string const& filename);
        virtual ~FileOBJModel();

    };

}

#endif //INC_3DENGINE_VULKAN_FILE_OBJ_MODEJ_H
