//
// Created by kkraujelis on 2020-02-29.
//

#include "vulkan_renderer.h"
#include "../window/sdl.h"
#include "../scene/object.h"
#include "../scene/ui/ui_text.h"

#define ENGINE_TITLE "Engine3D (Kudze)"
#define ENGINE_VERSION VK_MAKE_VERSION(1, 0, 0)

using namespace Engine3D;
using namespace Vulkan;

#include <iostream>

Renderer::Renderer(
    Engine *engine
)
    : EngineComponent(engine) {

    this->vkInstance = new Vulkan::Instance(
        this->getWindow()->getName(),
        VK_MAKE_VERSION(1, 0, 0),
        ENGINE_TITLE,
        ENGINE_VERSION,
        this->getWindow()->getRequiredVulkanInstanceExtensions(),
        this->getWindow()->getRequiredVulkanInstanceLayers()
    );

    this->vkValidation = new Vulkan::ValidationLayers(
        this->getInstance()
    );

    this->vkSurface = new Vulkan::Surface(
        this->getWindow(),
        this->getInstance()
    );

    auto deviceManager = new Vulkan::PhysicalDeviceManager(this->getInstance());
    this->vkDevice = new Vulkan::Device(deviceManager->getAllPhysicalDevices().at(0), this->getSurface());
    delete deviceManager;

    this->vkGraphicsCommandPool = new Vulkan::CommandPool(
        this->getDevice(),
        this->getDevice()->getGraphicsQueue(),
        this->getDevice()->getGraphicsQueueFamilyIndex()
    );

    this->vkTransferCommandPool = new Vulkan::CommandPool(
        this->getDevice(),
        this->getDevice()->getTransferQueue(),
        this->getDevice()->getTransferQueueFamilyIndex()
    );
}

Renderer::~Renderer() {
    delete this->vkDescriptorPool;
    this->vkDescriptorPool = nullptr;

    this->freeSwapchain();

    delete this->vkGraphicsCommandPool;
    this->vkGraphicsCommandPool = nullptr;

    delete this->vkTransferCommandPool;
    this->vkTransferCommandPool = nullptr;

    for (auto description : this->pipelineDescriptions)
        delete description;
    this->pipelineDescriptions.clear();

    delete this->vkDevice;
    this->vkDevice = nullptr;

    delete this->vkSurface;
    this->vkSurface = nullptr;

    delete this->vkValidation;
    this->vkValidation = nullptr;

    delete this->vkInstance;
    this->vkInstance = nullptr;

};

Vulkan::CommandBuffer *Renderer::recordCommandBuffer(uint32_t imageIndex) {
    auto buffer = new Vulkan::CommandBuffer(
        this->getGraphicsCommandPool()
    );

    buffer->beginRecording(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

    VkClearValue colorClearValue = {};
    colorClearValue.color = {0.0f, 0.0f, 0.0f, 1.0f};

    VkClearValue depthClearValue = {};
    depthClearValue.depthStencil = {1.0f, 0};

    VkClearValue clearValues[] = {
        colorClearValue,
        depthClearValue
    };

    VkRenderPassBeginInfo renderPassInfo = {};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassInfo.renderPass = this->getRenderPass()->getHandle();
    renderPassInfo.framebuffer = this->getSwapchain()->getFrameBuffers().at(imageIndex);
    renderPassInfo.renderArea.offset = {0, 0};
    renderPassInfo.renderArea.extent = this->getSwapchain()->getExtent();
    renderPassInfo.clearValueCount = 2;
    renderPassInfo.pClearValues = clearValues;

    vkCmdBeginRenderPass(buffer->getHandle(), &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    for (auto const &pair : this->getEngine()->getScene()->getObjectsMappedToPipelines()) {
        auto pipeline = this->getGraphicsPipelines().at(pair.first);

        vkCmdBindPipeline(buffer->getHandle(), VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->getHandle());
        for (auto object : pair.second) {
            if (object->getGraphicsPipelineIndex() == TEXT_PIPELINE_INDEX) {
                auto text = (UI::Text *) object;

                if(!text->getCanvas()->isVisible())
                    continue;
            }

            object->updateBufferData(this->getEngine()->getScene()->getCamera(), imageIndex);

            auto vertexBuffer = object->getModel()->getVertexBuffer();
            auto indexBuffer = object->getModel()->getIndexBuffer();

            VkBuffer vertexBuffers[] = {vertexBuffer->getHandle()};
            VkDeviceSize offsets[] = {0};
            vkCmdBindVertexBuffers(buffer->getHandle(), 0, 1, vertexBuffers, offsets);
            vkCmdBindIndexBuffer(buffer->getHandle(), indexBuffer->getHandle(), 0, VK_INDEX_TYPE_UINT32);

            if (object->getGraphicsPipelineIndex() == TEXT_PIPELINE_INDEX) {
                auto text = (UI::Text*) object;

                for(int i = 0; i < text->getTextureUniformBuffers().size(); i++)
                {
                    auto textureDescriptorSet = text->getTextureUniformBuffers().at(i)->getDescriptorSetHandle();

                    vkCmdBindDescriptorSets(buffer->getHandle(), VK_PIPELINE_BIND_POINT_GRAPHICS,
                                            pipeline->getLayoutHandle(),
                                            0, 1, &textureDescriptorSet, 0, nullptr);

                    vkCmdDrawIndexed(buffer->getHandle(), 6, 1, i * 6, 0, 0);
                }
            } else {
                auto descriptorSet = object->getUniformBuffers().at(imageIndex)->getDescriptorSetHandle();
                vkCmdBindDescriptorSets(buffer->getHandle(), VK_PIPELINE_BIND_POINT_GRAPHICS,
                                        pipeline->getLayoutHandle(),
                                        0, 1, &descriptorSet, 0, nullptr);

                vkCmdDrawIndexed(buffer->getHandle(), indexBuffer->getIndexCount(), 1, 0, 0, 0);
            }
        }
    }

    vkCmdEndRenderPass(buffer->getHandle());

    buffer->endRecording();
    vkQueueWaitIdle(this->getDevice()->getGraphicsQueue());

    return buffer;
}

void Renderer::createSwapchain() {
    this->getDevice()->waitTillIdle();

    this->vkSwapchain = new Vulkan::Swapchain(
        this->getDevice(),
        this->getSurface(),
        this->getWindow()
    );

    this->depthBuffer = new Vulkan::Image(
        this->getDevice(),
        this->getTransferCommandPool(),
        this->getGraphicsCommandPool(),
        this->getSwapchain()->getExtent().width,
        this->getSwapchain()->getExtent().height,
        nullptr,
        Vulkan::Image::findDepthFormat(this->getDevice()->getPhysicalDevice()),
        VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        VK_IMAGE_ASPECT_DEPTH_BIT,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    );

    this->vkRenderPass = new Vulkan::RenderPass(
        this->getDevice(),
        this->getSwapchain()
    );

    for (auto description : this->pipelineDescriptions) {
        this->vkPipelines[description->getUniqueID()] =
            new GraphicsPipeline(
                description,
                this->getSwapchain(),
                this->getDevice(),
                this->getRenderPass()
            );
    }

    this->getSwapchain()->initializeFrameBuffers(
        this->getRenderPass(),
        this->getDepthBuffer()
    );

    this->imageAvailableSemaphores.resize(this->getMaxFramesInFlight());
    this->frameAvailableSemaphores.resize(this->getMaxFramesInFlight());
    this->frameRendereringFences.resize(this->getMaxFramesInFlight());

    for (size_t i = 0; i < this->getMaxFramesInFlight(); i++) {
        this->imageAvailableSemaphores[i] = new Vulkan::Semaphore(this->getDevice());
        this->frameAvailableSemaphores[i] = new Vulkan::Semaphore(this->getDevice());
        this->frameRendereringFences[i] = new Vulkan::Fence(this->getDevice(), true);
    }

    this->swapchainFrameRenderingFence.resize(this->getSwapchain()->getImageCount(), nullptr);
}

void Renderer::freeSwapchain() {
    for (auto semaphore : this->imageAvailableSemaphores)
        delete semaphore;
    this->imageAvailableSemaphores.clear();

    for (auto semaphore : this->frameAvailableSemaphores)
        delete semaphore;
    this->frameAvailableSemaphores.clear();

    for (auto fence : this->frameRendereringFences)
        delete fence;
    this->frameRendereringFences.clear();

    if (this->getSwapchain() != nullptr)
        this->getSwapchain()->destroyFrameBuffers();

    for (auto pipeline : this->vkPipelines)
        delete pipeline.second;
    this->vkPipelines.clear();

    delete this->vkRenderPass;
    this->vkRenderPass = nullptr;

    delete this->depthBuffer;
    this->depthBuffer = nullptr;

    delete this->vkSwapchain;
    this->vkSwapchain = nullptr;
}

void Renderer::registerPipeline(GraphicsPipelineDescription *description) {
    this->pipelineDescriptions.push_back(description);

    if (this->vkSwapchain != nullptr)
        this->vkPipelines[description->getUniqueID()] = new GraphicsPipeline(
            description,
            this->getSwapchain(),
            this->getDevice(),
            this->getRenderPass()
        );
}

void Renderer::bootUpPipelines() {
    if (this->vkSwapchain == nullptr)
        this->createSwapchain();

    if (vkDescriptorPool == nullptr)
        this->vkDescriptorPool = new Vulkan::DescriptorPool(
            this->getDevice(),
            this->getSwapchain()->getImageCount() * MAX_OBJECTS_PER_SCENE
        );
}

void Renderer::render() {
    uint32_t imageIndex;
    auto acquireImageResult = vkAcquireNextImageKHR(
        this->getDevice()->getHandle(),
        this->getSwapchain()->getHandle(),
        UINT64_MAX,
        this->getImageAvailableSemaphores().at(this->currentFrame)->getHandle(),
        VK_NULL_HANDLE,
        &imageIndex
    );

    auto buffer = this->recordCommandBuffer(imageIndex);

    if (acquireImageResult == VK_ERROR_OUT_OF_DATE_KHR) {
        this->freeSwapchain();
        this->createSwapchain();

        return;
    } else if (acquireImageResult != VK_SUCCESS && acquireImageResult != VK_SUBOPTIMAL_KHR)
        throw std::runtime_error("Failed to fetch swap chain image!");

    if (this->swapchainFrameRenderingFence.at(imageIndex) != nullptr)
        this->swapchainFrameRenderingFence.at(imageIndex)->wait();

    this->swapchainFrameRenderingFence.at(imageIndex) = this->frameRendereringFences.at(currentFrame);

    //TODO: move this into command pool.
    std::vector<Vulkan::Semaphore *> waitSemaphores = {this->getImageAvailableSemaphores().at(this->currentFrame)};
    std::vector<Vulkan::Semaphore *> signalSemaphores = {this->getFrameAvailableSemaphores().at(this->currentFrame)};
    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};

    buffer->submit(
        this->swapchainFrameRenderingFence.at(imageIndex),
        waitSemaphores,
        signalSemaphores,
        waitStages
    );

    VkSemaphore semaphore = signalSemaphores.at(0)->getHandle();
    VkSwapchainKHR swapChains[] = {this->getSwapchain()->getHandle()};
    VkPresentInfoKHR presentInfo = {};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = &semaphore;
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = swapChains;
    presentInfo.pImageIndices = &imageIndex;
    presentInfo.pResults = nullptr;
    auto presentResult = vkQueuePresentKHR(this->getDevice()->getPresentQueue(), &presentInfo);

    if (presentResult == VK_ERROR_OUT_OF_DATE_KHR) {
        this->freeSwapchain();
        this->createSwapchain();

        return;
    } else if (presentResult != VK_SUCCESS && presentResult != VK_SUBOPTIMAL_KHR)
        throw std::runtime_error("Failed to fetch swap chain image!");

    this->currentFrame = (this->currentFrame + 1) % this->maxFramesInFlight;

    this->getDevice()->waitTillIdle();
}

void Renderer::recreateSwapchain() {
    this->freeSwapchain();
    this->createSwapchain();
}

Vulkan::Window *Renderer::getWindow() const {
    return (Vulkan::Window *)
        this->getEngine()->getWindow();
}

Vulkan::Instance *Renderer::getInstance() const {
    return this->vkInstance;
}

Vulkan::ValidationLayers *Renderer::getValidationLayers() const {
    return this->vkValidation;
}

Vulkan::Surface *Renderer::getSurface() const {
    return this->vkSurface;
}

Vulkan::Device *Renderer::getDevice() const {
    return this->vkDevice;
}

Vulkan::RenderPass *Renderer::getRenderPass() const {
    return this->vkRenderPass;
}

Vulkan::Swapchain *Renderer::getSwapchain() const {
    return this->vkSwapchain;
}

Vulkan::CommandPool *Renderer::getGraphicsCommandPool() const {
    return this->vkGraphicsCommandPool;
}

Vulkan::CommandPool *Renderer::getTransferCommandPool() const {
    return this->vkTransferCommandPool;
}

Vulkan::DescriptorPool *Renderer::getDescriptorPool() const {
    return this->vkDescriptorPool;
}

Vulkan::Image *Renderer::getDepthBuffer() const {
    return this->depthBuffer;
}

std::vector<Vulkan::Semaphore *> const &Renderer::getImageAvailableSemaphores() const {
    return this->imageAvailableSemaphores;
}

std::vector<Vulkan::Semaphore *> const &Renderer::getFrameAvailableSemaphores() const {
    return this->frameAvailableSemaphores;
}

const std::vector<Vulkan::Fence *> &Renderer::getFrameRenderingFences() const {
    return this->frameRendereringFences;
}

const std::unordered_map<graphicsPipelineIndex_t, Vulkan::GraphicsPipeline *> &Renderer::getGraphicsPipelines() const {
    return this->vkPipelines;
}

const std::vector<Vulkan::GraphicsPipelineDescription *> &Renderer::getGraphicsPipelinesDescriptions() const {
    return this->pipelineDescriptions;
}

uint8_t Renderer::getMaxFramesInFlight() const {
    return this->maxFramesInFlight;
}

uint8_t Renderer::getCurrentFrameIndex() const {
    return this->currentFrame;
}