//
// Created by kkraujelis on 2020-02-29.
//

#include <SDL2/SDL_vulkan.h>
#include <vulkan/vulkan.h>
#include <stdexcept>

#include "vulkan_window.h"

using namespace Engine3D;

uint32_t Vulkan::Window::constructFlags(Engine3D::Window::Type type) const {
    auto res = Engine3D::Window::constructFlags(type);

    return res | SDL_WINDOW_VULKAN;
}

Vulkan::Window::Window(
    Engine3D::Window::Type type,
    std::string const &name,
    Engine3D::Dimensions<uint16_t> const &size,
    bool resizeable
)
    : Engine3D::Window() {
    this->createWindow(type, name, size, resizeable);
}

Vulkan::Window::~Window() = default;

Dimensions<uint16_t> Vulkan::Window::getDrawableSize() const {
    int w, h;

    SDL_Vulkan_GetDrawableSize(
        this->getHandle(),
        &w,
        &h
    );

    return Dimensions<uint16_t>(
        static_cast<uint16_t>(w),
        static_cast<uint16_t>(h)
    );
}

std::vector<std::string> Vulkan::Window::getRequiredVulkanInstanceExtensions() const {
    unsigned int size = 0;
    bool result;

    result = SDL_Vulkan_GetInstanceExtensions(
        this->getHandle(),
        &size,
        nullptr
    );

    if (!result)
        throw std::runtime_error(
            "Failed to create a list of required Vulkan Instance extensions! "
            + std::string(SDL_GetError())
        );

    const char **extensions = new const char *[size];

    result = SDL_Vulkan_GetInstanceExtensions(
        this->getHandle(),
        &size,
        extensions
    );

    if (!result)
        throw std::runtime_error(
            "Failed to create a list of required Vulkan Instance extensions! "
            + std::string(SDL_GetError())
        );

    std::vector<std::string> _result;

    for (uint32_t i = 0; i < size; i++)
        _result.emplace_back(std::string(extensions[i]));

    delete[] extensions;

#ifdef BUILD_DEBUG
    _result.emplace_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
#endif

    return _result;
}

std::vector<std::string> Vulkan::Window::getRequiredVulkanInstanceLayers() const {
#ifdef BUILD_DEBUG
    std::vector<std::string> a;
    a.emplace_back("VK_LAYER_KHRONOS_validation");
    return a;
#else
    return std::vector<std::string>();
#endif
}