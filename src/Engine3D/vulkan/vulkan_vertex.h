//
// Created by karol on 4/5/2020.
//

#ifndef INC_3DENGINE_VULKAN_VERTEX_H
#define INC_3DENGINE_VULKAN_VERTEX_H

#include <array>
#include <vulkan/vulkan_core.h>
#include <glm.hpp>
#include <iostream>

namespace Engine3D::Vulkan {

    /**
     * This is a representation of a single Vertex in Model
     */
    class Vertex {

        /**
         * @var glm::vec2 pos - position of a vertex
         */
        glm::vec3 pos;

        /**
         * @var glm::vec3 col - color of a vertex
         */
        glm::vec3 col;

        /**
         * @var glm::vec2 tex - position for a texture.
         */
         glm::vec2 tex;

        /**
         * Position setter
         * @param pos
         */
        void setPosition(glm::vec3 const &pos);

        /**
         * Color setter
         * @param color
         */
        void setColor(glm::vec3 const &color);

        /**
         * Texture position setter
         * @param col
         */
         void setTextureCoordinates(glm::vec2 const& tex);

    public:

        /**
         * Builds vertex shader input binding descriptions.
         * (This is fetched by vulkan pipeline to determine how to read vertex)
         * @return VkVertexInputBindingDescription
         */
        static VkVertexInputBindingDescription buildBindingDescription();

        /**
         * Builds vertex shader input attribute descriptions.
         * (This is fetched by vulkan pipeline to determine how to split read vertex into shader input attributes)
         * @return VkVertexInputBindingDescription
         */
        static std::array<VkVertexInputAttributeDescription, 3> buildAttributeDescriptions();

        /**
         * Constructor
         *
         * @param pos
         * @param col
         */
        Vertex(glm::vec3 const &pos, glm::vec3 const &col, glm::vec2 const& tex);

        /**
         * Position getter
         *
         * @return glm::vec2
         */
        glm::vec3 const &getPosition() const;

        /**
         * Color getter
         *
         * @return glm::vec3
         */
        glm::vec3 const &getColor() const;

        /**
         * Texture coordinates getter.
         *
         * @return glm::vec2
         */
        glm::vec2 const& getTextureCoordinates() const;

        /**
         * == operator
         * @param other
         * @return
         */
        bool operator==(const Vertex& other) const;

        ///<< operator (prints vertex to ostream)
        ///@param os - output stream
        ///@param o - vertex to print
        ///@returns os
        friend std::ostream& operator<<(std::ostream& os, const Vertex& o)
        {
            os  << o.getPosition().x << " " << o.getPosition().y
                << " "<< o.getColor().r << " " << o.getColor().g << " " << o.getColor().b;

            return os;
        }

        ///>> operator (reads vertex from istream)
        ///@param is - input stream
        ///@param o - vertex to read
        ///@returns is
        friend std::istream& operator>>(std::istream& is, Vertex& o)
        {
            is >> o.pos.x >> o.pos.y >> o.col.r >> o.col.g >> o.col.b;

            return is;
        }
    };
}

namespace std {
    template<> struct hash<Engine3D::Vulkan::Vertex> {
        size_t operator()(Engine3D::Vulkan::Vertex const& vertex) const;
    };
}


#endif //INC_3DENGINE_VULKAN_VERTEX_H
