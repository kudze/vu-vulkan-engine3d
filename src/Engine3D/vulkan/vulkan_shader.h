//
// Created by kkraujelis on 2020-03-16.
//

#ifndef INC_3DENGINE_VULKAN_SHADER_H
#define INC_3DENGINE_VULKAN_SHADER_H

#include <vector>

#include "vulkan_device.h"

namespace Engine3D::Vulkan {
    /**
     * Wrapper around VkShaderModule
     *
     * Stores shader before pipeline is created.
     */
    class Shader : public NonCopyable {
    private:
        VkShaderModule handle = VK_NULL_HANDLE;
        Vulkan::Device *device = nullptr;
        std::vector<VkDescriptorSetLayoutBinding> uboLayoutBindings = {};

        void setHandle(VkShaderModule handle);
        void setDevice(Device *device);

    public:
        Shader(Vulkan::Device *device, std::vector<char> const &shader);

        virtual ~Shader();

        void addUBOLayoutBinding(uint32_t binding, VkDescriptorType descriptorType, VkShaderStageFlags stageFlags);

        VkShaderModule getHandle() const;
        Vulkan::Device *getDevice() const;
        std::vector<VkDescriptorSetLayoutBinding> const &getUBOLayoutBindings() const;

    };

    class VertexShader
        : public Shader
    {
    public:
        VertexShader(Vulkan::Device *device, std::vector<char> const &shader);
        virtual ~VertexShader();
    };

    class FragmentShader
        : public Shader
    {
    public:
        FragmentShader(Vulkan::Device *device, std::vector<char> const &shader);
        virtual ~FragmentShader();
    };
}

#endif //INC_3DENGINE_VULKAN_SHADER_H
