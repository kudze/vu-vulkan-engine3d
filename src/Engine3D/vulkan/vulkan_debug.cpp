//
// Created by kkraujelis on 2020-03-15.
//


#include "vulkan_debug.h"
#include <iostream>

using namespace Engine3D;
using namespace Vulkan;
using namespace std;

VkBool32 ValidationLayers::debugCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
    void* pUserData
)
{
    switch(messageSeverity)
    {
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
        {
            cout << "[ERROR][VULKAN] " + std::string(pCallbackData->pMessage) << endl;

            break;
        }

        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
        {
            cout << "[WARN][VULKAN] " + std::string(pCallbackData->pMessage) << endl;

            break;
        }

        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
        {
            cout << "[INFO][VULKAN] " + std::string(pCallbackData->pMessage) << endl;

            break;
        }

        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
        {
            cout << "[DEBUG][VULKAN] " + std::string(pCallbackData->pMessage) << endl;

            break;
        }
    }

    return VK_FALSE;
}

PFN_vkCreateDebugUtilsMessengerEXT ValidationLayers::loadCreateDebugUtilsMessengerFunction(Engine3D::Vulkan::Instance* instance) const
{
    return (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(
        instance->getHandle(),
        "vkCreateDebugUtilsMessengerEXT"
    );
}

PFN_vkDestroyDebugUtilsMessengerEXT ValidationLayers::loadDestroyDebugUtilsMessengerFunction(Engine3D::Vulkan::Instance* instance) const
{
    return (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(
        instance->getHandle(),
        "vkDestroyDebugUtilsMessengerEXT"
    );
}

VkDebugUtilsMessengerCreateInfoEXT ValidationLayers::createValidationLayerCreateInfo()
{
    VkDebugUtilsMessengerCreateInfoEXT createInfo = {};

    createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    createInfo.messageSeverity =
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT
        | VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT
        | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
        | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    createInfo.messageType =
        VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT
        | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT
        | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    createInfo.pfnUserCallback = ValidationLayers::debugCallback;
    createInfo.pUserData = nullptr; // Optional

    return createInfo;
}

ValidationLayers::ValidationLayers(Vulkan::Instance* instance)
{
    this->instance = instance;

#ifdef BUILD_DEBUG
    VkDebugUtilsMessengerCreateInfoEXT createInfo = ValidationLayers::createValidationLayerCreateInfo();

    auto vkCreateDebugUtilsMessenger = this->loadCreateDebugUtilsMessengerFunction(instance);

    if(vkCreateDebugUtilsMessenger == nullptr)
        throw std::runtime_error("Failed to fetch vkCreateDebugUtilsMessenger function!");

    auto vkDestroyDebugUtilsMessenger = this->loadDestroyDebugUtilsMessengerFunction(
        this->instance
    );

    if(vkDestroyDebugUtilsMessenger == nullptr)
        throw std::runtime_error("Failed to fetch vkDestroyDebugUtilsMessenger function!");

    if(vkCreateDebugUtilsMessenger(
        instance->getHandle(),
        &createInfo,
        nullptr,
        &this->handle
    ) != VK_SUCCESS)
        throw std::runtime_error("Failed to create validation layer messenger!");

#endif

}

ValidationLayers::~ValidationLayers()
{
#ifdef BUILD_DEBUG

    auto vkDestroyDebugUtilsMessenger = this->loadDestroyDebugUtilsMessengerFunction(
        this->instance
    );

    if(vkDestroyDebugUtilsMessenger != nullptr)
        vkDestroyDebugUtilsMessenger(
            this->instance->getHandle(),
            this->handle,
            nullptr
        );

#endif
}

VkDebugUtilsMessengerEXT ValidationLayers::getHandle() const
{
    return this->handle;
}

Engine3D::Vulkan::Instance* ValidationLayers::getInstance() const
{
    return this->instance;
}

void ValidationLayers::setHandle(VkDebugUtilsMessengerEXT handle) {
    ValidationLayers::handle = handle;
}

void ValidationLayers::setInstance(Instance *instance) {
    ValidationLayers::instance = instance;
}
