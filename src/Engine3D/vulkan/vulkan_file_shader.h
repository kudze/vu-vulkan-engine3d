//
// Created by kkraujelis on 2020-03-16.
//

#ifndef INC_3DENGINE_VULKAN_FILE_SHADER_H
#define INC_3DENGINE_VULKAN_FILE_SHADER_H

#include <string>

#include "vulkan_shader.h"

namespace Engine3D::Vulkan {
    /**
     * Initializes Vulkan::VertexShader from a file.
     */
    class FileVertexShader
        : public Vulkan::VertexShader {
    private:

    public:
        FileVertexShader(Vulkan::Device *device, std::string const &fileName);

        virtual ~FileVertexShader();
    };

    /**
     * Initializes Vulkan::FragmentShader from a file.
     */
    class FileFragmentShader
        : public Vulkan::FragmentShader {
    private:

    public:
        FileFragmentShader(Vulkan::Device *device, std::string const &fileName);

        virtual ~FileFragmentShader();
    };

}


#endif //INC_3DENGINE_VULKAN_FILE_SHADER_H
