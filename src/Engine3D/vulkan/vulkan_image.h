//
// Created by karol on 5/7/2020.
//

#ifndef INC_3DENGINE_VULKAN_IMAGE_H
#define INC_3DENGINE_VULKAN_IMAGE_H

#include <vector>

#include "vulkan_device.h"
#include "vulkan_command_buffer.h"

namespace Engine3D::Vulkan {
    class ImageSampler;

    class Image : public NonCopyable {
        uint32_t width;
        uint32_t height;
        VkFormat format;

        VkImage handle;
        VkDeviceMemory memoryHandle;
        VkImageView view;
        Device* device;
        ImageSampler* sampler = nullptr;

        void transitionLayout(CommandPool *graphicsPool, VkImageLayout oldLayout, VkImageLayout newLayout);
        void uploadData(CommandPool *transferPool, void* data);

    public:
        //TODO: move this somewhere else.
        static bool hasStencilComponent(VkFormat format);
        static VkFormat findDepthFormat(PhysicalDevice *device);
        static VkFormat findSupportedFormat(PhysicalDevice* device, std::vector<VkFormat> const& candidates, VkImageTiling tiling, VkFormatFeatureFlags features);
        static VkImageView createImageView(Device* device, VkImage image, VkFormat format, VkImageAspectFlags aspectFlags = VK_IMAGE_ASPECT_COLOR_BIT);

        Image(
            Device* device,
            CommandPool *transferPool,
            CommandPool *graphicsPool,
            uint32_t width,
            uint32_t height,
            void* data,
            VkFormat format = VK_FORMAT_R8G8B8A8_SRGB,
            VkImageTiling tiling = VK_IMAGE_TILING_OPTIMAL,
            VkImageUsageFlags usageFlags
                = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
            VkMemoryPropertyFlags memoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
            VkImageAspectFlags aspectFlags = VK_IMAGE_ASPECT_COLOR_BIT,
            VkImageLayout layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        );
        virtual ~Image();

        void attachImageSampler(ImageSampler* sampler);
        bool hasSampler() const;
        ImageSampler* getSampler() const;

        VkImage getHandle() const;
        VkDeviceMemory getMemory() const;
        VkImageView getView() const;
        Device* getDevice() const;
        uint32_t getWidth() const;
        uint32_t getHeight() const;
        VkFormat getFormat() const;
    };

}

#include "vulkan_image_sampler.h"

#endif //INC_3DENGINE_VULKAN_IMAGE_H
