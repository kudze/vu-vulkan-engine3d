//
// Created by kkraujelis on 2020-03-15.
//

#ifndef INC_3DENGINE_VULKAN_DEBUG_H
#define INC_3DENGINE_VULKAN_DEBUG_H

#include "vulkan_instance.h"

namespace Engine3D::Vulkan {

    /**
     * Validation (Debug) layers for Vulkan.
     * This enables vulkan debugging.
     */
    class ValidationLayers : public NonCopyable {
    private:
        VkDebugUtilsMessengerEXT handle;
        Vulkan::Instance *instance;

        static VKAPI_ATTR VkBool32
        VKAPI_CALL debugCallback(
            VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
            VkDebugUtilsMessageTypeFlagsEXT messageType,
            const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
            void *pUserData
        );

        PFN_vkCreateDebugUtilsMessengerEXT loadCreateDebugUtilsMessengerFunction(
            Vulkan::Instance *instance
        ) const;

        PFN_vkDestroyDebugUtilsMessengerEXT loadDestroyDebugUtilsMessengerFunction(
            Vulkan::Instance *instance
        ) const;

        void setHandle(VkDebugUtilsMessengerEXT handle);

        void setInstance(Instance *instance);

    public:

        static VkDebugUtilsMessengerCreateInfoEXT createValidationLayerCreateInfo();

        explicit ValidationLayers(Vulkan::Instance *instance);

        ~ValidationLayers();

        VkDebugUtilsMessengerEXT getHandle() const;

        Vulkan::Instance *getInstance() const;
    };
}


#endif //INC_3DENGINE_VULKAN_DEBUG_H
