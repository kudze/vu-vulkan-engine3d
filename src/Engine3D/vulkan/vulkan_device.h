//
// Created by kkraujelis on 2020-03-15.
//

#ifndef INC_3DENGINE_VULKAN_DEVICE_H
#define INC_3DENGINE_VULKAN_DEVICE_H

#include <optional>
#include <vector>
#include <map>

#include <vulkan/vulkan.h>

#include "vulkan_instance.h"
#include "vulkan_surface.h"

namespace Engine3D::Vulkan
    {
        class Surface;

        /**
         * Wrapper around VkPhysicalDevice.
         * Physical device is a GPU.
         */
        class PhysicalDevice : public NonCopyable
        {
            private:
                VkPhysicalDevice handle = VK_NULL_HANDLE;

                mutable std::optional<VkPhysicalDeviceProperties> properties;
                mutable std::optional<VkPhysicalDeviceMemoryProperties> memProperties;
                mutable std::vector<VkQueueFamilyProperties> queueFamilyProperties;
                mutable std::vector<VkExtensionProperties> deviceExtensionProperties;
                mutable std::vector<VkLayerProperties> deviceLayerProperties;
                mutable std::map<VkFormat, VkFormatProperties> deviceFormatProperties;

                void setHandle(VkPhysicalDevice handle);
                void setProperties(const std::optional<VkPhysicalDeviceProperties> &properties);
                void setQueueFamilyProperties(const std::vector<VkQueueFamilyProperties> &queueFamilyProperties);
                void setDeviceExtensionProperties(const std::vector<VkExtensionProperties> &deviceExtensionProperties);
                void setDeviceLayerProperties(const std::vector<VkLayerProperties> &deviceLayerProperties);

            public:
                explicit PhysicalDevice(VkPhysicalDevice device);

                std::vector<VkLayerProperties> const& getAvailableDeviceLayerProperties() const;
                std::map<VkFormat, VkFormatProperties> const& getAvailableDeviceFormatProperties() const;
                VkFormatProperties const& getDeviceFormatProperties(VkFormat format) const;
                std::vector<VkExtensionProperties> const& getAvailableDeviceExtensionProperties() const;
                std::vector<VkQueueFamilyProperties> const& getQueueFamilyProperties() const;
                VkPhysicalDeviceMemoryProperties const& getMemoryProperties() const;
                VkPhysicalDeviceProperties const& getProperties() const;

                VkPhysicalDevice getHandle() const;
                std::string getName() const;
                uint32_t getQueueGraphicsFamilyIndex() const;
                uint32_t getQueuePresentFamilyIndex(Vulkan::Surface* surface) const;
                uint32_t getQueueTransferFamilyIndex() const;
                bool isDeviceExtensionValid(std::string const& extension) const;
                bool isDeviceLayerValid(std::string const& layer) const;
                uint32_t findMemoryTypeIndex(uint32_t typeFilter, VkMemoryPropertyFlags properties) const;
        };

        /**
         * Manager to manage all physical instances of GPU.
         */
        class PhysicalDeviceManager : public NonCopyable
        {
        private:
                Vulkan::Instance* instance = nullptr;

                void setInstance(Instance *instance);
        public:

                explicit PhysicalDeviceManager(Vulkan::Instance* instance);
                ~PhysicalDeviceManager() = default;

                /**
                 * Returns a list of all available physical deivces (GPUs)
                 * @return
                 */
                std::vector<PhysicalDevice*> getAllPhysicalDevices() const;

                Instance *getInstance() const;
        };

        /**
         * Wrapper around VkDevice.
         * This class stores logical handle to GPU.
         * Initializes all extensions and layers required by our application.
         */
        class Device : public NonCopyable
        {
            private:
                Vulkan::PhysicalDevice* device = nullptr;
                VkDevice handle = VK_NULL_HANDLE;
                VkQueue graphicsQueue = VK_NULL_HANDLE;
                uint32_t graphicsQueueFamilyIndex = 0;
                VkQueue presentQueue = VK_NULL_HANDLE;
                uint32_t presentQueueFamilyIndex = 0;
                VkQueue transferQueue = VK_NULL_HANDLE;
                uint32_t transferQueueFamilyIndex = 0;

                void setDevice(PhysicalDevice *device);
                void setHandle(VkDevice handle);
                void setGraphicsQueue(VkQueue graphicsQueue);
                void setGraphicsQueueFamilyIndex(uint32_t graphicsQueueFamilyIndex);
                void setPresentQueue(VkQueue presentQueue);
                void setPresentQueueFamilyIndex(uint32_t presentQueueFamilyIndex);
                void setTransferQueue(VkQueue transferQueue);
                void setTransferQueueFamilyIndex(uint32_t transferQueueFamilyIndex);

        public:
                Device(PhysicalDevice* device, Surface* surface);
                ~Device();

                void waitTillIdle() const;

                Vulkan::PhysicalDevice* getPhysicalDevice() const;
                VkDevice getHandle() const;
                VkQueue getGraphicsQueue() const;
                VkQueue getPresentQueue() const;
                VkQueue getTransferQueue() const;
                uint32_t getGraphicsQueueFamilyIndex() const;
                uint32_t getPresentQueueFamilyIndex() const;
                uint32_t getTransferQueueFamilyIndex() const;
        };

}


#endif //INC_3DENGINE_VULKAN_DEVICE_H
