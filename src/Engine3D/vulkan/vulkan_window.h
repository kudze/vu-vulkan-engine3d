//
// Created by kkraujelis on 2020-02-29.
//

#ifndef INC_3DENGINE_VULKAN_WINDOW_H
#define INC_3DENGINE_VULKAN_WINDOW_H

#include "../window/window.h"
#include <vector>
#include <string>

namespace Engine3D::Vulkan {
    /**
     * @extens Engine3D::Window
     *
     * Engine3D::Window with some additional features for Vulkan.
     */
    class Window
        : public Engine3D::Window {
    protected:
        virtual uint32_t constructFlags(Type type) const override;

    public:
        Window(Type type, std::string const &name, Dimensions<uint16_t> const &size, bool resizeable = false);

        virtual ~Window();

        Dimensions<uint16_t> getDrawableSize() const;

        std::vector<std::string> getRequiredVulkanInstanceExtensions() const;

        std::vector<std::string> getRequiredVulkanInstanceLayers() const;

    };
}


#endif //INC_3DENGINE_VULKAN_WINDOW_H
