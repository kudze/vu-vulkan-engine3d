//
// Created by karol on 5/8/2020.
//

#ifndef INC_3DENGINE_VULKAN_FILE_IMAGE_H
#define INC_3DENGINE_VULKAN_FILE_IMAGE_H

#include "vulkan_image.h"

namespace Engine3D::Vulkan {

    class FileImage
    {
    public:
        static Image* createImageFromFile(
            Device* device,
            CommandPool *transferPool,
            CommandPool *graphicsPool,
            std::string const& fileName,
            VkFormat format = VK_FORMAT_R8G8B8A8_SRGB,
            VkImageTiling tiling = VK_IMAGE_TILING_OPTIMAL,
            VkImageUsageFlags usageFlags
                = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
            VkMemoryPropertyFlags memoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
            VkImageAspectFlags aspectFlags = VK_IMAGE_ASPECT_COLOR_BIT,
            VkImageLayout layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        );
    };

}


#endif //INC_3DENGINE_VULKAN_FILE_IMAGE_H
