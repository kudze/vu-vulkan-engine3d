//
// Created by karol on 5/19/2020.
//

#include "vulkan_font.h"

using namespace Engine3D;
using namespace Vulkan;

Character::Character(
    Engine3D::Vulkan::Image *image,
    glm::ivec2 const &size,
    glm::ivec2 const &bearing,
    uint32_t advance
)
    : image(image), size(size), bearing(bearing), advance(advance) {

}

Character::~Character() {
    delete this->image;
}

Vulkan::Image *Character::getImage() const {
    return this->image;
}

const glm::ivec2 &Character::getSize() const {
    return this->size;
}

const glm::ivec2 &Character::getBearing() const {
    return this->bearing;
}

uint32_t Character::getAdvance() const {
    return this->advance;
}

FT_Library Font::library;
size_t Font::instancesCount = 0;

void Font::initializeLibrary() {
    Font::instancesCount++;

    if (Font::instancesCount == 1) {
        auto error = FT_Init_FreeType(&Font::library);

        if (error)
            throw std::runtime_error("Failed to load FreeType library!\nError:" + error);
    }
}

void Font::deinitializeLibrary() {
    Font::instancesCount--;

    if(Font::instancesCount == 0)
        FT_Done_FreeType(Font::library);
}

Font::Font(Renderer *renderer, std::string const &filename, uint8_t fontSize, std::string const &charset) {
    Font::initializeLibrary();

    FT_Face face;

    auto error = FT_New_Face(
        this->library,
        filename.c_str(),
        0,
        &face
    );

    if (error == FT_Err_Unknown_File_Format)
        throw std::runtime_error("Invalid font file format at: " + filename);

    else if (error)
        throw std::runtime_error("Failed to load font file (" + filename + "), erorr: " + std::to_string(error));

    error = FT_Set_Pixel_Sizes(
        face,
        0,
        fontSize
    );

    if (error)
        throw std::runtime_error("Failed to set character size of the font: " + std::to_string(error));

    for (auto letter : charset) {
        error = FT_Load_Char(face, letter, FT_LOAD_RENDER);

        if (error)
            throw std::runtime_error("Failed to load character " + std::to_string(letter) + " from font: " + filename);

        auto image = new Vulkan::Image(
            renderer->getDevice(),
            renderer->getTransferCommandPool(),
            renderer->getGraphicsCommandPool(),
            face->glyph->bitmap.width,
            face->glyph->bitmap.rows,
            face->glyph->bitmap.buffer,
            VK_FORMAT_R8_SRGB,
            VK_IMAGE_TILING_OPTIMAL,
            VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
            VK_IMAGE_ASPECT_COLOR_BIT,
            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        );

        image->attachImageSampler(
            new ImageSampler(
                image,
                VK_FILTER_LINEAR,
                16,
                VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE
            )
        );

        this->charset.insert(
            std::pair<char, Character *>(
                letter,
                new Character(
                    image,
                    glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
                    glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
                    face->glyph->advance.x
                )
            )
        );
    }

    FT_Done_Face(face);
}

Font::~Font() {

    for (auto const &pair : this->charset)
        delete pair.second;

    //Maybe this should be moved to end of the constructor.
    //TODO: figure out.
    Font::deinitializeLibrary();
}

Character* Font::getCharacterInfo(char letter) const {
    return this->charset.at(letter);
}