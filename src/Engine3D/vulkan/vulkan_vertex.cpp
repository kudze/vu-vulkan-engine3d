//
// Created by karol on 4/5/2020.
//

#include "vulkan_vertex.h"

using namespace Engine3D;
using namespace Vulkan;

void Vertex::setPosition(const glm::vec3 &pos) {
    this->pos = pos;
}

void Vertex::setColor(const glm::vec3 &color) {
    this->col = color;
}

void Vertex::setTextureCoordinates(glm::vec2 const &tex) {
    this->tex = tex;
}

VkVertexInputBindingDescription Vertex::buildBindingDescription() {
    VkVertexInputBindingDescription bindingDescription = {};

    bindingDescription.binding = 0;
    bindingDescription.stride = sizeof(Vertex);
    bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    return bindingDescription;
}

std::array<VkVertexInputAttributeDescription, 3> Vertex::buildAttributeDescriptions() {
    std::array<VkVertexInputAttributeDescription, 3> attributeDescriptions = {};

    attributeDescriptions[0].binding = 0;
    attributeDescriptions[0].location = 0;
    attributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
    attributeDescriptions[0].offset = offsetof(Vertex, pos);

    attributeDescriptions[1].binding = 0;
    attributeDescriptions[1].location = 1;
    attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
    attributeDescriptions[1].offset = offsetof(Vertex, col);

    attributeDescriptions[2].binding = 0;
    attributeDescriptions[2].location = 2;
    attributeDescriptions[2].format = VK_FORMAT_R32G32_SFLOAT;
    attributeDescriptions[2].offset = offsetof(Vertex, tex);

    return attributeDescriptions;
}

Vertex::Vertex(const glm::vec3 &pos, const glm::vec3 &col, const glm::vec2 &tex)
    : pos(pos), col(col), tex(tex) {

}

glm::vec3 const &Vertex::getPosition() const {
    return this->pos;
}

const glm::vec3 &Vertex::getColor() const {
    return this->col;
}

const glm::vec2 &Vertex::getTextureCoordinates() const {
    return this->tex;
}

bool Vertex::operator==(const Vertex &other) const {
    return this->pos == other.pos && this->col == other.col && this->tex == other.tex;
}

#define GLM_ENABLE_EXPERIMENTAL

#include <gtx/hash.hpp>

size_t std::hash<Vertex>::operator()(Vertex const &vertex) const {
    return ((hash<glm::vec3>()(vertex.getPosition()) ^
             (hash<glm::vec3>()(vertex.getColor()) << 1)) >> 1) ^
           (hash<glm::vec2>()(vertex.getTextureCoordinates()) << 1);
}
