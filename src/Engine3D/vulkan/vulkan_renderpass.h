//
// Created by kkraujelis on 2020-03-16.
//

#ifndef INC_3DENGINE_VULKAN_RENDERPASS_H
#define INC_3DENGINE_VULKAN_RENDERPASS_H

#include "vulkan_device.h"

namespace Engine3D::Vulkan {
    class Swapchain;

    /**
     * Wrapper around VkRenderPass
     * Describes all attachments used by frame buffers.
     */
    class RenderPass : public NonCopyable {
    private:
        VkRenderPass handle = VK_NULL_HANDLE;
        Vulkan::Device *device = nullptr;

        void setHandle(VkRenderPass handle);

        void setDevice(Device *device);

    public:
        RenderPass(Vulkan::Device *device, Vulkan::Swapchain *swapchain);

        ~RenderPass();

        VkRenderPass getHandle() const;

        Vulkan::Device *getDevice() const;
    };

}


#endif //INC_3DENGINE_VULKAN_RENDERPASS_H
