//
// Created by karol on 5/18/2020.
//

#include "vulkan_file_obj_modej.h"
#include "vulkan_vertex.h"

#define TINYOBJLOADER_IMPLEMENTATION

#include <tiny_obj_loader.h>

using namespace Engine3D;
using namespace Vulkan;

FileOBJModel::FileOBJModel(Device* device, CommandPool* transferCommandPool, std::string const &fileName)
    : Model(nullptr, nullptr) {
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string warn, err;

    if (!tinyobj::LoadObj(
        &attrib,
        &shapes,
        &materials,
        &warn,
        &err,
        fileName.c_str()
    ))
        throw std::runtime_error("Failed to load model!\nPath: " + fileName + "\nWarn: " + warn + "\nError: " + err);

    std::vector<Vertex> vertices;
    std::vector<index_t> indices;

    std::unordered_map<Vertex, uint32_t> uniqueVertices{};

    for (const auto& shape : shapes) {
        for (const auto& index : shape.mesh.indices) {
            Vertex vertex(
                glm::vec3(
                    attrib.vertices[3 * index.vertex_index + 0],
                    attrib.vertices[3 * index.vertex_index + 1],
                    attrib.vertices[3 * index.vertex_index + 2]
                ),
                glm::vec3(
                    1.0f,
                    1.0f,
                    1.0f
                ),
                glm::vec2(
                    attrib.texcoords[2 * index.texcoord_index + 0],
                    attrib.texcoords[2 * index.texcoord_index + 1]
                )
            );

            if (uniqueVertices.count(vertex) == 0) {
                uniqueVertices[vertex] = static_cast<uint32_t>(vertices.size());
                vertices.push_back(vertex);
            }

            indices.push_back(uniqueVertices[vertex]);
        }
    }

    this->setIndexBuffer(new IndexBuffer(device, transferCommandPool, indices.data(), indices.size()));
    this->setVertexBuffer(new VertexBuffer(device, transferCommandPool, vertices.data(), vertices.size()));
}

FileOBJModel::~FileOBJModel() = default;