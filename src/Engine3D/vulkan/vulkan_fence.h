//
// Created by karol on 4/5/2020.
//

#ifndef INC_3DENGINE_VULKAN_FENCE_H
#define INC_3DENGINE_VULKAN_FENCE_H

#include "vulkan_device.h"

namespace Engine3D::Vulkan {

    /**
     * Wrapper around VkFence.
     *
     * Fence is used to sync GPU Command Buffers to CPU work.
     */
    class Fence : public NonCopyable {
    private:
        VkFence handle = VK_NULL_HANDLE;
        Vulkan::Device *device = nullptr;

        void setHandle(VkFence handle);

        void setDevice(Vulkan::Device *device);

    public:
        Fence(Vulkan::Device *device, bool enabledAfterCreation);

        ~Fence();

        void wait();

        void reset();

        VkFence getHandle() const;

        Vulkan::Device *getDevice() const;
    };

}


#endif //INC_3DENGINE_VULKAN_FENCE_H
