//
// Created by kkraujelis on 2020-03-16.
//

#include "vulkan_command_pool.h"
#include "vulkan_swapchain.h"
#include "vulkan_graphics_pipeline.h"
#include "../scene/object.h"

using namespace Engine3D;
using namespace Vulkan;

CommandPool::CommandPool(
    Engine3D::Vulkan::Device* device,
    VkQueue queueFamily,
    uint32_t queueFamilyIndex
)
    : device(device), queue(queueFamily)
{
    VkCommandPoolCreateInfo poolInfo = {};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex = queueFamilyIndex;
    poolInfo.flags = 0;

    if(vkCreateCommandPool(
        this->getDevice()->getHandle(),
        &poolInfo,
        nullptr,
        &this->handle
    ) != VK_SUCCESS)
        throw std::runtime_error("Failed to create vulkan command pool!");
}

CommandPool::~CommandPool()
{
    vkDestroyCommandPool(
        this->getDevice()->getHandle(),
        this->getHandle(),
        nullptr
    );
}

VkCommandPool CommandPool::getHandle() const
{
    return this->handle;
}

Vulkan::Device* CommandPool::getDevice() const
{
    return this->device;
}

void CommandPool::setQueue(VkQueue queue) {
    this->queue = queue;
}

void CommandPool::setDevice(Device *device) {
    CommandPool::device = device;
}

void CommandPool::setHandle(VkCommandPool handle) {
    CommandPool::handle = handle;
}

VkQueue CommandPool::getQueue() const {
    return this->queue;
}
