//
// Created by karol on 5/7/2020.
//

#ifndef INC_3DENGINE_VULKAN_STAGING_BUFFER_H
#define INC_3DENGINE_VULKAN_STAGING_BUFFER_H

#include "vulkan_buffer.h"
#include "vulkan_device.h"

namespace Engine3D::Vulkan {

    class StagingBuffer
        : public Buffer
    {

    public:
        StagingBuffer(Device* device, VkDeviceSize size, VkSharingMode sharingMode);
        ~StagingBuffer();

    };

}


#endif //INC_3DENGINE_VULKAN_STAGING_BUFFER_H
