//
// Created by kkraujelis on 2020-03-16.
//

#include "vulkan_shader.h"

using namespace Engine3D;
using namespace Vulkan;

Shader::Shader(Vulkan::Device* device, std::vector<char> const& shader)
    : device(device)
{
    VkShaderModuleCreateInfo shaderModuleCreateInfo = {};
    shaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    shaderModuleCreateInfo.codeSize = shader.size();
    shaderModuleCreateInfo.pCode = reinterpret_cast<const uint32_t*>(shader.data());

    if(vkCreateShaderModule(
        this->getDevice()->getHandle(),
        &shaderModuleCreateInfo,
        nullptr,
        &this->handle
    ) != VK_SUCCESS)
        throw std::runtime_error("Failed to create vulkan shader!");
}

Shader::~Shader()
{
    vkDestroyShaderModule(
        this->device->getHandle(),
        this->getHandle(),
        nullptr
    );
}

void Shader::addUBOLayoutBinding(uint32_t binding, VkDescriptorType descriptorType, VkShaderStageFlags stageFlags) {
    VkDescriptorSetLayoutBinding descriptorSetLayoutBinding = {};
    descriptorSetLayoutBinding.binding = binding;
    descriptorSetLayoutBinding.descriptorCount = 1;
    descriptorSetLayoutBinding.descriptorType = descriptorType;
    descriptorSetLayoutBinding.stageFlags = stageFlags;
    descriptorSetLayoutBinding.pImmutableSamplers = nullptr;

    this->uboLayoutBindings.emplace_back(descriptorSetLayoutBinding);
}

Vulkan::Device * Shader::getDevice() const {
    return this->device;
}

VkShaderModule Shader::getHandle() const {
    return this->handle;
}

void Shader::setHandle(VkShaderModule handle) {
    Shader::handle = handle;
}

void Shader::setDevice(Device *device) {
    Shader::device = device;
}

const std::vector<VkDescriptorSetLayoutBinding> & Shader::getUBOLayoutBindings() const {
    return this->uboLayoutBindings;
}

VertexShader::VertexShader(Device* device, std::vector<char> const& shader)
    : Shader(device, shader)
{

}

VertexShader::~VertexShader() = default;

FragmentShader::FragmentShader(Device* device, std::vector<char> const& shader)
    : Shader(device, shader)
{

}

FragmentShader::~FragmentShader() = default;