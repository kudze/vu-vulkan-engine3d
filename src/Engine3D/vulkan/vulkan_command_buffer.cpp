//
// Created by karol on 4/14/2020.
//

#include "vulkan_command_buffer.h"

using namespace Engine3D;
using namespace Vulkan;

CommandBuffer::CommandBuffer(
    CommandPool *pool,
    VkCommandBufferLevel level
)
    : pool(pool) {

    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = level;
    allocInfo.commandPool = pool->getHandle();
    allocInfo.commandBufferCount = 1;

    vkAllocateCommandBuffers(
        pool->getDevice()->getHandle(),
        &allocInfo,
        &this->handle
    );
}

CommandBuffer::~CommandBuffer() {
    vkFreeCommandBuffers(
        this->getPool()->getDevice()->getHandle(),
        this->getPool()->getHandle(),
        1,
        &this->handle
    );
};

Vulkan::CommandPool *CommandBuffer::getPool() const {
    return this->pool;
}

VkCommandBuffer CommandBuffer::getHandle() const {
    return this->handle;
}

void CommandBuffer::setPool(Engine3D::Vulkan::CommandPool *pool) {
    this->pool = pool;
}

void CommandBuffer::setHandle(VkCommandBuffer handle) {
    this->handle = handle;
}

void CommandBuffer::beginRecording(VkCommandBufferUsageFlags flags) const {
    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = flags;

    vkBeginCommandBuffer(this->getHandle(), &beginInfo);
}

void CommandBuffer::endRecording() const {
    vkEndCommandBuffer(this->getHandle());
}

#include <iostream>

void CommandBuffer::submit(
    Vulkan::Fence *fence,
    std::vector<Vulkan::Semaphore *> const &waitSemaphores,
    std::vector<Vulkan::Semaphore *> const &signalSemaphores,
    VkPipelineStageFlags const *waitStages
) const {
    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &this->handle;
    submitInfo.pWaitDstStageMask = waitStages;

    std::vector<VkSemaphore> waitSemaphoreHandles(waitSemaphores.size());
    if (!waitSemaphores.empty()) {
        for (size_t i = 0; i < waitSemaphores.size(); i++)
            waitSemaphoreHandles.at(i) = waitSemaphores.at(i)->getHandle();

        submitInfo.waitSemaphoreCount = static_cast<uint32_t>(waitSemaphoreHandles.size());
        submitInfo.pWaitSemaphores = waitSemaphoreHandles.data();
    }

    std::vector<VkSemaphore> signalSemaphoreHandles(signalSemaphores.size());
    if (!signalSemaphores.empty()) {
        for (size_t i = 0; i < signalSemaphores.size(); i++)
            signalSemaphoreHandles.at(i) = signalSemaphores.at(i)->getHandle();

        submitInfo.signalSemaphoreCount = static_cast<uint32_t>(signalSemaphoreHandles.size());
        submitInfo.pSignalSemaphores = signalSemaphoreHandles.data();
    }

    VkFence fenceHandle = VK_NULL_HANDLE;
    if(fence != nullptr) {
        fenceHandle = fence->getHandle();

        fence->reset();
    }

    if (vkQueueSubmit(
        this->getPool()->getQueue(),
        1,
        &submitInfo,
        fenceHandle
    ) != VK_SUCCESS)
        throw std::runtime_error("Failed to submit command buffer to queue!");
}