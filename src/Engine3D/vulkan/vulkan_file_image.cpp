//
// Created by karol on 5/8/2020.
//

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include "vulkan_file_image.h"

using namespace Engine3D;
using namespace Vulkan;

Image *FileImage::createImageFromFile(
    Device* device,
    CommandPool *transferPool,
    CommandPool *graphicsPool,
    std::string const& fileName,
    VkFormat format,
    VkImageTiling tiling,
    VkImageUsageFlags usageFlags,
    VkMemoryPropertyFlags memoryPropertyFlags,
    VkImageAspectFlags aspectFlags,
    VkImageLayout layout
    ) {
    int width, height, channels;

    auto pixels = stbi_load(fileName.c_str(), &width, &height, &channels, STBI_rgb_alpha);

    if (!pixels)
        throw std::runtime_error("Failed to load texture image from file: " + fileName);

    auto result = new Image(
        device,
        transferPool,
        graphicsPool,
        static_cast<uint32_t>(width),
        static_cast<uint32_t>(height),
        pixels,
        format,
        tiling,
        usageFlags,
        memoryPropertyFlags,
        aspectFlags,
        layout
    );

    stbi_image_free(pixels);

    return result;
}