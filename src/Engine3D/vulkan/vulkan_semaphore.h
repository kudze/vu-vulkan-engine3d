//
// Created by kkraujelis on 2020-03-16.
//

#ifndef INC_3DENGINE_VULKAN_SEMAPHORE_H
#define INC_3DENGINE_VULKAN_SEMAPHORE_H

#include "vulkan_device.h"

namespace Engine3D::Vulkan {

    /**
     * Wrapper around VkSemaphore.
     * Semaphores are used to sync GPU tasks.
     */
    class Semaphore : public NonCopyable {
    private:
        VkSemaphore handle = VK_NULL_HANDLE;
        Vulkan::Device *device = nullptr;

        void setHandle(VkSemaphore handle);

        void setDevice(Device *device);

    public:
        explicit Semaphore(Vulkan::Device *device);

        ~Semaphore();

        VkSemaphore getHandle() const;

        Vulkan::Device *getDevice() const;
    };
}


#endif //INC_3DENGINE_VULKAN_SEMAPHORE_H
