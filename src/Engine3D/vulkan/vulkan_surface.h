//
// Created by kkraujelis on 2020-03-15.
//

#ifndef INC_3DENGINE_VULKAN_SURFACE_H
#define INC_3DENGINE_VULKAN_SURFACE_H

#include "vulkan_window.h"
#include "vulkan_instance.h"
#include "vulkan_device.h"

namespace Engine3D::Vulkan {
    class PhysicalDevice;

    /**
     * Wrapper around VkSurface.
     * Surface describes window to Vulkan.
     */
    class Surface : public NonCopyable {
    private:
        Vulkan::Instance *instance = nullptr;
        VkSurfaceKHR handle = VK_NULL_HANDLE;

        void setInstance(Instance *instance);

        void setHandle(VkSurfaceKHR handle);

    public:
        Surface(Vulkan::Window *window, Vulkan::Instance *instance);

        ~Surface();

        Vulkan::Instance *getInstance() const;

        VkSurfaceKHR getHandle() const;

        VkSurfaceCapabilitiesKHR getCapabilities(Vulkan::PhysicalDevice *device) const;

        std::vector<VkSurfaceFormatKHR> getAvailableFormats(Vulkan::PhysicalDevice *device) const;

        std::vector<VkPresentModeKHR> getAvailablePresentModes(Vulkan::PhysicalDevice *device) const;
    };
}


#endif //INC_3DENGINE_VULKAN_SURFACE_H
