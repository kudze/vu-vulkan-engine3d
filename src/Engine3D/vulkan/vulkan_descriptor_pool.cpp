//
// Created by karol on 4/7/2020.
//

#include "vulkan_descriptor_pool.h"

using namespace Engine3D;
using namespace Vulkan;

DescriptorPool::DescriptorPool(Engine3D::Vulkan::Device *device, size_t maxUniformBuffers)
    : device(device) {
    std::vector<VkDescriptorPoolSize> descriptorPoolSizes = {};

    VkDescriptorPoolSize uniformPoolSize = {};
    uniformPoolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uniformPoolSize.descriptorCount = static_cast<uint32_t>(maxUniformBuffers);
    descriptorPoolSizes.push_back(uniformPoolSize);

    VkDescriptorPoolSize samplerPoolSize = {};
    samplerPoolSize.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    samplerPoolSize.descriptorCount = static_cast<uint32_t>(maxUniformBuffers);
    descriptorPoolSizes.push_back(samplerPoolSize);

    VkDescriptorPoolCreateInfo descriptorPoolCreateInfo = {};
    descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptorPoolCreateInfo.poolSizeCount = static_cast<uint32_t>(descriptorPoolSizes.size());
    descriptorPoolCreateInfo.pPoolSizes = descriptorPoolSizes.data();
    descriptorPoolCreateInfo.maxSets = static_cast<uint32_t>(maxUniformBuffers);

    if (vkCreateDescriptorPool(
        this->getDevice()->getHandle(),
        &descriptorPoolCreateInfo,
        nullptr,
        &this->handle
    ) != VK_SUCCESS)
        throw std::runtime_error("Failed to create descriptor pool!");
}

DescriptorPool::~DescriptorPool() {
    vkDestroyDescriptorPool(
        this->getDevice()->getHandle(),
        this->getHandle(),
        nullptr
    );
}

void DescriptorPool::setHandle(VkDescriptorPool handle) {
    this->handle = handle;
}

void DescriptorPool::setDevice(Engine3D::Vulkan::Device *device) {
    this->device = device;
}

VkDescriptorPool DescriptorPool::getHandle() const {
    return this->handle;
}

Vulkan::Device *DescriptorPool::getDevice() const {
    return this->device;
}