//
// Created by karol on 4/5/2020.
//

#ifndef INC_3DENGINE_VULKAN_INDEX_BUFFER_H
#define INC_3DENGINE_VULKAN_INDEX_BUFFER_H

#include "vulkan_buffer.h"
#include "vulkan_command_pool.h"

namespace Engine3D::Vulkan {
    /**
     * Describes how indexes should be stored on GPU.
     */
    typedef uint32_t index_t;

    /**
     * Index buffer.
     *
     * Indexes are used to describe in which order to render vertices.
     */
    class IndexBuffer
        : public Buffer {
        uint32_t indexCount;

    public:
        IndexBuffer(
            Vulkan::Device *device,
            Vulkan::CommandPool *transferCommandPool,
            index_t const *data,
            uint32_t indexCount,
            VkSharingMode sharingMode = VK_SHARING_MODE_EXCLUSIVE
        );

        virtual ~IndexBuffer();

        uint32_t getIndexCount() const;
    };
}


#endif //INC_3DENGINE_VULKAN_INDEX_BUFFER_H
