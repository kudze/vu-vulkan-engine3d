//
// Created by kkraujelis on 2020-03-16.
//

#include "vulkan_graphics_pipeline.h"
#include "vulkan_vertex.h"

using namespace Engine3D;
using namespace Vulkan;

GraphicsPipelineDescription::GraphicsPipelineDescription(
    VertexShader *vertexShader,
    FragmentShader *fragmentShader,
    graphicsPipelineIndex_t uniqueID
) : vertexShader(vertexShader), fragmentShader(fragmentShader), uniqueID(uniqueID) {

}

GraphicsPipelineDescription::~GraphicsPipelineDescription() {
    delete this->vertexShader;
    delete this->fragmentShader;
}

VertexShader *GraphicsPipelineDescription::getVertexShader() const {
    return this->vertexShader;
}

FragmentShader *GraphicsPipelineDescription::getFragmentShader() const {
    return this->fragmentShader;
}

graphicsPipelineIndex_t GraphicsPipelineDescription::getUniqueID() const {
    return this->uniqueID;
}

GraphicsPipeline::GraphicsPipeline(
    GraphicsPipelineDescription *description,
    Vulkan::Swapchain *swapchain,
    Vulkan::Device *device,
    Vulkan::RenderPass *renderpass
)
    : device(device) {

    auto vertexDescriptorBindings = description->getVertexShader()->getUBOLayoutBindings();
    auto fragmentDescriptorBindings = description->getFragmentShader()->getUBOLayoutBindings();

    auto descriptorBindings = std::vector<VkDescriptorSetLayoutBinding>();
    descriptorBindings.insert(descriptorBindings.end(), vertexDescriptorBindings.begin(),
                              vertexDescriptorBindings.end());
    descriptorBindings.insert(descriptorBindings.end(), fragmentDescriptorBindings.begin(),
                              fragmentDescriptorBindings.end());

    VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo = {};
    descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptorSetLayoutCreateInfo.bindingCount = static_cast<uint32_t>(descriptorBindings.size());
    descriptorSetLayoutCreateInfo.pBindings = descriptorBindings.data();

    if (vkCreateDescriptorSetLayout(
        this->getDevice()->getHandle(),
        &descriptorSetLayoutCreateInfo,
        nullptr,
        &this->descriptorSetLayout
    ) != VK_SUCCESS)
        throw std::runtime_error("Failed to create descriptor set layout!");

    VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
    pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = 1;
    pipelineLayoutInfo.pSetLayouts = &this->descriptorSetLayout;
    pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
    pipelineLayoutInfo.pPushConstantRanges = nullptr; // Optional

    if (vkCreatePipelineLayout(this->device->getHandle(), &pipelineLayoutInfo, nullptr, &this->layout) != VK_SUCCESS)
        throw std::runtime_error("failed to create pipeline layout!");

    VkPipelineShaderStageCreateInfo vertexShaderCreateInfo = {};
    vertexShaderCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    vertexShaderCreateInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
    vertexShaderCreateInfo.pName = "main";
    vertexShaderCreateInfo.module = description->getVertexShader()->getHandle();

    VkPipelineShaderStageCreateInfo fragmentShaderCreateInfo = {};
    fragmentShaderCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    fragmentShaderCreateInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    fragmentShaderCreateInfo.pName = "main";
    fragmentShaderCreateInfo.module = description->getFragmentShader()->getHandle();

    VkPipelineShaderStageCreateInfo shaders[] = {vertexShaderCreateInfo, fragmentShaderCreateInfo};

    auto bindingDescription = Vertex::buildBindingDescription();
    auto attributeDescriptions = Vertex::buildAttributeDescriptions();

    VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
    vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputInfo.vertexBindingDescriptionCount = 1;
    vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
    vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
    vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

    VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
    inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    inputAssembly.primitiveRestartEnable = VK_FALSE;

    auto extent = swapchain->getExtent();

    VkViewport viewport = {};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = (float) extent.width;
    viewport.height = (float) extent.height;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    VkRect2D scissor = {};
    scissor.offset = {0, 0};
    scissor.extent = extent;

    VkPipelineViewportStateCreateInfo viewportState = {};
    viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;

    VkPipelineRasterizationStateCreateInfo rasterizer = {};
    rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizer.lineWidth = 1.0f;
    rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
    rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterizer.depthBiasEnable = VK_FALSE;
    rasterizer.depthBiasConstantFactor = 0.0f; // Optional
    rasterizer.depthBiasClamp = 0.0f; // Optional
    rasterizer.depthBiasSlopeFactor = 0.0f; // Optional

    VkPipelineMultisampleStateCreateInfo multisampling = {};
    multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisampling.minSampleShading = 1.0f; // Optional
    multisampling.pSampleMask = nullptr; // Optional
    multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
    multisampling.alphaToOneEnable = VK_FALSE; // Optional

    VkPipelineDepthStencilStateCreateInfo depthStencil = {};
    depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencil.depthTestEnable = VK_TRUE;
    depthStencil.depthWriteEnable = VK_TRUE;
    depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
    depthStencil.depthBoundsTestEnable = VK_FALSE;
    depthStencil.minDepthBounds = 0.0f; // Optional
    depthStencil.maxDepthBounds = 1.0f; // Optional
    depthStencil.stencilTestEnable = VK_FALSE;
    depthStencil.front = {}; // Optional
    depthStencil.back = {}; // Optional

    VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
    colorBlendAttachment.colorWriteMask =
        VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    colorBlendAttachment.blendEnable = VK_TRUE;
    colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA; // Optional
    colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA; // Optional
    colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD; // Optional
    colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
    colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
    colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD; // Optional

    VkPipelineColorBlendStateCreateInfo colorBlending = {};
    colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlending.logicOpEnable = VK_FALSE;
    colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
    colorBlending.attachmentCount = 1;
    colorBlending.pAttachments = &colorBlendAttachment;
    colorBlending.blendConstants[0] = 0.0f; // Optional
    colorBlending.blendConstants[1] = 0.0f; // Optional
    colorBlending.blendConstants[2] = 0.0f; // Optional
    colorBlending.blendConstants[3] = 0.0f; // Optional

    VkGraphicsPipelineCreateInfo pipelineInfo = {};
    pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineInfo.stageCount = 2;
    pipelineInfo.pStages = shaders;
    pipelineInfo.pVertexInputState = &vertexInputInfo;
    pipelineInfo.pInputAssemblyState = &inputAssembly;
    pipelineInfo.pViewportState = &viewportState;
    pipelineInfo.pRasterizationState = &rasterizer;
    pipelineInfo.pMultisampleState = &multisampling;
    pipelineInfo.pDepthStencilState = &depthStencil; // Optional
    pipelineInfo.pColorBlendState = &colorBlending;
    pipelineInfo.pDynamicState = nullptr; // Optional
    pipelineInfo.layout = this->getLayoutHandle();
    pipelineInfo.renderPass = renderpass->getHandle();
    pipelineInfo.subpass = 0;
    pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
    pipelineInfo.basePipelineIndex = -1; // Optional

    if (vkCreateGraphicsPipelines(
        this->getDevice()->getHandle(),
        VK_NULL_HANDLE,
        1,
        &pipelineInfo,
        nullptr,
        &this->handle
    ) != VK_SUCCESS)
        throw std::runtime_error("Failed to create Vulkan Graphics Pipeline!");
}

GraphicsPipeline::~GraphicsPipeline() {
    vkDestroyPipeline(
        this->getDevice()->getHandle(),
        this->getHandle(),
        nullptr
    );

    vkDestroyPipelineLayout(
        this->getDevice()->getHandle(),
        this->getLayoutHandle(),
        nullptr
    );

    vkDestroyDescriptorSetLayout(
        this->getDevice()->getHandle(),
        this->getDescriptorSetLayerHandle(),
        nullptr
    );
}

VkPipeline GraphicsPipeline::getHandle() const {
    return this->handle;
}

VkDescriptorSetLayout GraphicsPipeline::getDescriptorSetLayerHandle() const {
    return this->descriptorSetLayout;
}

VkPipelineLayout GraphicsPipeline::getLayoutHandle() const {
    return this->layout;
}

Device *GraphicsPipeline::getDevice() const {
    return this->device;
}

void GraphicsPipeline::setDescriptorSetLayoutHandle(VkDescriptorSetLayout descriptorSetLayout) {
    this->descriptorSetLayout = descriptorSetLayout;
}

void GraphicsPipeline::setLayout(VkPipelineLayout layout) {
    GraphicsPipeline::layout = layout;
}

void GraphicsPipeline::setHandle(VkPipeline handle) {
    GraphicsPipeline::handle = handle;
}

void GraphicsPipeline::setDevice(Device *device) {
    GraphicsPipeline::device = device;
}
