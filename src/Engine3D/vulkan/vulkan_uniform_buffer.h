//
// Created by karol on 4/7/2020.
//

#ifndef INC_3DENGINE_VULKAN_UNIFORM_BUFFER_H
#define INC_3DENGINE_VULKAN_UNIFORM_BUFFER_H

#include "vulkan_buffer.h"
#include "vulkan_descriptor_pool.h"
#include "vulkan_graphics_pipeline.h"
#include "vulkan_image.h"

#include <vulkan/vulkan.h>

namespace Engine3D::Vulkan {

    /**
     * Interface that can be used to implement custom uniform buffer layouts for pipelines.
     */
    class UniformBufferLayoutInterface {

    public:
        virtual ~UniformBufferLayoutInterface() = default;
        virtual VkDescriptorSet getDescriptorSetHandle() const = 0;

    };

    /**
     * Implementation for ColorPipeline.
     */
    class ColorUniformBufferLayout
        : public UniformBufferLayoutInterface
    {
        VkDescriptorSet vkDescriptorSet;
        Buffer* matrixBuffer;

    public:
        ColorUniformBufferLayout(
            Vulkan::Device *device,
            Vulkan::DescriptorPool *descriptorPool,
            Vulkan::GraphicsPipeline *graphicsPipeline,
            VkSharingMode sharingMode = VK_SHARING_MODE_EXCLUSIVE
        );
        ~ColorUniformBufferLayout();

        Buffer* getMatrixBuffer() const;
        VkDescriptorSet getDescriptorSetHandle() const override;
    };

    /**
     * Implementation for texture pipeline
     */
    class TextureUniformBufferLayout
        : public UniformBufferLayoutInterface
    {
        VkDescriptorSet vkDescriptorSet;
        Buffer* matrixBuffer;

    public:
        TextureUniformBufferLayout(
            Vulkan::Device *device,
            Vulkan::DescriptorPool *descriptorPool,
            Vulkan::GraphicsPipeline *graphicsPipeline,
            Image* texture,
            VkSharingMode sharingMode = VK_SHARING_MODE_EXCLUSIVE
        );
        ~TextureUniformBufferLayout();

        Buffer* getMatrixBuffer() const;
        VkDescriptorSet getDescriptorSetHandle() const override;
    };

    /**
     * Implementation for text pipeline
     */
    class TextUniformBufferLayout
        : public UniformBufferLayoutInterface
    {
        VkDescriptorSet vkDescriptorSet;
        Buffer* matrixBuffer;
        Buffer* textColorBuffer;

    public:
        TextUniformBufferLayout(
            Vulkan::Device *device,
            Vulkan::DescriptorPool *descriptorPool,
            Vulkan::GraphicsPipeline *graphicsPipeline,
            Vulkan::Image* texture,
            VkSharingMode sharingMode = VK_SHARING_MODE_EXCLUSIVE
        );
        ~TextUniformBufferLayout();

        Buffer* getMatrixBuffer() const;
        Buffer* getTextColorBuffer() const;
        VkDescriptorSet getDescriptorSetHandle() const override;
    };
}


#endif //INC_3DENGINE_VULKAN_UNIFORM_BUFFER_H
