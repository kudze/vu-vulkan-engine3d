//
// Created by karol on 4/5/2020.
//

#include "vulkan_fence.h"

using namespace Engine3D;
using namespace Vulkan;

void Fence::setDevice(Engine3D::Vulkan::Device *device) {
    this->device = device;
}

void Fence::setHandle(VkFence handle) {
    this->handle = handle;
}

Fence::Fence(Vulkan::Device *device, bool enabledAfterCreation)
    : device(device)
{
    VkFenceCreateInfo vkFenceCreateInfo = {};
    vkFenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

    if(enabledAfterCreation)
        vkFenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    if(vkCreateFence(device->getHandle(), &vkFenceCreateInfo, nullptr, &this->handle) != VK_SUCCESS)
        throw std::runtime_error("Failed to create Vulkan fence!");
}

Fence::~Fence() {
    vkDestroyFence(this->getDevice()->getHandle(), this->getHandle(), nullptr);
}

void Fence::wait() {
    vkWaitForFences(this->getDevice()->getHandle(), 1, &this->handle, VK_TRUE, UINT64_MAX);
}

void Fence::reset() {
    vkResetFences(this->getDevice()->getHandle(), 1, &this->handle);
}

VkFence Fence::getHandle() const {
    return this->handle;
}

Vulkan::Device * Fence::getDevice() const {
    return this->device;
}
