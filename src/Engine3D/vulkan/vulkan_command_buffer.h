//
// Created by kkraujelis on 2020-03-16.
//

#ifndef INC_3DENGINE_VULKAN_COMMAND_BUFFER_H
#define INC_3DENGINE_VULKAN_COMMAND_BUFFER_H

#include "vulkan_command_pool.h"
#include "vulkan_semaphore.h"
#include "vulkan_fence.h"

namespace Engine3D::Vulkan {

    /**
     * Class that stores command buffer
     *
     * Command buffer stores instructions to be executed by GPU.
     */
    class CommandBuffer : public NonCopyable {
    private:
        /**
         * Pointer to a command pool which buffer belongs to.
         */
        Vulkan::CommandPool* pool;

        /**
         * VkCommandBuffer handle
         */
        VkCommandBuffer handle;

        /**
         * pool setter.
         *
         * @param pool
         */
        void setPool(Vulkan::CommandPool* pool);

        /**
         * Handle setter.
         *
         * @param buffer
         */
        void setHandle(VkCommandBuffer buffer);

    public:
        /**
         * Constructor
         *
         * @param pool
         * @param level - describes priority for command buffer
         */
        CommandBuffer(
            Vulkan::CommandPool *pool,
            VkCommandBufferLevel level = VK_COMMAND_BUFFER_LEVEL_PRIMARY
        );

        /**
         * Destructor
         */
        virtual ~CommandBuffer();

        /**
         * Begins recording of instructions to execute.
         *
         * @param flags - buffer usage flags.
         */
        void beginRecording(VkCommandBufferUsageFlags flags) const;

        /**
         * Stops recording of instructions.
         */
        void endRecording() const;

        /**
         * Submits instructions to be executed on GPU
         *
         * @param fence - fence to sync GPU to CPU.
         * @param waitSemaphores - semaphores to be waited before buffer is executed.
         * @param signalSemaphores - semaphore to be signaled after buffer is executed.
         * @param waitStages - pipeline stages to be waited before buffer is executed.
         */
        void submit(
            Vulkan::Fence* fence = nullptr,
            std::vector<Vulkan::Semaphore*> const& waitSemaphores = {},
            std::vector<Vulkan::Semaphore*> const& signalSemaphores = {},
            VkPipelineStageFlags const* waitStages = nullptr
        ) const;

        /**
         * Command bufffer pool getter.
         *
         * @return
         */
        Vulkan::CommandPool* getPool() const;

        /**
         * Buffer handle getter.
         *
         * @return
         */
        VkCommandBuffer getHandle() const;
    };

}

#endif //INC_3DENGINE_VULKAN_COMMAND_BUFFER_H
