//
// Created by karol on 5/7/2020.
//

#include "vulkan_image.h"
#include "vulkan_staging_buffer.h"

using namespace Engine3D;
using namespace Vulkan;

bool Image::hasStencilComponent(VkFormat format) {
    return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}

VkFormat Image::findDepthFormat(PhysicalDevice *device) {
    return Image::findSupportedFormat(
        device,
        {
            VK_FORMAT_D32_SFLOAT,
            VK_FORMAT_D32_SFLOAT_S8_UINT,
            VK_FORMAT_D24_UNORM_S8_UINT,
        },
        VK_IMAGE_TILING_OPTIMAL,
        VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
    );
}

VkFormat
Image::findSupportedFormat(PhysicalDevice *device, const std::vector<VkFormat> &candidates, VkImageTiling tiling,
                           VkFormatFeatureFlags features) {

    for (auto format : candidates) {
        auto const &properties = device->getDeviceFormatProperties(format);

        if (tiling == VK_IMAGE_TILING_LINEAR && (properties.linearTilingFeatures & features))
            return format;

        if (tiling == VK_IMAGE_TILING_OPTIMAL && (properties.optimalTilingFeatures & features))
            return format;
    }

    throw std::runtime_error("Failed to find supported format!");

}

VkImageView Image::createImageView(Device *device, VkImage image, VkFormat format, VkImageAspectFlags aspectFlags) {
    VkImageViewCreateInfo imageViewCreateInfo = {};
    imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    imageViewCreateInfo.image = image;
    imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    imageViewCreateInfo.format = format;
    imageViewCreateInfo.subresourceRange.aspectMask = aspectFlags;
    imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
    imageViewCreateInfo.subresourceRange.levelCount = 1;
    imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
    imageViewCreateInfo.subresourceRange.layerCount = 1;
    imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

    VkImageView result;

    if (vkCreateImageView(
        device->getHandle(),
        &imageViewCreateInfo,
        nullptr,
        &result
    ) != VK_SUCCESS)
        throw std::runtime_error("Failed to create image view!");

    return result;
}

Image::Image(
    Engine3D::Vulkan::Device *device,
    Engine3D::Vulkan::CommandPool *transferPool,
    Engine3D::Vulkan::CommandPool *graphicsPool,
    uint32_t width,
    uint32_t height,
    void *data,
    VkFormat format,
    VkImageTiling tiling,
    VkImageUsageFlags usageFlags,
    VkMemoryPropertyFlags memoryPropertyFlags,
    VkImageAspectFlags aspectFlags,
    VkImageLayout layout
)
    : device(device), width(width), height(height), format(format) {

    //Image creation.
    VkImageCreateInfo imageCreateInfo = {};
    imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
    imageCreateInfo.extent.width = width;
    imageCreateInfo.extent.height = height;
    imageCreateInfo.extent.depth = 1;
    imageCreateInfo.mipLevels = 1;
    imageCreateInfo.arrayLayers = 1;
    imageCreateInfo.format = format;
    imageCreateInfo.tiling = tiling;
    imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageCreateInfo.usage = usageFlags;
    imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    imageCreateInfo.flags = 0;

    if (vkCreateImage(
        this->getDevice()->getHandle(),
        &imageCreateInfo,
        nullptr,
        &this->handle
    ) != VK_SUCCESS)
        throw std::runtime_error("Failed to create an image!");

    //Memory allocation.
    VkMemoryRequirements memoryRequirements;
    vkGetImageMemoryRequirements(device->getHandle(), this->getHandle(), &memoryRequirements);

    VkMemoryAllocateInfo memoryAllocateInfo = {};
    memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memoryAllocateInfo.allocationSize = memoryRequirements.size;
    memoryAllocateInfo.memoryTypeIndex = this->getDevice()->getPhysicalDevice()->findMemoryTypeIndex(
        memoryRequirements.memoryTypeBits,
        memoryPropertyFlags
    );

    if (vkAllocateMemory(
        this->getDevice()->getHandle(),
        &memoryAllocateInfo,
        nullptr,
        &this->memoryHandle
    ) != VK_SUCCESS)
        throw std::runtime_error("Failed to allocate image memory!");

    vkBindImageMemory(this->getDevice()->getHandle(), this->getHandle(), this->getMemory(), 0);

    if(data != nullptr) {
        this->transitionLayout(
            graphicsPool,
            VK_IMAGE_LAYOUT_UNDEFINED,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
        );
        this->uploadData(
            transferPool,
            data
        );
        this->transitionLayout(
            graphicsPool,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            layout
        );
    } else {
        this->transitionLayout(
            graphicsPool,
            VK_IMAGE_LAYOUT_UNDEFINED,
            layout
        );
    }

    this->view = createImageView(this->getDevice(), this->getHandle(), format, aspectFlags);
}

Image::~Image() {
    delete sampler;

    vkDestroyImageView(this->getDevice()->getHandle(), this->getView(), nullptr);
    vkDestroyImage(this->getDevice()->getHandle(), this->getHandle(), nullptr);
    vkFreeMemory(this->getDevice()->getHandle(), this->getMemory(), nullptr);
}

void Image::transitionLayout(CommandPool *graphicsPool, VkImageLayout oldLayout, VkImageLayout newLayout) {
    VkImageMemoryBarrier imageMemoryBarrier = {};
    imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    imageMemoryBarrier.oldLayout = oldLayout;
    imageMemoryBarrier.newLayout = newLayout;
    imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    imageMemoryBarrier.image = this->getHandle();
    imageMemoryBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    imageMemoryBarrier.subresourceRange.baseMipLevel = 0;
    imageMemoryBarrier.subresourceRange.levelCount = 1;
    imageMemoryBarrier.subresourceRange.baseArrayLayer = 0;
    imageMemoryBarrier.subresourceRange.layerCount = 1;
    imageMemoryBarrier.srcAccessMask = 0;
    imageMemoryBarrier.dstAccessMask = 0;

    auto commandBuffer = new CommandBuffer(
        graphicsPool,
        VK_COMMAND_BUFFER_LEVEL_PRIMARY
    );

    commandBuffer->beginRecording(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

    VkPipelineStageFlags sourceStage;
    VkPipelineStageFlags destinationStage;

    if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
        imageMemoryBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;

        if (hasStencilComponent(this->getFormat()))
            imageMemoryBarrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
    } else {
        imageMemoryBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    }

    if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
        imageMemoryBarrier.srcAccessMask = 0;
        imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

        sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    } else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL &&
               newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
        imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    } else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
        imageMemoryBarrier.srcAccessMask = 0;
        imageMemoryBarrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

        sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
    } else {
        throw std::invalid_argument("unsupported layout transition!");
    }


    vkCmdPipelineBarrier(
        commandBuffer->getHandle(),
        sourceStage, destinationStage,
        0,
        0, nullptr,
        0, nullptr,
        1, &imageMemoryBarrier
    );

    commandBuffer->endRecording();
    commandBuffer->submit();
    vkQueueWaitIdle(this->getDevice()->getGraphicsQueue());

    delete commandBuffer;
}

void Image::attachImageSampler(ImageSampler *sampler) {
    this->sampler = sampler;
}

bool Image::hasSampler() const {
    return this->sampler != nullptr;
}

ImageSampler *Image::getSampler() const {
    return this->sampler;
}

void Image::uploadData(Engine3D::Vulkan::CommandPool *transferPool, void *data) {
    auto stagingBuffer = new StagingBuffer(
        device,
        width * height * 4,
        VK_SHARING_MODE_EXCLUSIVE
    );

    stagingBuffer->mapMemory((void *) data);

    auto commandBuffer = new CommandBuffer(
        transferPool,
        VK_COMMAND_BUFFER_LEVEL_PRIMARY
    );

    commandBuffer->beginRecording(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

    VkBufferImageCopy bufferImageCopy;
    bufferImageCopy.bufferOffset = 0;
    bufferImageCopy.bufferRowLength = 0;
    bufferImageCopy.bufferImageHeight = 0;
    bufferImageCopy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    bufferImageCopy.imageSubresource.mipLevel = 0;
    bufferImageCopy.imageSubresource.baseArrayLayer = 0;
    bufferImageCopy.imageSubresource.layerCount = 1;
    bufferImageCopy.imageOffset = {0, 0, 0};
    bufferImageCopy.imageExtent = {width, height, 1};

    vkCmdCopyBufferToImage(
        commandBuffer->getHandle(),
        stagingBuffer->getHandle(),
        this->getHandle(),
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1,
        &bufferImageCopy
    );

    commandBuffer->endRecording();
    commandBuffer->submit();
    vkQueueWaitIdle(this->getDevice()->getTransferQueue());

    delete commandBuffer;
    delete stagingBuffer;
}

Device *Image::getDevice() const {
    return this->device;
}

VkImage Image::getHandle() const {
    return this->handle;
}

VkDeviceMemory Image::getMemory() const {
    return this->memoryHandle;
}

VkImageView Image::getView() const {
    return this->view;
}

uint32_t Image::getHeight() const {
    return this->height;
}

uint32_t Image::getWidth() const {
    return this->width;
}

VkFormat Image::getFormat() const {
    return this->format;
}