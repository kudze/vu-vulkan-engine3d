//
// Created by kkraujelis on 2020-03-16.
//

#ifndef INC_3DENGINE_VULKAN_COMMAND_POOL_H
#define INC_3DENGINE_VULKAN_COMMAND_POOL_H

#include <functional>

#include "vulkan_device.h"

namespace Engine3D::Vulkan {

    /**
     * Wrapper around VkCommandPool.
     * Command pool should be created once per thread.
     * Command pool stores command buffers on the GPU.
     */
    class CommandPool : public NonCopyable {
    private:

        Vulkan::Device *device = nullptr;
        VkCommandPool handle = VK_NULL_HANDLE;
        VkQueue queue = VK_NULL_HANDLE;

        void setHandle(VkCommandPool handle);
        void setDevice(Device *device);
        void setQueue(VkQueue queue);

    public:
        CommandPool(
            Engine3D::Vulkan::Device *device,
            VkQueue queueFamily,
            uint32_t queueFamilyIndex
        );

        ~CommandPool();

        Vulkan::Device *getDevice() const;
        VkCommandPool getHandle() const;
        VkQueue getQueue() const;
    };

}

#endif //INC_3DENGINE_VULKAN_COMMAND_POOL_H
