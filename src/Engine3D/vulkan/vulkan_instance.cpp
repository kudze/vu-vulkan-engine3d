//
// Created by kkraujelis on 2020-02-29.
//

#include "vulkan_instance.h"
#include "vulkan_debug.h"
#include <cstring>

using namespace Engine3D;

std::vector<const char*> convertCPPSTRToCSTR(
    std::vector<std::string>
    const& data
)
{
    std::vector<const char*> result;

    for(std::string const& _str : data) {
        char* str = new char[128];

#ifdef __MINGW32__
        strcpy(str, _str.c_str());
#elif defined(_WIN32)
        strcpy_s(str, 128, _str.c_str());
#else
        strcpy(str, _str.c_str());
#endif

        result.push_back(
            str
        );
    }

    return result;
}

std::vector<VkExtensionProperties> Vulkan::Instance::getAvailableInstanceExtensionProperties()
{
    uint32_t vkExtensionsCount = 0;
    vkEnumerateInstanceExtensionProperties(nullptr, &vkExtensionsCount, nullptr);

    std::vector<VkExtensionProperties> vkExtensions(vkExtensionsCount);
    vkEnumerateInstanceExtensionProperties(nullptr, &vkExtensionsCount, vkExtensions.data());

    return vkExtensions;
}

bool Vulkan::Instance::isInstanceExtensionValid(const std::string& extensionName)
{
    static std::vector<VkExtensionProperties> properties = Vulkan::Instance::getAvailableInstanceExtensionProperties();

    for(auto const& extension : properties)
        if(std::string(extension.extensionName) == extensionName)
            return true;

    return false;
}

std::vector<VkLayerProperties> Vulkan::Instance::getAvailableInstanceLayerProperties()
{
    uint32_t vkLayersCount = 0;
    vkEnumerateInstanceLayerProperties(&vkLayersCount, nullptr);

    std::vector<VkLayerProperties> vkLayers(vkLayersCount);
    vkEnumerateInstanceLayerProperties(&vkLayersCount, vkLayers.data());

    return vkLayers;
}

bool Vulkan::Instance::isInstanceLayerValid(const std::string& layerName)
{
    static std::vector<VkLayerProperties> properties = Vulkan::Instance::getAvailableInstanceLayerProperties();

    for(auto const& layer : properties)
        if(std::string(layer.layerName) == layerName)
            return true;

    return false;
}

void Vulkan::Instance::setAppTitle(std::string const &title) {
    this->appTitle = title;
}

void Vulkan::Instance::setEngineTitle(std::string const &title) {
    this->engineTitle = title;
}

Vulkan::Instance::Instance(
    std::string const& appName, uint32_t appVersion,
    std::string const& engineName, uint32_t engineVersion,
    std::vector<std::string> const& vkExtensions,
    std::vector<std::string> const& vkLayers
)
    : appTitle(appName), engineTitle(engineName)
{
    //Lets check if extensions and layers are valid.
    for(std::string const& extension : vkExtensions)
        if(!Vulkan::Instance::isInstanceExtensionValid(extension))
            throw std::runtime_error(
                "Invalid vulkan instance extension was pushed during creation"
            );

    for(std::string const& layer : vkLayers)
        if(!Vulkan::Instance::isInstanceLayerValid(layer))
            throw std::runtime_error(
                "Invalid vulkan instance layer was pushed during creation"
            );

    VkApplicationInfo vkApplicationInfo = {};
    vkApplicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    vkApplicationInfo.pApplicationName = appName.c_str();
    vkApplicationInfo.applicationVersion = appVersion;
    vkApplicationInfo.pEngineName = engineName.c_str();
    vkApplicationInfo.engineVersion = engineVersion;
    vkApplicationInfo.apiVersion = VK_API_VERSION_1_0;

    std::vector<const char*> extensions = convertCPPSTRToCSTR(vkExtensions);
    std::vector<const char*> layers = convertCPPSTRToCSTR(vkLayers);

    VkInstanceCreateInfo vkInstanceCreateInfo = {};
    vkInstanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    vkInstanceCreateInfo.pApplicationInfo = &vkApplicationInfo;
    vkInstanceCreateInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
    vkInstanceCreateInfo.ppEnabledExtensionNames = extensions.data();
    vkInstanceCreateInfo.enabledLayerCount = static_cast<uint32_t>(layers.size());
    vkInstanceCreateInfo.ppEnabledLayerNames = layers.data();
#ifdef BUILD_DEBUG
    VkDebugUtilsMessengerCreateInfoEXT createInfoExt = Vulkan::ValidationLayers::createValidationLayerCreateInfo();
    vkInstanceCreateInfo.pNext = &createInfoExt;
#endif

    if(vkCreateInstance(&vkInstanceCreateInfo, nullptr, &this->handle) != VK_SUCCESS)
        throw std::runtime_error("Failed to create Vulkan Instance!");

    for(auto extension : extensions)
        delete extension;

    for(auto layer : layers)
        delete layer;
}

Vulkan::Instance::~Instance()
{
    vkDestroyInstance(this->handle, nullptr);
}

VkInstance Vulkan::Instance::getHandle() const
{
    return this->handle;
}

std::string const& Vulkan::Instance::getAppTitle() const
{  
    return this->appTitle;
}

std::string const& Vulkan::Instance::getEngineTitle() const
{
    return this->engineTitle;
}

void Vulkan::Instance::setHandle(VkInstance handle) {
    Instance::handle = handle;
}
