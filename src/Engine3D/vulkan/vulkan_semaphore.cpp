//
// Created by kkraujelis on 2020-03-16.
//

#include "vulkan_semaphore.h"

using namespace Engine3D;
using namespace Vulkan;

Semaphore::Semaphore(Engine3D::Vulkan::Device* device)
    : device(device)
{
    VkSemaphoreCreateInfo semaphoreInfo = {};
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    if(vkCreateSemaphore(
        this->getDevice()->getHandle(),
        &semaphoreInfo,
        nullptr,
        &this->handle
    ) != VK_SUCCESS)
        throw std::runtime_error("Failed to create vulkan semaphore");
}

Semaphore::~Semaphore()
{
    vkDestroySemaphore(
        this->getDevice()->getHandle(),
        this->getHandle(),
        nullptr
    );
}

VkSemaphore Semaphore::getHandle() const
{
    return this->handle;
}

Vulkan::Device* Semaphore::getDevice() const
{
    return this->device;
}

void Semaphore::setHandle(VkSemaphore handle) {
    Semaphore::handle = handle;
}

void Semaphore::setDevice(Device *device) {
    Semaphore::device = device;
}
