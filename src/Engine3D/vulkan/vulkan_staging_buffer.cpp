//
// Created by karol on 5/7/2020.
//

#include "vulkan_staging_buffer.h"

using namespace Engine3D;
using namespace Vulkan;

StagingBuffer::StagingBuffer(Device* device, VkDeviceSize size, VkSharingMode sharingMode)
    : Buffer(
            device,
            size,
            VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            sharingMode,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
        )
{

}

StagingBuffer::~StagingBuffer() = default;