//
// Created by karol on 4/5/2020.
//

#include "vulkan_index_buffer.h"
#include "vulkan_command_buffer.h"
#include "vulkan_staging_buffer.h"

using namespace Engine3D;
using namespace Vulkan;

//TODO: utilize offset. (it states its way faster that way.)
IndexBuffer::IndexBuffer(
    Vulkan::Device *device,
    Vulkan::CommandPool *transferCommandPool,
    index_t const *data,
    uint32_t indexCount,
    VkSharingMode sharingMode
) : Buffer(
    device,
    indexCount * sizeof(index_t),
    VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
    sharingMode,
    VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
), indexCount(indexCount) {
    VkDeviceSize rsize = indexCount * sizeof(index_t);

    //Setup staging buffers.
    auto stagingBuffer = new StagingBuffer(
        device,
        rsize,
        sharingMode
    );

    stagingBuffer->mapMemory((void *) data);

    //Record copy procedure.
    Vulkan::CommandBuffer commandBuffer(transferCommandPool);
    commandBuffer.beginRecording(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

    VkBufferCopy copyRegion = {};
    copyRegion.srcOffset = 0; // Optional
    copyRegion.dstOffset = 0; // Optional
    copyRegion.size = rsize;
    vkCmdCopyBuffer(
        commandBuffer.getHandle(),
        stagingBuffer->getHandle(),
        this->getHandle(),
        1, &copyRegion
    );

    commandBuffer.endRecording();

    //Submit command buffer.
    commandBuffer.submit();

    vkQueueWaitIdle(this->getDevice()->getTransferQueue());

    delete stagingBuffer;
}

IndexBuffer::~IndexBuffer() = default;

uint32_t IndexBuffer::getIndexCount() const {
    return this->indexCount;
}