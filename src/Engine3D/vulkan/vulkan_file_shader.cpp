//
// Created by kkraujelis on 2020-03-16.
//

#include "vulkan_file_shader.h"
#include <fstream>

using namespace Engine3D;
using namespace Vulkan;

std::vector<char> readShaderFile(std::string const& filename)
{
    std::ifstream file(filename, std::ios::ate | std::ios::binary);

    if (!file.is_open())
        throw std::runtime_error("Failed to open shader file!");

    size_t size = file.tellg();
    std::vector<char> result(size);

    file.seekg(0);
    file.read(result.data(), size);
    file.close();

    return result;
}

FileVertexShader::FileVertexShader(Vulkan::Device* device, std::string const& fileName)
    : VertexShader(device, readShaderFile(fileName))
{

}

FileVertexShader::~FileVertexShader() = default;

FileFragmentShader::FileFragmentShader(Vulkan::Device* device, std::string const& fileName)
    : FragmentShader(device, readShaderFile(fileName))
{

}

FileFragmentShader::~FileFragmentShader() = default;