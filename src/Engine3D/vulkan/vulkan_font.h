//
// Created by karol on 5/19/2020.
//

#ifndef INC_3DENGINE_VULKAN_FONT_H
#define INC_3DENGINE_VULKAN_FONT_H

#include <ft2build.h>
#include FT_FREETYPE_H

#include <glm.hpp>

#define DEFAULT_CHARSET_FONT "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM0123456789[]{}\\;:\"',./<>?+-*!@#$%^&*()_="

#include "vulkan_image.h"
#include "vulkan_renderer.h"

namespace Engine3D::Vulkan {

    class Character {
        Vulkan::Image* image; //texture
        glm::ivec2 size; //size of glyph
        glm::ivec2 bearing; //offset from baseline to left/top of glyph
        uint32_t advance; //offset to advance next glyph to

    public:
        Character(
            Vulkan::Image* image,
            glm::ivec2 const& size,
            glm::ivec2 const& bearing,
            uint32_t advance
        );
        ~Character();

        Vulkan::Image* getImage() const;
        glm::ivec2 const& getSize() const;
        glm::ivec2 const& getBearing() const;
        uint32_t getAdvance() const;
    };

    class Font {
        static FT_Library library;
        static size_t instancesCount;
        static void initializeLibrary();
        static void deinitializeLibrary();
        std::map<char, Character*> charset;

    public:
        Font(Renderer* renderer, std::string const& filename, uint8_t fontSize, std::string const& charactersToLoad = DEFAULT_CHARSET_FONT);
        ~Font();

        Character* getCharacterInfo(char letter) const;
    };

}


#endif //INC_3DENGINE_VULKAN_FONT_H
