//
// Created by karol on 5/7/2020.
//

#ifndef INC_3DENGINE_VULKAN_IMAGE_SAMPLER_H
#define INC_3DENGINE_VULKAN_IMAGE_SAMPLER_H

#include "vulkan_image.h"

namespace Engine3D::Vulkan
{
    class ImageSampler : public NonCopyable
    {
        Image* image = nullptr;
        VkSampler handle;

    public:
        ImageSampler(Image* image, VkFilter filter, float anisotropicFilter, VkSamplerAddressMode samplerAddressMode = VK_SAMPLER_ADDRESS_MODE_REPEAT);
        ~ImageSampler();

        Image* getImage() const;
        VkSampler getHandle() const;
    };

}

#endif //INC_3DENGINE_VULKAN_IMAGE_SAMPLER_H
