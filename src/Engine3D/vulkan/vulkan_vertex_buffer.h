//
// Created by karol on 4/5/2020.
//

#ifndef INC_3DENGINE_VULKAN_VERTEX_BUFFER_H
#define INC_3DENGINE_VULKAN_VERTEX_BUFFER_H

#include "vulkan_buffer.h"
#include "vulkan_command_pool.h"
#include "vulkan_vertex.h"

namespace Engine3D::Vulkan {
    class VertexBufferInterface
        : public Buffer {

    public:

        VertexBufferInterface(
            Vulkan::Device *device,
            VkDeviceSize size,
            VkBufferUsageFlags usage,
            VkSharingMode sharingMode,
            VkMemoryPropertyFlags memoryPropertyFlags
        ) : Buffer(
            device,
            size,
            usage,
            sharingMode,
            memoryPropertyFlags
        ) {}

        virtual ~VertexBufferInterface() = default;

        virtual uint32_t getVertexCount() const = 0;
    };

    /**
    * class that stores vertex buffers.
    *
    * Vertex buffers are used to render models.
    * Dynamic buffer is visible by cpu so mapMemory can be used!
    */
    class VertexDynamicBuffer
        : public VertexBufferInterface {
        uint32_t vertexCount;

    public:
        VertexDynamicBuffer(
            Vulkan::Device *device,
            Vertex const *data,
            uint32_t vertexCount,
            VkSharingMode sharingMode = VK_SHARING_MODE_EXCLUSIVE
        );
        virtual ~VertexDynamicBuffer();

        uint32_t getVertexCount() const override;
    };

    /**
     * class that stores vertex buffers.
     *
     * Vertex buffers are used to render models.
     */
    class VertexBuffer
        : public VertexBufferInterface {
        uint32_t vertexCount;

    public:
        VertexBuffer(
            Vulkan::Device *device,
            Vulkan::CommandPool *transferCommandPool,
            Vertex const *data,
            uint32_t vertexCount,
            VkSharingMode sharingMode = VK_SHARING_MODE_EXCLUSIVE
        );
        virtual ~VertexBuffer();

        /**
         * Maps memory to buffer.
         *
         * @param data
         */
        void mapMemory(void *data) = delete;

        uint32_t getVertexCount() const override;
    };
}


#endif //INC_3DENGINE_VULKAN_VERTEX_BUFFER_H
