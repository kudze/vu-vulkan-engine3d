//
// Created by kkraujelis on 2020-02-29.
//

#ifndef INC_3DENGINE_RENDERINGENGINE_H
#define INC_3DENGINE_RENDERINGENGINE_H

#define MAX_OBJECTS_PER_SCENE 500

#include <functional>
#include <unordered_map>

#include "vulkan_window.h"
#include "vulkan_instance.h"
#include "vulkan_debug.h"
#include "vulkan_device.h"
#include "vulkan_surface.h"
#include "vulkan_swapchain.h"
#include "vulkan_renderpass.h"
#include "vulkan_graphics_pipeline.h"
#include "vulkan_semaphore.h"
#include "vulkan_command_pool.h"
#include "vulkan_command_buffer.h"
#include "vulkan_descriptor_pool.h"
#include "vulkan_fence.h"
#include "vulkan_image.h"

#include "../engine.h"
#include "../engine_component.h"

namespace Engine3D
{
    class Engine;
    class RenderingEngine;

    namespace Vulkan {
        /**
         * RenderingEngine is a class that ties all this project together.
         */
        class Renderer
            : public EngineComponent
        {
        private:
            Instance *vkInstance = nullptr;
            ValidationLayers *vkValidation = nullptr;
            Surface *vkSurface = nullptr;
            Device *vkDevice = nullptr;
            Swapchain *vkSwapchain = nullptr;
            RenderPass *vkRenderPass = nullptr;
            Vulkan::CommandPool *vkGraphicsCommandPool = nullptr;
            Vulkan::CommandPool *vkTransferCommandPool = nullptr;
            Vulkan::DescriptorPool *vkDescriptorPool = nullptr;
            Vulkan::Image* depthBuffer = nullptr;
            std::vector<Vulkan::Semaphore *> imageAvailableSemaphores;
            std::vector<Vulkan::Semaphore *> frameAvailableSemaphores;
            std::vector<Vulkan::Fence *> frameRendereringFences;
            std::vector<Vulkan::Fence *> swapchainFrameRenderingFence;
            std::vector<GraphicsPipelineDescription*> pipelineDescriptions;
            std::unordered_map<graphicsPipelineIndex_t, GraphicsPipeline*> vkPipelines;
            uint8_t maxFramesInFlight = 2;
            uint8_t currentFrame = 0;

            Vulkan::CommandBuffer *recordCommandBuffer(uint32_t imageIndex);

            void createSwapchain();
            void freeSwapchain();

        public:
            explicit Renderer(Engine* engine);
            ~Renderer();

            void registerPipeline(GraphicsPipelineDescription* description);
            void bootUpPipelines();

            void render();

            void recreateSwapchain();

            Vulkan::Instance *getInstance() const;
            Vulkan::ValidationLayers *getValidationLayers() const;
            Vulkan::Surface *getSurface() const;
            Vulkan::Device *getDevice() const;
            Vulkan::RenderPass *getRenderPass() const;
            Vulkan::Swapchain *getSwapchain() const;
            Vulkan::CommandPool *getGraphicsCommandPool() const;
            Vulkan::CommandPool *getTransferCommandPool() const;
            Vulkan::DescriptorPool *getDescriptorPool() const;
            Vulkan::Image* getDepthBuffer() const;
            std::vector<Vulkan::Semaphore *> const &getImageAvailableSemaphores() const;
            std::vector<Vulkan::Semaphore *> const &getFrameAvailableSemaphores() const;
            std::vector<Vulkan::Fence *> const &getFrameRenderingFences() const;
            std::unordered_map<graphicsPipelineIndex_t, Vulkan::GraphicsPipeline*> const& getGraphicsPipelines() const;
            std::vector<Vulkan::GraphicsPipelineDescription*> const& getGraphicsPipelinesDescriptions() const;
            uint8_t getMaxFramesInFlight() const;
            uint8_t getCurrentFrameIndex() const;
            Vulkan::Window* getWindow() const;
        };
    };
}


#endif //INC_3DENGINE_RENDERINGENGINE_H
