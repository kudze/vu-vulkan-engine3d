//
// Created by karol on 5/7/2020.
//

#include "vulkan_image_sampler.h"

using namespace Engine3D;
using namespace Vulkan;

ImageSampler::ImageSampler(Image *image, VkFilter filter, float anisotropicFilter, VkSamplerAddressMode samplerAddressMode)
    : image(image) {
    VkSamplerCreateInfo samplerCreateInfo = {};
    samplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerCreateInfo.magFilter = filter;
    samplerCreateInfo.minFilter = filter;
    samplerCreateInfo.addressModeU = samplerAddressMode;
    samplerCreateInfo.addressModeV = samplerAddressMode;
    samplerCreateInfo.addressModeW = samplerAddressMode;
    samplerCreateInfo.anisotropyEnable = anisotropicFilter == 1.f ? VK_FALSE : VK_TRUE;
    samplerCreateInfo.maxAnisotropy = anisotropicFilter;
    samplerCreateInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    samplerCreateInfo.unnormalizedCoordinates = VK_FALSE;
    samplerCreateInfo.compareEnable = VK_FALSE;
    samplerCreateInfo.compareOp = VK_COMPARE_OP_ALWAYS;
    samplerCreateInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerCreateInfo.mipLodBias = 0.f;
    samplerCreateInfo.minLod = 0.f;
    samplerCreateInfo.maxLod = 0.f;

    if (vkCreateSampler(
        this->getImage()->getDevice()->getHandle(),
        &samplerCreateInfo,
        nullptr,
        &this->handle
    ) != VK_SUCCESS)
        throw std::runtime_error("Failed to create image sampler!");
}

ImageSampler::~ImageSampler() {
    vkDestroySampler(this->getImage()->getDevice()->getHandle(), this->getHandle(), nullptr);
}

Image * ImageSampler::getImage() const {
    return this->image;
}

VkSampler ImageSampler::getHandle() const {
    return this->handle;
}