//
// Created by karol on 4/5/2020.
//

#ifndef INC_3DENGINE_VULKAN_BUFFER_H
#define INC_3DENGINE_VULKAN_BUFFER_H

#include "vulkan_device.h"

namespace Engine3D::Vulkan {

    /**
     * Vulkan buffer.
     * An object which stores memory on GPU.
     */
    class Buffer : public NonCopyable {
        /**
         * VkBuffer handle
         */
        VkBuffer handle = VK_NULL_HANDLE;

        /**
         * VkDeviceMemory handle.
         */
        VkDeviceMemory memoryHandle = VK_NULL_HANDLE;

        /**
         * Ponter to Vulkan::Device (logical GPU)
         */
        Vulkan::Device *device = nullptr;

        /**
         * Size of the buffer.
         */
        size_t size;

        /**
         * Handle setter.
         *
         * @param handle
         */
        void setHandle(VkBuffer handle);

        /**
         * Setter to memory handle
         *
         * @param memoryHandle
         */
        void setMemoryHandle(VkDeviceMemory memoryHandle);

        /**
         * Vulkan::Device setter
         *
         * @param device
         */
        void setDevice(Device *device);

    public:
        /**
         * Constructor
         *
         * @param device
         * @param size
         * @param usage
         * @param sharingMode
         * @param memoryPropertyFlags
         */
        Buffer(
            Vulkan::Device *device,
            VkDeviceSize size,
            VkBufferUsageFlags usage,
            VkSharingMode sharingMode,
            VkMemoryPropertyFlags memoryPropertyFlags
        );

        /**
         * Destructor
         */
        virtual ~Buffer();

        /**
         * Maps memory to buffer.
         *
         * @param data
         */
        void mapMemory(void *data);

        /**
         * Handle getter.
         *
         * @return
         */
        VkBuffer getHandle() const;

        /**
         * Memory handle getter.
         *
         * @return
         */
        VkDeviceMemory getMemoryHandle() const;

        /**
         * Vulkan device getter.
         * @return
         */
        Vulkan::Device *getDevice() const;

        /**
         * size getter
         *
         * @return
         */
        size_t getBufferSize() const;
    };
}


#endif //INC_3DENGINE_VULKAN_BUFFER_H
