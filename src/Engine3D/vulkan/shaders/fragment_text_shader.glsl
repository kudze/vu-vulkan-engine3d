#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

layout(binding = 1) uniform sampler2D texSampler;
layout(binding = 2) uniform TextColor {vec3 textColor;} tc;

void main() {
    vec4 sampled = vec4(1.0, 1.0, 1.0, texture(texSampler, fragTexCoord).r);
    outColor = vec4(tc.textColor, 1.0) * sampled;
}