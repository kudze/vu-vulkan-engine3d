//
// Created by karol on 5/12/2020.
//

#include "plane.h"

namespace Game {

    class PlaneImplementation {
    private:
        static Engine3D::Vulkan::Model* planeModel;
        static size_t planeCount;

        static void setModel(Engine3D::Vulkan::Model* model);
    public:
        explicit PlaneImplementation(Engine3D::Vulkan::Renderer* renderer);
        ~PlaneImplementation();

        Engine3D::Vulkan::Model* getModel() const;
    };

}

Engine3D::Vulkan::Model* Game::PlaneImplementation::planeModel = nullptr;
size_t Game::PlaneImplementation::planeCount = 0;

using namespace Game;

PlaneImplementation::PlaneImplementation(Engine3D::Vulkan::Renderer* renderer) {
    PlaneImplementation::planeCount++;

    if(PlaneImplementation::planeModel == nullptr) {
        const std::vector<Engine3D::Vulkan::Vertex> planeVertices = {
            {{-0.5f, -0.5f, 0.f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
            {{0.5f,  -0.5f, 0.f}, {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f}},
            {{0.5f,  0.5f, 0.f},  {0.0f, 0.0f, 1.0f}, {1.0f, 1.0f}},
            {{-0.5f, 0.5f, 0.f},  {1.0f, 1.0f, 1.0f}, {0.0f, 1.0f}}
        };

        const std::vector<Engine3D::Vulkan::index_t> planeIndices = {
            0, 1, 2,
            2, 3, 0
        };

        auto planeModel = new Engine3D::Vulkan::Model(
            new Engine3D::Vulkan::VertexBuffer(
                renderer->getDevice(),
                renderer->getTransferCommandPool(),
                planeVertices.data(),
                static_cast<uint32_t>(planeVertices.size())
            ),
            new Engine3D::Vulkan::IndexBuffer(
                renderer->getDevice(),
                renderer->getTransferCommandPool(),
                planeIndices.data(),
                static_cast<uint32_t>(planeIndices.size())
            )
        );

        PlaneImplementation::setModel(planeModel);
    }
}

PlaneImplementation::~PlaneImplementation() {
    PlaneImplementation::planeCount--;

    if(PlaneImplementation::planeCount == 0) {
        delete PlaneImplementation::planeModel;
        PlaneImplementation::setModel(nullptr);
    }
}

void PlaneImplementation::setModel(Engine3D::Vulkan::Model *model) {
    PlaneImplementation::planeModel = model;
}

Engine3D::Vulkan::Model * PlaneImplementation::getModel() const {
    return PlaneImplementation::planeModel;
}

Plane::Plane(Engine3D::Vulkan::Renderer* renderingEngine, Engine3D::Vulkan::graphicsPipelineIndex_t graphicsPipelineIndex, Engine3D::Vulkan::Image* texture)
    : impl(new PlaneImplementation(renderingEngine)), Object(nullptr, renderingEngine, graphicsPipelineIndex, texture)
{
    this->setModel(impl->getModel());
}

Plane::~Plane() {
    delete this->impl;
}

