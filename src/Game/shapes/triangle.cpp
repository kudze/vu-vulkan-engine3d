//
// Created by karol on 5/12/2020.
//

#include "triangle.h"

namespace Game {

    class TriangleImplementation {
    private:
        static Engine3D::Vulkan::Model* triangleModel;
        static size_t triangleCount;

        static void setModel(Engine3D::Vulkan::Model* model);
    public:
        explicit TriangleImplementation(Engine3D::Vulkan::Renderer* renderer);
        ~TriangleImplementation();

        Engine3D::Vulkan::Model* getModel() const;
    };

}

Engine3D::Vulkan::Model* Game::TriangleImplementation::triangleModel = nullptr;
size_t Game::TriangleImplementation::triangleCount = 0;

using namespace Game;

TriangleImplementation::TriangleImplementation(Engine3D::Vulkan::Renderer* renderer) {
    TriangleImplementation::triangleCount++;

    if(TriangleImplementation::triangleModel == nullptr) {
        const std::vector<Engine3D::Vulkan::Vertex> triangleVertices = {
            {{-0.5f, -0.5f, 0.f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
            {{0.5f,  0.f, 0.f}, {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f}},
            {{-0.5f,  0.5f, 0.f},  {0.0f, 0.0f, 1.0f}, {1.0f, 1.0f}},
        };

        const std::vector<Engine3D::Vulkan::index_t> triangleIndices = {
            0, 1, 2
        };

        auto triangleModel = new Engine3D::Vulkan::Model(
            new Engine3D::Vulkan::VertexBuffer(
                renderer->getDevice(),
                renderer->getTransferCommandPool(),
                triangleVertices.data(),
                static_cast<uint32_t>(triangleVertices.size())
            ),
            new Engine3D::Vulkan::IndexBuffer(
                renderer->getDevice(),
                renderer->getTransferCommandPool(),
                triangleIndices.data(),
                static_cast<uint32_t>(triangleIndices.size())
            )
        );

        TriangleImplementation::setModel(triangleModel);
    }
}

TriangleImplementation::~TriangleImplementation() {
    TriangleImplementation::triangleCount--;

    if(TriangleImplementation::triangleCount == 0) {
        delete TriangleImplementation::triangleModel;
        TriangleImplementation::setModel(nullptr);
    }
}

void TriangleImplementation::setModel(Engine3D::Vulkan::Model *model) {
    TriangleImplementation::triangleModel = model;
}

Engine3D::Vulkan::Model * TriangleImplementation::getModel() const {
    return TriangleImplementation::triangleModel;
}

Triangle::Triangle(Engine3D::Vulkan::Renderer* renderingEngine, Engine3D::Vulkan::graphicsPipelineIndex_t graphicsPipelineIndex, Engine3D::Vulkan::Image* texture)
    : impl(new TriangleImplementation(renderingEngine)), Object(nullptr, renderingEngine, graphicsPipelineIndex, texture)
{
    this->setModel(impl->getModel());
}

Triangle::~Triangle() {
    delete this->impl;
}

