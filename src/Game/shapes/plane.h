//
// Created by karol on 5/12/2020.
//

#ifndef INC_3DENGINE_PLANE_H
#define INC_3DENGINE_PLANE_H

#include "../../Engine3D/scene/object.h"

namespace Game {
    class PlaneImplementation;

    class Plane
        : public Engine3D::Object
    {
    private:
        PlaneImplementation* impl;

    public:
        Plane(Engine3D::Vulkan::Renderer* renderingEngine, Engine3D::Vulkan::graphicsPipelineIndex_t graphicsPipelineIndex, Engine3D::Vulkan::Image* texture = nullptr);
        ~Plane();

    };
}


#endif //INC_3DENGINE_PLANE_H
