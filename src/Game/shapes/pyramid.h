//
// Created by karol on 5/12/2020.
//

#ifndef INC_3DENGINE_PYRAMID_H
#define INC_3DENGINE_PYRAMID_H

#include "../../Engine3D/scene/object.h"

namespace Game {
    class PyramidImplementation;

    class Pyramid
        : public Engine3D::Object
    {
    private:
        PyramidImplementation* impl;

    public:
        Pyramid(Engine3D::Vulkan::Renderer* renderingEngine, Engine3D::Vulkan::graphicsPipelineIndex_t graphicsPipelineIndex, Engine3D::Vulkan::Image* texture = nullptr);
        ~Pyramid();

    };
}

#endif //INC_3DENGINE_PYRAMID_H
