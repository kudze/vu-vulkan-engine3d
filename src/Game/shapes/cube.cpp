//
// Created by karol on 5/12/2020.
//

#include "cube.h"

namespace Game {

    class CubeImplementation {
    private:
        static Engine3D::Vulkan::Model* cubeModel;
        static size_t cubeCount;

        static void setModel(Engine3D::Vulkan::Model* model);
    public:
        explicit CubeImplementation(Engine3D::Vulkan::Renderer* renderer);
        ~CubeImplementation();

        Engine3D::Vulkan::Model* getModel() const;
    };

}

Engine3D::Vulkan::Model* Game::CubeImplementation::cubeModel = nullptr;
size_t Game::CubeImplementation::cubeCount = 0;

using namespace Game;

CubeImplementation::CubeImplementation(Engine3D::Vulkan::Renderer* renderer) {
    CubeImplementation::cubeCount++;

    if(CubeImplementation::cubeModel == nullptr) {
        const std::vector<Engine3D::Vulkan::Vertex> cubeVertices = {
            {{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
            {{0.5f,  -0.5f, -0.5f}, {0.0f, 0.0f, 1.0f}, {1.0f, 0.0f}},
            {{0.5f,  0.5f, -0.5f},  {0.0f, 1.0f, 0.0f}, {1.0f, 1.0f}},
            {{-0.5f, 0.5f, -0.5f},  {1.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},

            {{-0.5f, -0.5f, 0.5f}, {1.0f, 0.5f, 0.0f}, {0.0f, 0.0f}},
            {{0.5f,  -0.5f, 0.5f}, {0.0f, 1.0f, 1.0f}, {1.0f, 0.0f}},
            {{0.5f,  0.5f, 0.5f},  {1.0f, 1.0f, 1.0f}, {1.0f, 1.0f}},
            {{-0.5f, 0.5f, 0.5f},  {1.0f, 0.0f, 1.0f}, {0.0f, 1.0f}}
        };

        const std::vector<Engine3D::Vulkan::index_t> cubeIndices = {
            3, 1, 0, 2, 1, 3,
            2, 5, 1, 6, 5, 2,
            6, 4, 5, 7, 4, 6,
            7, 0, 4, 3, 0, 7,
            7, 2, 3, 6, 2, 7,
            0, 5, 4, 1, 5, 0
        };

        auto cubeModel = new Engine3D::Vulkan::Model(
            new Engine3D::Vulkan::VertexBuffer(
                renderer->getDevice(),
                renderer->getTransferCommandPool(),
                cubeVertices.data(),
                static_cast<uint32_t>(cubeVertices.size())
            ),
            new Engine3D::Vulkan::IndexBuffer(
                renderer->getDevice(),
                renderer->getTransferCommandPool(),
                cubeIndices.data(),
                static_cast<uint32_t>(cubeIndices.size())
            )
        );

        CubeImplementation::setModel(cubeModel);
    }
}

CubeImplementation::~CubeImplementation() {
    CubeImplementation::cubeCount--;

    if(CubeImplementation::cubeCount == 0) {
        delete CubeImplementation::cubeModel;
        CubeImplementation::setModel(nullptr);
    }
}

void CubeImplementation::setModel(Engine3D::Vulkan::Model *model) {
    CubeImplementation::cubeModel = model;
}

Engine3D::Vulkan::Model * CubeImplementation::getModel() const {
    return CubeImplementation::cubeModel;
}

Cube::Cube(Engine3D::Vulkan::Renderer* renderingEngine, Engine3D::Vulkan::graphicsPipelineIndex_t graphicsPipelineIndex, Engine3D::Vulkan::Image* texture)
    : impl(new CubeImplementation(renderingEngine)), Object(nullptr, renderingEngine, graphicsPipelineIndex, texture)
{
    this->setModel(impl->getModel());
}

Cube::~Cube() {
    delete this->impl;
}

