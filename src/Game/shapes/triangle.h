//
// Created by karol on 5/12/2020.
//

#ifndef INC_3DENGINE_TRIANGLE_H
#define INC_3DENGINE_TRIANGLE_H

#include "../../Engine3D/scene/object.h"

namespace Game {
    class TriangleImplementation;

    class Triangle
        : public Engine3D::Object
    {
    private:
        TriangleImplementation* impl;

    public:
        Triangle(Engine3D::Vulkan::Renderer* renderingEngine, Engine3D::Vulkan::graphicsPipelineIndex_t graphicsPipelineIndex, Engine3D::Vulkan::Image* texture = nullptr);
        ~Triangle();

    };
}


#endif //INC_3DENGINE_TRIANGLE_H
