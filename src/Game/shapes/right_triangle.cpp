//
// Created by karol on 5/12/2020.
//

#include "right_triangle.h"

namespace Game {

    class RightTriangleImplementation {
    private:
        static Engine3D::Vulkan::Model* rightTriangleModel;
        static size_t rightTriangleCount;

        static void setModel(Engine3D::Vulkan::Model* model);
    public:
        explicit RightTriangleImplementation(Engine3D::Vulkan::Renderer* renderer);
        ~RightTriangleImplementation();

        Engine3D::Vulkan::Model* getModel() const;
    };

}

Engine3D::Vulkan::Model* Game::RightTriangleImplementation::rightTriangleModel = nullptr;
size_t Game::RightTriangleImplementation::rightTriangleCount = 0;

using namespace Game;

RightTriangleImplementation::RightTriangleImplementation(Engine3D::Vulkan::Renderer* renderer) {
    RightTriangleImplementation::rightTriangleCount++;

    if(RightTriangleImplementation::rightTriangleModel == nullptr) {
        const std::vector<Engine3D::Vulkan::Vertex> rightTriangleVertices = {
            {{-0.5f, -0.5f, 0.f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
            {{0.5f,  -0.5f, 0.f}, {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f}},
            {{0.5f,  0.5f, 0.f},  {0.0f, 0.0f, 1.0f}, {1.0f, 1.0f}},
        };

        const std::vector<Engine3D::Vulkan::index_t> rightTriangleIndices = {
            0, 1, 2
        };

        auto rightTriangleModel = new Engine3D::Vulkan::Model(
            new Engine3D::Vulkan::VertexBuffer(
                renderer->getDevice(),
                renderer->getTransferCommandPool(),
                rightTriangleVertices.data(),
                static_cast<uint32_t>(rightTriangleVertices.size())
            ),
            new Engine3D::Vulkan::IndexBuffer(
                renderer->getDevice(),
                renderer->getTransferCommandPool(),
                rightTriangleIndices.data(),
                static_cast<uint32_t>(rightTriangleIndices.size())
            )
        );

        RightTriangleImplementation::setModel(rightTriangleModel);
    }
}

RightTriangleImplementation::~RightTriangleImplementation() {
    RightTriangleImplementation::rightTriangleCount--;

    if(RightTriangleImplementation::rightTriangleCount == 0) {
        delete RightTriangleImplementation::rightTriangleModel;
        RightTriangleImplementation::setModel(nullptr);
    }
}

void RightTriangleImplementation::setModel(Engine3D::Vulkan::Model *model) {
    RightTriangleImplementation::rightTriangleModel = model;
}

Engine3D::Vulkan::Model * RightTriangleImplementation::getModel() const {
    return RightTriangleImplementation::rightTriangleModel;
}

RightTriangle::RightTriangle(Engine3D::Vulkan::Renderer* renderingEngine, Engine3D::Vulkan::graphicsPipelineIndex_t graphicsPipelineIndex, Engine3D::Vulkan::Image* texture)
    : impl(new RightTriangleImplementation(renderingEngine)), Object(nullptr, renderingEngine, graphicsPipelineIndex, texture)
{
    this->setModel(impl->getModel());
}

RightTriangle::~RightTriangle() {
    delete this->impl;
}