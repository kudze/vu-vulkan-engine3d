//
// Created by karol on 5/12/2020.
//

#ifndef INC_3DENGINE_RIGHT_TRIANGLE_H
#define INC_3DENGINE_RIGHT_TRIANGLE_H

#include "../../Engine3D/scene/object.h"

namespace Game {
    class RightTriangleImplementation;

    class RightTriangle
        : public Engine3D::Object
    {
    private:
        RightTriangleImplementation* impl;

    public:
        RightTriangle(Engine3D::Vulkan::Renderer* renderingEngine, Engine3D::Vulkan::graphicsPipelineIndex_t graphicsPipelineIndex, Engine3D::Vulkan::Image* texture = nullptr);
        ~RightTriangle();
    };
}


#endif //INC_3DENGINE_RIGHT_TRIANGLE_H
