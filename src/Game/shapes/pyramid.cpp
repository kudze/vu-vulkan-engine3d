//
// Created by karol on 5/12/2020.
//

#include "pyramid.h"

namespace Game {

    class PyramidImplementation {
    private:
        static Engine3D::Vulkan::Model* pyramidModel;
        static size_t pyramidCount;

        static void setModel(Engine3D::Vulkan::Model* model);
    public:
        explicit PyramidImplementation(Engine3D::Vulkan::Renderer* renderer);
        ~PyramidImplementation();

        Engine3D::Vulkan::Model* getModel() const;
    };

}

Engine3D::Vulkan::Model* Game::PyramidImplementation::pyramidModel = nullptr;
size_t Game::PyramidImplementation::pyramidCount = 0;

using namespace Game;

PyramidImplementation::PyramidImplementation(Engine3D::Vulkan::Renderer* renderer) {
    PyramidImplementation::pyramidCount++;

    if(PyramidImplementation::pyramidModel == nullptr) {
        const std::vector<Engine3D::Vulkan::Vertex> pyramidVertices = {
            {{0.f, 0.f, 0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
            {{-0.5f,  -0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f}},
            {{0.5f,  -0.5f, -0.5f},  {0.0f, 0.0f, 1.0f}, {1.0f, 1.0f}},
            {{0.5f, 0.5f, -0.5f},  {1.0f, 1.0f, 1.0f}, {0.0f, 1.0f}},
            {{-0.5f, 0.5f, -0.5f},  {1.0f, 1.0f, 1.0f}, {0.0f, 1.0f}}
        };

        const std::vector<Engine3D::Vulkan::index_t> pyramidIndices = {
            0, 1, 2,
            0, 2, 3,
            0, 3, 4,
            0, 4, 1,
            3, 2, 1,
            1, 4, 3
        };

        auto pyramidModel = new Engine3D::Vulkan::Model(
            new Engine3D::Vulkan::VertexBuffer(
                renderer->getDevice(),
                renderer->getTransferCommandPool(),
                pyramidVertices.data(),
                static_cast<uint32_t>(pyramidVertices.size())
            ),
            new Engine3D::Vulkan::IndexBuffer(
                renderer->getDevice(),
                renderer->getTransferCommandPool(),
                pyramidIndices.data(),
                static_cast<uint32_t>(pyramidIndices.size())
            )
        );

        PyramidImplementation::setModel(pyramidModel);
    }
}

PyramidImplementation::~PyramidImplementation() {
    PyramidImplementation::pyramidCount--;

    if(PyramidImplementation::pyramidCount == 0) {
        delete PyramidImplementation::pyramidModel;
        PyramidImplementation::setModel(nullptr);
    }
}

void PyramidImplementation::setModel(Engine3D::Vulkan::Model *model) {
    PyramidImplementation::pyramidModel = model;
}

Engine3D::Vulkan::Model * PyramidImplementation::getModel() const {
    return PyramidImplementation::pyramidModel;
}

Pyramid::Pyramid(Engine3D::Vulkan::Renderer* renderingEngine, Engine3D::Vulkan::graphicsPipelineIndex_t graphicsPipelineIndex, Engine3D::Vulkan::Image* texture)
    : impl(new PyramidImplementation(renderingEngine)), Object(nullptr, renderingEngine, graphicsPipelineIndex, texture)
{
    this->setModel(impl->getModel());
}

Pyramid::~Pyramid() {
    delete this->impl;
}

