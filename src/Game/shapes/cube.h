//
// Created by karol on 5/12/2020.
//

#ifndef INC_3DENGINE_CUBE_H
#define INC_3DENGINE_CUBE_H

#include "../../Engine3D/scene/object.h"

namespace Game {
    class CubeImplementation;

    class Cube
        : public Engine3D::Object
    {
    private:
        CubeImplementation* impl;

    public:
        Cube(Engine3D::Vulkan::Renderer* renderingEngine, Engine3D::Vulkan::graphicsPipelineIndex_t graphicsPipelineIndex, Engine3D::Vulkan::Image* texture = nullptr);
        ~Cube();

    };
}


#endif //INC_3DENGINE_CUBE_H
