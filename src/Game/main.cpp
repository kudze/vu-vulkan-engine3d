//
// Created by kkraujelis on 2020-02-27.
//

#include "../Engine3D/engine.h"
#include "../Engine3D/vulkan/vulkan_file_shader.h"
#include "../Engine3D/vulkan/vulkan_file_image.h"
#include "../Engine3D/vulkan/vulkan_graphics_pipeline.h"
#include "../Engine3D/vulkan/vulkan_model.h"
#include "../Engine3D/vulkan/vulkan_file_obj_modej.h"
#include "../Engine3D/vulkan/vulkan_font.h"
#include "../Engine3D/scene/scene.h"
#include "../Engine3D/scene/object.h"

#include "components/rotate_component.h"
#include "components/camera_movement_component.h"
#include "shapes/plane.h"
#include "shapes/cube.h"
#include "shapes/triangle.h"
#include "shapes/right_triangle.h"
#include "shapes/pyramid.h"
#include "../Engine3D/scene/ui/ui_canvas.h"
#include "../Engine3D/scene/ui/ui_text.h"
#include "components/main_menu_canvas_component.h"
#include "components/pause_menu_canvas_component.h"

#include <iostream>

using namespace Engine3D;
using namespace Game;

#define APP_VERSION VK_MAKE_VERSION(1, 0, 0)

#ifdef __WIN32__

#include <cstdio>
#include <windows.h>

// maximum mumber of lines the output console should have
static const WORD MAX_CONSOLE_LINES = 500;

/**
 * Redirects stdout stderr and stdin to Windows console.
 */
void RedirectIOToConsole() {
    BOOL f = AllocConsole();
    freopen("CONIN$", "r", stdin);
    freopen("CONOUT$", "w", stdout);
    freopen("CONOUT$", "w", stderr);
}

/**
 * Windows Entry Point.
 *
 * @param hInstance
 * @param hPrevInstance
 * @param lpCmdLine
 * @param nCmdShow
 * @return INT
 */
INT WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
            PSTR lpCmdLine, INT nCmdShow)
#else

/**
 * UNIX entry point.
 *
 * @return int
 */
int main()
#endif
{
    try {

#ifdef __WIN32__
        RedirectIOToConsole();
#endif
        std::cout << "Running normal main!" << std::endl;

        //Window
        auto *window = new Vulkan::Window(
            Window::Type::WINDOWED,
            "Žaidimas",
            Window::Size(1280, 720),
            false
        );

        //Engine.
        auto *engine = new Engine(window);

//        Engine engineS = *engine;

        //Color pipeline
        auto vertexShader = new Vulkan::FileVertexShader(engine->getRenderer()->getDevice(), "vertex_color_shader.spv");

        vertexShader->addUBOLayoutBinding(
            0,
            VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            VK_SHADER_STAGE_VERTEX_BIT
        );

        engine->getRenderer()->registerPipeline(
            new Vulkan::GraphicsPipelineDescription(
                vertexShader,
                new Vulkan::FileFragmentShader(engine->getRenderer()->getDevice(), "fragment_color_shader.spv"),
                COLOR_PIPELINE_INDEX
            )
        );

        //Texture pipeline
        vertexShader = new Vulkan::FileVertexShader(engine->getRenderer()->getDevice(), "vertex_texture_shader.spv");

        vertexShader->addUBOLayoutBinding(
            0,
            VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            VK_SHADER_STAGE_VERTEX_BIT
        );
        vertexShader->addUBOLayoutBinding(
            1,
            VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            VK_SHADER_STAGE_FRAGMENT_BIT
        );

        engine->getRenderer()->registerPipeline(
            new Vulkan::GraphicsPipelineDescription(
                vertexShader,
                new Vulkan::FileFragmentShader(engine->getRenderer()->getDevice(), "fragment_texture_shader.spv"),
                TEXTURE_PIPELINE_INDEX
            )
        );

        //Text pipeline
        vertexShader = new Vulkan::FileVertexShader(engine->getRenderer()->getDevice(), "vertex_text_shader.spv");

        vertexShader->addUBOLayoutBinding(
            0,
            VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            VK_SHADER_STAGE_VERTEX_BIT
        );
        vertexShader->addUBOLayoutBinding(
            1,
            VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            VK_SHADER_STAGE_FRAGMENT_BIT
        );
        vertexShader->addUBOLayoutBinding(
            2,
            VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            VK_SHADER_STAGE_FRAGMENT_BIT
        );

        engine->getRenderer()->registerPipeline(
            new Vulkan::GraphicsPipelineDescription(
                vertexShader,
                new Vulkan::FileFragmentShader(engine->getRenderer()->getDevice(), "fragment_text_shader.spv"),
                TEXT_PIPELINE_INDEX
            )
        );

        //Booting up pipelines.s
        engine->getRenderer()->bootUpPipelines();

        //Font
        auto font = new Vulkan::Font(engine->getRenderer(), "fonts/OpenSans-Regular.ttf", 32);
        auto scene = engine->getScene();

        //Main Menu
        auto canvas = new UI::Canvas(engine->getRenderer()->getWindow());
        scene->getRootEntity()->addChild(canvas);
        canvas->addComponent(new Game::MainMenuCanvasComponent());

        auto text = new UI::Text(engine->getRenderer(), font, "Noredamas zaisti naudok ENTER!", glm::vec3(1.f, 1.f, 1.f));
        text->setPosition(glm::vec3(360, 300, 0));
        canvas->addChild(text);

        auto text2 = new UI::Text(engine->getRenderer(), font, "Noredamas iseiti naudok ESC!", glm::vec3(1.f, 1.f, 1.f));
        text2->setPosition(glm::vec3(400, 350, 0));
        canvas->addChild(text2);

        //Pause menu
        auto canvasPause = new UI::Canvas(engine->getRenderer()->getWindow());
        scene->getRootEntity()->addChild(canvasPause);
        new Game::PauseMenuCanvasComponent(canvasPause);

        auto text3 = new UI::Text(engine->getRenderer(), font, "Noredamas testi zaidima naudok ESCAPE!", glm::vec3(1.f, 1.f, 1.f));
        text3->setPosition(glm::vec3(340, 300, 0));
        canvasPause->addChild(text3);

        auto text4 = new UI::Text(engine->getRenderer(), font, "Noredamas iseiti is zaidimo naudok E!", glm::vec3(1.f, 1.f, 1.f));
        text4->setPosition(glm::vec3(360, 350, 0));
        canvasPause->addChild(text4);

        std::cout <<
                  "Starting Application: \"" << engine->getRenderer()->getInstance()->getAppTitle()
                  << "\" with engine: \"" <<
                  engine->getRenderer()->getInstance()->getEngineTitle() << "\""
                  << std::endl;

        engine->run();

        delete font;
        delete engine;
    } catch (std::exception const &e) {
        std::cout << "Unhandled std::exception has been thrown!" << std::endl;
        std::cout << "Error: " + std::string(e.what()) << std::endl;
    } catch (std::string const &e) {
        std::cout << "Unhandled std::string exception has been thrown!" << std::endl;
        std::cout << "Error: " + e << std::endl;
    } catch (const char *e) {
        std::cout << "Unhandled const char* exception has been thrown!" << std::endl;
        std::cout << "Error: " + std::string(e) << std::endl;
    } catch (...) {
        std::cout << "Unhandled unknown type of exception has been thrown!" << std::endl;
    }

#ifdef WIN32
    system("PAUSE");
#endif

    return 0;
}