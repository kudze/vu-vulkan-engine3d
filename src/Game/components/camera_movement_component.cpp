//
// Created by karol on 5/6/2020.
//

#include "camera_movement_component.h"

using namespace Game;
using namespace Engine3D;

CameraMovementComponent::CameraMovementComponent(float speed)
    : CameraComponent(), speed(speed) {

}

CameraMovementComponent::CameraMovementComponent(Engine3D::Camera *camera, float speed)
    : CameraComponent(camera), speed(speed) {

}

CameraMovementComponent::~CameraMovementComponent() {

}

void CameraMovementComponent::onUpdate(float deltaTime) const {
    auto scene = this->getEntity()->findRootEntity()->getScene();
    auto inputEngine = scene->getEngine()->getInputEngine();
    auto camera = this->getCamera();
    float const sensitivity = 0.01f;

    if (inputEngine->isKeyDown(SDL_SCANCODE_W))
        camera->getPosition() += camera->calculateFrontVector() * deltaTime * this->getSpeed();

    if (inputEngine->isKeyDown(SDL_SCANCODE_S))
        camera->getPosition() += camera->calculateBackVector() * deltaTime * this->getSpeed();

    if (inputEngine->isKeyDown(SDL_SCANCODE_A))
        camera->getPosition() += camera->calculateLeftVector() * deltaTime * this->getSpeed();

    if (inputEngine->isKeyDown(SDL_SCANCODE_D))
        camera->getPosition() += camera->calculateRightVector() * deltaTime * this->getSpeed();

    if (inputEngine->isKeyDown(SDL_SCANCODE_SPACE))
        camera->getPosition() += camera->calculateUpVector() * deltaTime * this->getSpeed();

    if (inputEngine->isKeyDown(SDL_SCANCODE_LSHIFT))
        camera->getPosition() += camera->calculateDownVector() * deltaTime * this->getSpeed();

    auto mouseMovedX = inputEngine->getMouseMovedXAxisLastTick();
    auto mouseMovedY = inputEngine->getMouseMovedYAxisLastTick();

    if (mouseMovedX != 0)
        camera->rotate(mouseMovedX * sensitivity, camera->calculateDownVector(), 1.4f);

    if (mouseMovedY != 0)
        camera->rotate(mouseMovedY * sensitivity, camera->calculateLeftVector(), 1.4f);
}

float CameraMovementComponent::getSpeed() const {
    return speed;
}

void CameraMovementComponent::setSpeed(float speed) {
    CameraMovementComponent::speed = speed;
}

void CameraMovementComponent::onChildAdded(Engine3D::Entity *entity) const {

}

void CameraMovementComponent::onChildRemoved(Engine3D::Entity *entity) const {

}

void CameraMovementComponent::onParentChange(Engine3D::Entity *oldParent, Engine3D::Entity *newParent) const {

}
