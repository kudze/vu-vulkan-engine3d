//
// Created by karol on 5/20/2020.
//

#ifndef INC_3DENGINE_MAIN_MENU_CANVAS_COMPONENT_H
#define INC_3DENGINE_MAIN_MENU_CANVAS_COMPONENT_H

#include "../../Engine3D/scene/entity_component.h"

namespace Game {

    class MainMenuCanvasComponent : public Engine3D::EntityComponent {
        mutable bool hasGameStarted = false;

        void startDemo() const;
        void exitDemo() const;

    public:
        /**
         * Virtual function which is called on every tick.
         *
         * @param deltaTime - passed time since last tick (seconds)
         */
        void onUpdate(float deltaTime) const override;

        /**
         * Virtual function which is called when child has been added to entity
         *
         * @param entity
         */
        void onChildAdded(Engine3D::Entity* entity) const override;

        /**
         * Virtual function which is called when child has been removed from entity.
         *
         * @param entity
         */
        void onChildRemoved(Engine3D::Entity* entity) const override;

        /**
         * Virtual function which is called when parent of the class changes
         */
        void onParentChange(Engine3D::Entity* oldParent, Engine3D::Entity* newParent) const override;

    };

}


#endif //INC_3DENGINE_MAIN_MENU_CANVAS_COMPONENT_H
