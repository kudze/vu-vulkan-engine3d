//
// Created by karol on 5/6/2020.
//

#ifndef INC_3DENGINE_CAMERA_MOVEMENT_COMPONENT_H
#define INC_3DENGINE_CAMERA_MOVEMENT_COMPONENT_H


#include "../../Engine3D/scene/camera_component.h"

namespace Game
{
    /**
     * A demo entity component that rotates entity on every tick.
     *
     * @extends Engine3D::EntityComponent
     */
    class CameraMovementComponent
        : public Engine3D::CameraComponent
    {
        float speed;

    public:

        /**
         * Constructor.
         *
         * @param entity
         */
        CameraMovementComponent(float speed);

        /**
         * Constructor.
         *
         * @param entity
         */
        explicit CameraMovementComponent(Engine3D::Camera* entity, float speed);

        /**
         * Destructor
         */
        virtual ~CameraMovementComponent();

        /**
         * An overridden function which is called every tick.
         *
         * @param deltaTime - passed time since last update (seconds)
         */
        virtual void onUpdate(float deltaTime) const;

        void onChildAdded(Engine3D::Entity *entity) const override;

        void onChildRemoved(Engine3D::Entity *entity) const override;

        void onParentChange(Engine3D::Entity *oldParent, Engine3D::Entity *newParent) const override;

        float getSpeed() const;

        void setSpeed(float speed);
    };
}


#endif //INC_3DENGINE_CAMERA_MOVEMENT_COMPONENT_H
