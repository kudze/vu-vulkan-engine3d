//
// Created by karol on 4/14/2020.
//

#ifndef INC_3DENGINE_ROTATE_COMPONENT_H
#define INC_3DENGINE_ROTATE_COMPONENT_H

#include "../../Engine3D/scene/entity.h"

namespace Game {
    /**
     * A demo entity component that rotates entity on every tick.
     *
     * @extends Engine3D::EntityComponent
     */
    class RotateComponent
        : public Engine3D::EntityComponent {
    public:

        /**
         * Constructor.
         *
         * @param entity
         */
        explicit RotateComponent(Engine3D::Entity *entity);

        /**
         * Destructor
         */
        virtual ~RotateComponent();

        /**
         * An overridden function which is called every tick.
         *
         * @param deltaTime - passed time since last update (seconds)
         */
        virtual void onUpdate(float deltaTime) const;

        void onChildAdded(Engine3D::Entity *entity) const override;

        void onChildRemoved(Engine3D::Entity *entity) const override;

        void onParentChange(Engine3D::Entity *oldParent, Engine3D::Entity *newParent) const override;
    };
}


#endif //INC_3DENGINE_ROTATE_COMPONENT_H
