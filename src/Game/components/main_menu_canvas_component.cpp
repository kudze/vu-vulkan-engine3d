//
// Created by karol on 5/20/2020.
//

#include "main_menu_canvas_component.h"
#include "camera_movement_component.h"
#include "../shapes/cube.h"
#include "rotate_component.h"
#include "../../Engine3D/vulkan/vulkan_file_image.h"
#include "../shapes/plane.h"
#include "../shapes/pyramid.h"
#include "../shapes/triangle.h"
#include "../shapes/right_triangle.h"
#include "../../Engine3D/vulkan/vulkan_file_obj_modej.h"
#include "../../Engine3D/scene/ui/ui_canvas.h"

using namespace Game;
using namespace Engine3D;

void MainMenuCanvasComponent::startDemo() const {
    this->hasGameStarted = true;

    auto engine = this->getEntity()->findRootEntity()->getScene()->getEngine();
    auto window = engine->getRenderer()->getWindow();

    auto canvas = (Engine3D::UI::Canvas*) this->getEntity();
    canvas->setVisible(false);

    //Camera setup
    auto camera = new Camera(window, 90);
    camera->addComponent(new CameraMovementComponent(5));
    camera->setPosition(
        glm::vec3(
            2,
            2,
            1
        )
    );

    auto scene = engine->getScene();
    scene->getRootEntity()->addChild(camera);

    //Cube setup.
    auto object = new Game::Cube(
        engine->getRenderer(),
        COLOR_PIPELINE_INDEX
    );

    new RotateComponent(
        object
    );

    scene->getRootEntity()->addChild(object);

    //Texture set up
    auto texture = Vulkan::FileImage::createImageFromFile(
        engine->getRenderer()->getDevice(),
        engine->getRenderer()->getTransferCommandPool(),
        engine->getRenderer()->getGraphicsCommandPool(),
        "grass-texture.jpg"
    );

    texture->attachImageSampler(
        new Vulkan::ImageSampler(
            texture,
            VK_FILTER_LINEAR,
            16.f
        )
    );

    //Plane set up
    auto object2 = new Game::Plane(
        engine->getRenderer(),
        TEXTURE_PIPELINE_INDEX,
        texture
    );

    object2->setPosition(glm::vec3(0, 0, -5));
    object2->setScale(glm::vec3(20, 20, 20));
    scene->getRootEntity()->addChild(object2);

    //Pyramid
    auto pyramid = new Game::Pyramid(
        engine->getRenderer(),
        COLOR_PIPELINE_INDEX
    );
    pyramid->setPosition(glm::vec3(0, -2.f, 0));
    pyramid->setScale(glm::vec3(1, 1, 1));
    scene->getRootEntity()->addChild(pyramid);

    //Triangle
    auto triangle = new Game::Triangle(
        engine->getRenderer(),
        COLOR_PIPELINE_INDEX
    );

    triangle->setPosition(glm::vec3(10, 0, 0));
    triangle->setRotation(glm::vec3(0, -3.1415f / 2, 0));
    triangle->setScale(glm::vec3(5, 5, 5));
    scene->getRootEntity()->addChild(triangle);

    //Right triangle
    auto triangle2 = new Game::RightTriangle(
        engine->getRenderer(),
        COLOR_PIPELINE_INDEX
    );

    triangle2->setPosition(glm::vec3(-10, 0, 0));
    triangle2->setRotation(glm::vec3(0, 3.1415f / 2, 0));
    triangle2->setScale(glm::vec3(5, 5, 5));
    scene->getRootEntity()->addChild(triangle2);

    //Model
    auto vikingRoomModel = new Vulkan::FileOBJModel(
        engine->getRenderer()->getDevice(),
        engine->getRenderer()->getTransferCommandPool(),
        "models/viking_room.obj"
    );

    auto vikingRoomTexture = Vulkan::FileImage::createImageFromFile(
        engine->getRenderer()->getDevice(),
        engine->getRenderer()->getTransferCommandPool(),
        engine->getRenderer()->getGraphicsCommandPool(),
        "models/viking_room.png"
    );

    vikingRoomTexture->attachImageSampler(
        new Vulkan::ImageSampler(
            texture,
            VK_FILTER_LINEAR,
            16.f
        )
    );

    auto vikingRoomObject = new Object(
        vikingRoomModel,
        engine->getRenderer(),
        TEXTURE_PIPELINE_INDEX,
        vikingRoomTexture
    );

    vikingRoomObject->setPosition(glm::vec3(0, 2.f, 0));
    vikingRoomObject->getRotation().z = 3.1415f;
    scene->getRootEntity()->addChild(vikingRoomObject);
}

void MainMenuCanvasComponent::exitDemo() const {
    this->getEntity()->findRootEntity()->getScene()->getEngine()->stop();
}

void MainMenuCanvasComponent::onUpdate(float deltaTime) const
{
    auto inputEngine = this->getEntity()->findRootEntity()->getScene()->getEngine()->getInputEngine();

    if(inputEngine->isKeyDown(SDL_SCANCODE_RETURN) && !this->hasGameStarted)
        this->startDemo();

    if(inputEngine->isKeyDown(SDL_SCANCODE_ESCAPE) && !this->hasGameStarted)
        this->exitDemo();

}

void MainMenuCanvasComponent::onChildAdded(Engine3D::Entity *entity) const {

}

void MainMenuCanvasComponent::onChildRemoved(Engine3D::Entity *entity) const {

}

void MainMenuCanvasComponent::onParentChange(Engine3D::Entity *oldParent, Engine3D::Entity *newParent) const {

}
