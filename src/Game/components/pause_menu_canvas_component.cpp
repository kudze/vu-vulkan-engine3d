//
// Created by karol on 5/20/2020.
//

#include "pause_menu_canvas_component.h"

using namespace Game;

bool PauseMenuCanvasComponent::isPauseMenuActive() const {
    auto canvas = (Engine3D::UI::Canvas*) this->getEntity();

    return canvas->isVisible();
}

void PauseMenuCanvasComponent::openPauseMenu() const {
    auto canvas = (Engine3D::UI::Canvas*) this->getEntity();

    canvas->setVisible(true);
}

void PauseMenuCanvasComponent::closePauseMenu() const {
    auto canvas = (Engine3D::UI::Canvas*) this->getEntity();

    canvas->setVisible(false);
}

void PauseMenuCanvasComponent::stopGame() const {
    this->getEntity()->findRootEntity()->getScene()->getEngine()->stop();
}

PauseMenuCanvasComponent::PauseMenuCanvasComponent(Engine3D::UI::Canvas* canvas)
    : Engine3D::EntityComponent(canvas)
{
    this->closePauseMenu();
}

void PauseMenuCanvasComponent::onUpdate(float deltaTime) const
{
    static float deltaTimeSum = 0;

    deltaTimeSum += deltaTime;

    if(deltaTimeSum > 0.1f) {
        auto inputEngine = this->getEntity()->findRootEntity()->getScene()->getEngine()->getInputEngine();

        if (inputEngine->isKeyDown(SDL_SCANCODE_ESCAPE)) {
            if (this->isPauseMenuActive())
                this->closePauseMenu();

            else this->openPauseMenu();
        } else if (this->isPauseMenuActive()) {
            if (inputEngine->isKeyDown(SDL_SCANCODE_E))
                this->stopGame();
        }

        deltaTimeSum -= 0.1f;
    }

}

void PauseMenuCanvasComponent::onParentChange(Engine3D::Entity *oldParent, Engine3D::Entity *newParent) const
{

}

void PauseMenuCanvasComponent::onChildRemoved(Engine3D::Entity *entity) const
{

}

void PauseMenuCanvasComponent::onChildAdded(Engine3D::Entity *entity) const
{

}
