//
// Created by karol on 4/14/2020.
//

#include "rotate_component.h"

using namespace Game;
using namespace Engine3D;

RotateComponent::RotateComponent(Entity *entity)
    : EntityComponent(entity)
{

}

RotateComponent::~RotateComponent() = default;

void RotateComponent::onUpdate(float deltaTime) const {

    auto rot = this->getEntity()->getRotation();

    rot.z += 3.1415 * deltaTime;

    this->getEntity()->setRotation(rot);

}

void RotateComponent::onChildAdded(Engine3D::Entity *entity) const {

}

void RotateComponent::onChildRemoved(Engine3D::Entity *entity) const {

}

void RotateComponent::onParentChange(Engine3D::Entity *oldParent, Engine3D::Entity *newParent) const {

}

