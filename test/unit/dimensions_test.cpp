//
// Created by karol on 4/8/2020.
//

#include <gtest/gtest.h>
#include <fstream>

#include <Engine3D/math/dimensions.h>

using namespace Engine3D;

TEST(DimensionsTest, ConstructorTest) {
    auto dim = Dimensions<uint16_t>(1280, 720);

    EXPECT_EQ(dim.getWidth(), 1280);
    EXPECT_EQ(dim.getHeight(), 720);
}

TEST(DimensionsTest, SetterGetterTest) {
    auto dim = Dimensions<uint16_t>(1280, 720);

    dim.setWidth(2000);
    dim.setHeight(1000);

    EXPECT_EQ(dim.getWidth(), 2000);
    EXPECT_EQ(dim.getHeight(), 1000);
}

TEST(DimensionsTest, OperatorInputTest) {
    auto dim = Dimensions<uint16_t>(1280, 720);

    std::stringstream ss;
    ss << dim;
    ss >> dim;

    EXPECT_EQ(dim.getWidth(), 1280);
    EXPECT_EQ(dim.getHeight(), 720);
}

TEST(DimensiosTest, ToStringOperatorOutputTest)
{
    auto dim = Dimensions<uint16_t>(1280, 720);

    std::stringstream ss;
    ss << dim;

    EXPECT_TRUE(ss.str() == dim.toString());
}