//
// Created by karol on 4/22/2020.
//

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <glm.hpp>
#include <gtc/matrix_transform.hpp>

#include "../src/Engine3D/scene/entity.h"
#include "../src/Engine3D/scene/entity_component.h"

using namespace Engine3D;

class EmptyEntityComponent : public EntityComponent {
public:
    EmptyEntityComponent()
        : EntityComponent()
    {}

    explicit EmptyEntityComponent(Entity* e)
            : EntityComponent(e)
        {}
    void onUpdate(float deltaTime) const override {};

    void onChildAdded(Entity* entity) const override {};

    void onChildRemoved(Entity* entity) const override {};

    void onParentChange(Entity* oldParent, Entity* newParent) const override {};
};

class EntityComponentUpdateMock : public EntityComponent {
public:
    explicit EntityComponentUpdateMock(Entity* e)
        : EntityComponent(e)
    {}

    MOCK_METHOD(void, onUpdate, (float), (const override));

    void onChildAdded(Entity* entity) const override {};

    void onChildRemoved(Entity* entity) const override {};

    void onParentChange(Entity* oldParent, Entity* newParent) const override {};
};

TEST(EntityTest, UpdateTest) {
    auto entity = new Entity();
    auto entityComponent = new EntityComponentUpdateMock(entity);

    EXPECT_CALL(*entityComponent, onUpdate(5));

    entity->update(5);

    delete entity;
}

TEST(EntityTest, UpdateChildTest) {
    auto entity = new Entity();
    auto child = new Entity();
    entity->addChild(child);

    auto childComponent = new EntityComponentUpdateMock(child);

    EXPECT_CALL(*childComponent, onUpdate(10));

    entity->update(10);

    EXPECT_CALL(*childComponent, onUpdate(9));

    child->update(9);

    delete entity;
}

TEST(EntityTest, CircularSelfTest) {
    auto entity = new Entity();

    try {
        entity->addChild(entity);
        FAIL();
    }

    catch(std::runtime_error const& e)
    {
        EXPECT_STREQ(e.what(), "Circular inheritance is not supported!");
    }

    delete entity;
}

TEST(EntityTest, CircularChildTest) {
    auto entity = new Entity();
    auto child = new Entity();

    entity->addChild(child);

    try {
        child->addChild(entity);
        FAIL();
    }

    catch(std::runtime_error const& e)
    {
        EXPECT_STREQ(e.what(), "Circular inheritance is not supported!");
    }
}

TEST(EntityTest, ComponentDualParentTest) {
    auto entity = new Entity();
    auto entity2 = new Entity();
    auto entityComponent = new EmptyEntityComponent();

    try {
        entity->addComponent(entityComponent);
        entity2->addComponent(entityComponent);
        FAIL();
    }

    catch(std::runtime_error const& e)
    {
        EXPECT_STREQ(e.what(), "This entity component already belongs to another entity!");
    }
}

class EntityComponentChildAddMock : public EntityComponent {
public:
    explicit EntityComponentChildAddMock(Entity* e)
        : EntityComponent(e)
    {}

    MOCK_METHOD(void, onChildAdded, (Entity * ), (const override));

    void onUpdate(float deltaTime) const override {};

    void onChildRemoved(Entity* entity) const override {};

    void onParentChange(Entity* oldParent, Entity* newParent) const override {};
};

TEST(EntityTest, ChildAddedTest) {
    auto entity = new Entity();
    auto entityComponent = new EntityComponentChildAddMock(entity);

    auto child = new Entity();
    EXPECT_CALL(*entityComponent, onChildAdded(child));

    entity->addChild(child);

    delete entity;
}

class EntityComponentChildRemoveMock : public EntityComponent {
public:
    explicit EntityComponentChildRemoveMock(Entity* e)
        : EntityComponent(e)
    {}

    MOCK_METHOD(void, onChildRemoved, (Entity * ), (const override));

    void onUpdate(float deltaTime) const override {};

    void onChildAdded(Entity* entity) const override {};

    void onParentChange(Entity* oldParent, Entity* newParent) const override {};
};

TEST(EntityTest, ChildRemovedTest) {
    auto entity = new Entity();
    auto entityComponent = new EntityComponentChildRemoveMock(entity);

    auto child = new Entity();
    entity->addChild(child);

    EXPECT_CALL(*entityComponent, onChildRemoved(child));

    entity->removeChild(child);

    delete entity;
    delete child;
}

TEST(EntityTest, ComponentParentTest) {
    auto entity = new Entity();
    auto entity2 = new Entity();
    auto entityComponent = new EmptyEntityComponent();

    EXPECT_EQ(entityComponent->getEntity(), nullptr);

    entity->addComponent(entityComponent);

    EXPECT_EQ(entityComponent->getEntity(), entity);

    entity->removeComponent(entityComponent);

    EXPECT_EQ(entityComponent->getEntity(), nullptr);

    entity2->addComponent(entityComponent);

    EXPECT_EQ(entityComponent->getEntity(), entity2);

    entity2->removeComponent(entityComponent);

    EXPECT_EQ(entityComponent->getEntity(), nullptr);

    delete entityComponent;
    delete entity;
    delete entity2;
}


TEST(EntityTest, MatrixTest) {
    auto entity = new Entity();

    entity->getPosition().x = 5;
    entity->getPosition().y = 4;
    entity->getPosition().z = 9;

    entity->getRotation().x = 3.1415f;
    entity->getRotation().y = 3.1415f * 2.f;
    entity->getRotation().z = 3.1415f * 0.5f;

    entity->getScale().x = 2;
    entity->getScale().y = 3;
    entity->getScale().z = 3;

    auto position = glm::identity<glm::mat4>(), rotation = glm::identity<glm::mat4>(), scale = glm::identity<glm::mat4>();
    scale = glm::scale(scale, entity->getScale());
    rotation = glm::rotate(rotation, entity->getRotation().x, glm::vec3(1, 0, 0));
    rotation = glm::rotate(rotation, entity->getRotation().y, glm::vec3(0, 1, 0));
    rotation = glm::rotate(rotation, entity->getRotation().z, glm::vec3(0, 0, 1));
    position = glm::translate(position, entity->getPosition());

    EXPECT_EQ(position * rotation * scale, entity->calculateModelMatrix());

    delete entity;
}

TEST(EntityTest, IdentityMatrixTest) {
    auto entity = new Entity();

    auto result = glm::identity<glm::mat4>();
    EXPECT_EQ(result, entity->calculateModelMatrix());

    delete entity;
}