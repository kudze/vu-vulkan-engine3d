//
// Created by karol on 4/8/2020.
//

#include <gtest/gtest.h>

#include <Engine3D/window/window.h>
#include <Engine3D/window/sdl.h>

using namespace Engine3D;

#define TITLE_ONE u8"Testing jobs"
#define TITLE_TWO u8"Testing jobs 2"

TEST(WindowTest, ResizeableTest) {
    SDL::initialize();

    auto* window = new Window(
        Window::Type::WINDOWED,
        TITLE_ONE,
        Dimensions<uint16_t>(800, 600)
    );

    EXPECT_EQ(window->isResizeable(), false);

    window->setIsResizeable(true);
    EXPECT_EQ(window->isResizeable(), true);

    window->setIsResizeable(false);
    EXPECT_EQ(window->isResizeable(), false);

    delete window;

    SDL::deinitialize();
}

TEST(WindowTest, TitleTest) {
    SDL::initialize();

    auto* window = new Window(
        Window::Type::WINDOWED,
        TITLE_ONE,
        Dimensions<uint16_t>(800, 600)
    );

    EXPECT_EQ(window->getName(), TITLE_ONE);

    window->rename(TITLE_TWO);
    EXPECT_EQ(window->getName(), TITLE_TWO);

    delete window;

    SDL::deinitialize();
}

TEST(WindowTest, TypeTest) {
    SDL::initialize();

    #define TITLE_ONE u8"Testing jobs"
    #define TITLE_TWO u8"Testing jobs 2"
    auto* window = new Window(
        Window::Type::WINDOWED,
        TITLE_ONE,
        Dimensions<uint16_t>(800, 600)
    );

    EXPECT_EQ (window->getType(), Window::Type::WINDOWED);

    window->setType(Window::Type::BORDERLESS);
    EXPECT_EQ(window->getType(), Window::Type::BORDERLESS);

    window->setType(Window::Type::FULLSCREEN);
    EXPECT_EQ(window->getType(), Window::Type::FULLSCREEN);

    delete window;

    SDL::deinitialize();
}

TEST(WindowTest, ResizeTest) {
    SDL::initialize();

#define TITLE_ONE u8"Testing jobs"
#define TITLE_TWO u8"Testing jobs 2"
    auto* window = new Window(
        Window::Type::WINDOWED,
        TITLE_ONE,
        Dimensions<uint16_t>(800, 600)
    );

    EXPECT_EQ(window->getSize(), Dimensions<uint16_t>(800, 600));

    window->resize(Dimensions<uint16_t>(1280, 720));
    EXPECT_EQ(window->getSize(), Dimensions<uint16_t>(1280, 720));

    window->setType(Window::Type::BORDERLESS);
    EXPECT_EQ(window->getSize(), Dimensions<uint16_t>(1280, 720));

    window->resize(Dimensions<uint16_t>(1600, 1080));
    EXPECT_EQ(window->getSize(), Dimensions<uint16_t>(1600, 1080));

    delete window;

    SDL::deinitialize();
}