//
// Created by karol on 4/8/2020.
//

#include <cstdio>
#include <fstream>

#include <gtest/gtest.h>
#ifndef WIN32
#include <unistd.h>
#endif

int main(int argc, char** argv) {
    std::remove("unit_test_log.txt");
#ifndef WIN32
    int outdes = dup(1);
#endif

    //Redirecting stdout and stderr to file
    freopen("unit_test_log.txt", "w", stdout);
    freopen("unit_test_log.txt", "w", stderr);

    ::testing::InitGoogleTest(&argc, argv);
    auto result = RUN_ALL_TESTS();

#ifdef WIN32
    freopen("CON", "w", stdout);
#else
    stdout = fdopen(outdes, "w");
#endif

    //Reading log file and printing it back to old stdout.
    std::ifstream log("unit_test_log.txt");
    log.seekg(0, std::ios_base::end);
    size_t len = log.tellg();
    log.seekg(0, std::ios_base::beg);

    std::string logData(len, '\0');
    log.read(&logData[0], len);

    fprintf(stdout, "%s", logData.c_str());

    return result;
}
