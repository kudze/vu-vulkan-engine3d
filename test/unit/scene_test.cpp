//
// Created by karol on 4/28/2020.
//

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../src/Engine3D/scene/entity.h"
#include "../src/Engine3D/scene/scene.h"

using namespace Engine3D;

class MockEntity : public Entity
{
public:
    MOCK_METHOD(void, update, (float), (override));
};

TEST(SceneTest, SceneUpdateToEntityTest)
{
    auto testEntity = new MockEntity();

    auto scene = new Scene(nullptr);
    scene->getRootEntity()->addChild(testEntity);

    EXPECT_CALL(*testEntity, update(0));

    scene->update();

    delete scene;
}